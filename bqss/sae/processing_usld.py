from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

USLD_DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "JOU": "Int64",
}


def process_usld_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau USLD (Unité de Soin Longue Durée) :
        - table USLD
    """
    filenames = RAW_DOMAIN_PATH.glob("**/USLD_2*.csv")
    dfs = []
    for filename in filenames:
        usld_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=USLD_DTYPES_DICT,
            usecols=USLD_DTYPES_DICT.keys(),
        )
        dfs.append(usld_df)
    usld_df = pd.concat(dfs)
    usld_df.rename(
        columns={col: f"usld_{col.lower()}" for col in usld_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=usld_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["usld_an", "usld_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["usld_an", "usld_fi"], inplace=True)
    return sae_df
