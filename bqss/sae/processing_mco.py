from typing import Final

import numpy as np
import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

MCO_DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "LIT_MED": "Int64",
    "LIT_CHI": "Int64",
    "LIT_OBS": "Int64",
    "LIT_MCO": "Int64",
    "JOU_MCO": "Int64",
    "SEJHC_MED": "Int64",
    "SEJHC_CHI": "Int64",
    "SEJHC_OBS": "Int64",
    "SEJHC_MCO": "Int64",
    "SEJ0_MED": "Int64",
    "SEJ0_CHI": "Int64",
    "SEJ0_OBS": "Int64",
    "SEJ0_MCO": "Int64",
    "PLA_MED": "Int64",
    "PLA_CHI": "Int64",
    "PLA_OBS": "Int64",
    "PLA_MCO": "Int64",
}

PERINAT_DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "AUTOR": "string",
    "SEJACC": "Int64",
    "JOUACC": "Int64",
    "SEJCES": "Int64",
    "JOUCES": "Int64",
    "IVG": "Int64",
    "IVGME": "Int64",
}


def process_mco_data(sae_df) -> pd.DataFrame:
    """
    Traite les données des bordereaux MCO et PERINAT :
        - table MCO : description des capacités et activités MCO
        - table PERINAT : périnatalité (obstétrique, néonatologie, IVG, etc)
    """
    sae_df = process_mco_table(sae_df)
    sae_df = process_perinat_table(sae_df)
    return sae_df


def process_mco_table(sae_df) -> pd.DataFrame:
    """
    Traite les données de la table MCO : description des capacités et activités
    """
    filenames = RAW_DOMAIN_PATH.glob("**/MCO_2*.csv")
    dfs = []
    for filename in filenames:
        mco_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=MCO_DTYPES_DICT,
            usecols=MCO_DTYPES_DICT.keys(),
        )
        dfs.append(mco_df)
    mco_df = pd.concat(dfs)
    mco_df.rename(
        columns={col: f"mco_{col.lower()}" for col in mco_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=mco_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["mco_an", "mco_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["mco_an", "mco_fi"], inplace=True)
    return sae_df


def process_perinat_table(sae_df) -> pd.DataFrame:
    """
    Traite les données de la table PERINAT : périnatalité
    """
    filenames = RAW_DOMAIN_PATH.glob("**/PERINAT_2*.csv")
    dfs = []
    for filename in filenames:
        perinat_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=PERINAT_DTYPES_DICT,
            usecols=PERINAT_DTYPES_DICT.keys(),
        )
        dfs.append(perinat_df)
    perinat_df = pd.concat(dfs)

    # calcul des durées moyennes des séjours :
    #   - ACC : accouchements (tous types confondus)
    #   - CES : accouchements par césarienne
    #   - NAT : accouchements par voie naturelle
    perinat_df["JOUNAT"] = perinat_df["JOUACC"] - perinat_df["JOUCES"]
    perinat_df["SEJNAT"] = perinat_df["SEJACC"] - perinat_df["SEJCES"]
    perinat_df["SEJACC_DM"] = np.around(
        perinat_df["JOUACC"] / perinat_df["SEJACC"] + 1, decimals=1
    )  # vérification métier nécessaire, ex : 750300931 en 2018
    perinat_df["SEJCES_DM"] = np.around(
        perinat_df["JOUCES"] / perinat_df["SEJCES"] + 1, decimals=1
    )
    perinat_df["SEJNAT_DM"] = np.around(
        perinat_df["JOUNAT"] / perinat_df["SEJNAT"] + 1, decimals=1
    )

    perinat_df.rename(
        columns={col: f"perinat_{col.lower()}" for col in perinat_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=perinat_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["perinat_an", "perinat_fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["perinat_an", "perinat_fi"], inplace=True)
    return sae_df
