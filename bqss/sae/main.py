import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.sae.constants import RESOURCES_DOMAIN_PATH
from bqss.sae.data_download import download_data
from bqss.sae.processing import process_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(download: bool = True):
    """
    Point d'entrée du pipeline de données SAE
    """
    if not download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    process_data()
