from typing import Final

import pandas as pd

from bqss.constants import ISO_8859_1
from bqss.sae.constants import RAW_DOMAIN_PATH

CANCERO_DTYPES_DICT: Final[dict] = {
    "AN": "int64",
    "FI": "string",
    "CANCERO_A10": "Int64",
    "CANCERO_A15": "Int64",
    "CANCERO_A16": "Int64",
}


def process_cancero_data(sae_df) -> pd.DataFrame:
    """
    Traite les données du bordereau CANCERO :
        - table CANCERO
    """
    filenames = RAW_DOMAIN_PATH.glob("**/CANCERO_2*.csv")
    dfs = []
    for filename in filenames:
        cancero_df = pd.read_csv(
            filename,
            delimiter=";",
            encoding=ISO_8859_1,
            dtype=CANCERO_DTYPES_DICT,
            usecols=CANCERO_DTYPES_DICT.keys(),
        )
        dfs.append(cancero_df)

    cancero_df = pd.concat(dfs)
    cancero_df.rename(
        columns={col: col.lower() for col in cancero_df.columns},
        inplace=True,
    )
    sae_df = pd.merge(
        left=sae_df,
        right=cancero_df,
        how="outer",
        left_on=["id_an", "id_fi"],
        right_on=["an", "fi"],
        validate="one_to_one",
    )
    sae_df.drop(columns=["an", "fi"], inplace=True)
    return sae_df
