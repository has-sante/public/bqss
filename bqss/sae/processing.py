import logging
from typing import Final

import pandas as pd

from bqss.constants import FINESS_TYPE_GEO, FINESS_TYPE_JUR, UTF_8
from bqss.data_acquisition_utils import clean_directory
from bqss.metadata_utils import table_schema_to_df
from bqss.sae.constants import (
    FINAL_DOMAIN_PATH,
    METADATA_SECTEUR_DICT,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.sae.processing_blocs import process_blocs_data
from bqss.sae.processing_cancero import process_cancero_data
from bqss.sae.processing_dialyse import process_dialyse_data
from bqss.sae.processing_filtre import process_filtre_data
from bqss.sae.processing_had import process_had_data
from bqss.sae.processing_id import process_id_data
from bqss.sae.processing_mco import process_mco_data
from bqss.sae.processing_psy import process_psy_data
from bqss.sae.processing_ssr import process_ssr_data
from bqss.sae.processing_usld import process_usld_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine de données SAE
    """
    logger.info("Processing data...")

    clean_directory(FINAL_DOMAIN_PATH)

    sae_df = process_id_data()
    sae_df = process_filtre_data(sae_df)
    sae_df = process_mco_data(sae_df)
    sae_df = process_had_data(sae_df)
    sae_df = process_ssr_data(sae_df)
    sae_df = process_psy_data(sae_df)
    sae_df = process_usld_data(sae_df)
    sae_df = process_dialyse_data(sae_df)
    sae_df = process_blocs_data(sae_df)
    sae_df = process_cancero_data(sae_df)

    logger.info(
        "%d FINESS SAE data have been historized among %d records",
        sae_df.nunique()["id_fi"],
        len(sae_df),
    )

    sae_df.to_csv(
        FINAL_DOMAIN_PATH / "sae.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    # generate metadata file from table schema
    metadata_df = table_schema_to_df(SCHEMAS_DOMAIN_PATH / "sae.json")
    metadata_df["secteur"] = (
        metadata_df["name"].str.split("_").str[0].map(METADATA_SECTEUR_DICT)
    )
    metadata_df = metadata_df[~metadata_df["name"].str.startswith("id_")]
    metadata_df.to_csv(
        FINAL_DOMAIN_PATH / "sae_metadata.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    sae_kv_df = get_key_value_format(sae_df)
    sae_kv_df.to_csv(
        FINAL_DOMAIN_PATH / "sae_key_value.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    logger.info("Processing data OK")


def get_key_value_format(sae_df: pd.DataFrame) -> pd.DataFrame:
    """
    Construit la représentation clé-valeur du dataframe SAE en paramètre
    """
    sae_kv_df = sae_df.melt(
        id_vars=["id_an", "id_fi", "id_rs", "id_fi_ej"],
        var_name="key",
    )
    sae_kv_df.dropna(subset=["value"], inplace=True)

    metadata = pd.read_csv(
        FINAL_DOMAIN_PATH / "sae_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    metadata = metadata[~metadata["name"].str.startswith("id_")]

    for value_type in ["boolean", "string", "integer", "number"]:
        keys = metadata[metadata["type"] == value_type]["name"]
        sae_kv_df[f"value_{value_type}"] = sae_kv_df[
            sae_kv_df["key"].isin(keys)
        ]["value"]

    sae_kv_df.drop(columns="value", inplace=True)
    sae_kv_df.rename(
        columns={
            "id_an": "annee",
            "id_fi": "finess",
            "id_rs": "raison_sociale",
            "id_fi_ej": "finess_ej",
            "value_number": "value_float",
        },
        inplace=True,
    )
    sae_kv_df.insert(2, "finess_type", FINESS_TYPE_GEO)

    sae_kv_df.loc[
        sae_kv_df["finess"] == sae_kv_df["finess_ej"], "finess_type"
    ] = FINESS_TYPE_JUR

    return sae_kv_df
