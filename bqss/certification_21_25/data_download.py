import logging
import os
import shutil
from typing import Final

import requests

from bqss.certification_21_25.constants import RAW_DOMAIN_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def download_data(data_sources):
    """
    Télécharge les données de certifications
    """
    logger.info("Téléchargement des données de certifications ref 2021-2025")

    # clean directory structure
    shutil.rmtree(RAW_DOMAIN_PATH, ignore_errors=True)
    os.makedirs(RAW_DOMAIN_PATH)

    data_source = data_sources["sources"]
    for source_name, url in data_source.items():
        # download data
        with requests.get(url, timeout=10) as response, open(
            RAW_DOMAIN_PATH / f"{source_name}.csv", "wb"
        ) as file:
            file.write(response.content)

    logger.info("Téléchargements terminés")
