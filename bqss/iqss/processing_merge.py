import pandas as pd

from bqss.iqss.constants import CLEAN_IQSS_PATH, FINAL_IQSS_PATH


def merged_iqss_data():
    """
    Merger tous les fichiers iqss
    """
    merged_dfs = []
    for year_data_path in CLEAN_IQSS_PATH.iterdir():
        files = [
            file_path
            for file_path in year_data_path.iterdir()
            if (year_data_path / file_path).is_file()
        ]
        df_from_each_file = [pd.read_parquet(file_path) for file_path in files]
        year_merged_df = pd.concat(df_from_each_file, ignore_index=True)
        merged_dfs.append(year_merged_df)
    merged_df = pd.concat(merged_dfs, ignore_index=True)

    merged_df.to_parquet(
        FINAL_IQSS_PATH / "iqss.parquet",
    )
