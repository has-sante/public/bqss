import logging
from typing import Final

import numpy as np
import pandas as pd
import tqdm

from bqss.data_acquisition_utils import read_file
from bqss.iqss.constants import CLEAN_IQSS_PATH, COL_2_DROP, RAW_IQSS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def clean_data(metadata_df):
    """
    Nettoyer les fichiers iqss
    """
    for year_data_path in RAW_IQSS_PATH.iterdir():
        files = [
            file_path
            for file_path in year_data_path.iterdir()
            if (year_data_path / file_path).is_file()
        ]
        for file_path in tqdm.tqdm(
            files, desc=f"Nettoyage des fichiers IQSS {year_data_path.stem}"
        ):
            source_name = CLEAN_IQSS_PATH / year_data_path.name
            source_name.mkdir(parents=True, exist_ok=True)
            iqss_df = read_file(file_path)
            iqss_df.drop(columns=COL_2_DROP, inplace=True, errors="ignore")
            iqss_df = clean_columns(iqss_df, file_path, metadata_df)
            iqss_df = clean_values(
                iqss_df, metadata_df, source_name, file_path
            )
            name = file_path.name.split(".")[0] + ".parquet"

            iqss_df.drop_duplicates(inplace=True)
            iqss_df.to_parquet(source_name / name)


def clean_columns(iqss_df, file_path, metadata_df):
    """
    Nettoie les colonnes des fichiers iqss raw et les stocker dans clean
    """
    # renommer les critères iqss grâce aux codes
    annee = int(file_path.parent.name)
    ancien_criteres = list(iqss_df.columns)
    code_dict = get_code(file_path, metadata_df, annee, ancien_criteres)
    iqss_df.rename(columns=code_dict, inplace=True)
    # définir en minuscule les noms des colonnes
    iqss_df.columns = [critere.lower() for critere in iqss_df.columns]
    # renommer des colonnes spécifiques
    iqss_df = iqss_df.rename(
        columns={
            "sources": "annee",
            "rs": "raison_sociale",
            "nom_es": "raison_sociale",
            "raison_sociale_2016": "raison_sociale",
            "raison_sociale_2017": "raison_sociale",
            "raison sociale": "raison_sociale",
        }
    )
    # abandonner les champs inultiles
    iqss_df = drop_colums(iqss_df, metadata_df)
    if "raison_sociale" not in iqss_df.columns:
        logger.warning("Pas de raison sociale dans %s", file_path)
    return iqss_df


def get_code(file_path, metadata_df, annee, ancien_criteres):
    theme, secteur, *_ = file_path.name.split("_")

    if secteur in ["mco", "had", "ssr", "psy"]:
        code_df = metadata_df.loc[
            (metadata_df["annee"] == annee - 1)
            & (metadata_df["secteur"] == secteur.upper())
            & (metadata_df["theme"] == theme.upper())
            & (metadata_df["ancienne_variable"].isin(ancien_criteres))
        ]
    elif theme == "ias":
        code_df = metadata_df.loc[
            (metadata_df["annee"] == annee - 1)
            & (metadata_df["theme"] == theme.upper())
            & (metadata_df["ancienne_variable"].isin(ancien_criteres))
        ]
    else:
        code_df = metadata_df.loc[
            (metadata_df["annee"] == annee - 1)
            & (metadata_df["ancienne_variable"].isin(ancien_criteres))
        ]
    code_dict = {}
    for i in code_df["ancienne_variable"]:
        if i in ancien_criteres:
            code_dict[i] = code_df.loc[code_df["ancienne_variable"] == i][
                "code"
            ].array[0]

    return code_dict


def clean_values(iqss_df, metadata_df, source_name, file_path):
    """
    Nettoie les valeurs des fichiers iqss raw et les stocke dans clean
    """
    # nettoie type de la donnée
    columns_string_list = (
        metadata_df.loc[
            (metadata_df["type"] == "string")
            | (metadata_df["type"] == "classe")
            | (metadata_df["type"] == "float-string")
            | (metadata_df["type"] == "int-string")
        ]["code"]
        .str.lower()
        .unique()
    )
    columns_float_list = (
        metadata_df[metadata_df["type"] == "float"]["code"]
        .str.lower()
        .unique()
    )
    columns_int_list = (
        metadata_df[metadata_df["type"] == "int"]["code"].str.lower().unique()
    )

    columns_2_string = iqss_df.columns.intersection(
        columns_string_list
    ).tolist()
    columns_2_float = iqss_df.columns.intersection(columns_float_list).tolist()
    columns_2_int = iqss_df.columns.intersection(columns_int_list).tolist()

    for col_2_float in columns_2_float:
        casted_df = pd.to_numeric(
            iqss_df[col_2_float].dropna(), errors="coerce"
        )
        if casted_df.isna().any():
            logger.error(
                "Le type de la variable %s est mixte et non float", col_2_float
            )
    for col_2_int in columns_2_int:
        casted_df = pd.to_numeric(iqss_df[col_2_int].dropna(), errors="coerce")
        if casted_df.isna().any():
            logger.error(
                "Le type de la variable %s est mixte et non int", col_2_int
            )

    if columns_2_string:
        iqss_df[columns_2_string] = iqss_df[columns_2_string].astype("string")
    if columns_2_float:
        iqss_df[columns_2_float] = iqss_df[columns_2_float].replace(
            [".", ","], [np.nan, "."], regex=True
        )
        iqss_df[columns_2_float] = iqss_df[columns_2_float].astype("Float64")
    if columns_2_int:
        iqss_df[columns_2_int] = iqss_df[columns_2_int].astype("Int64")

    # defragment dataframe
    iqss_df = iqss_df.copy()
    if "annee" not in iqss_df.columns:
        iqss_df["annee"] = int(source_name.stem) - 1
    iqss_df["annee"] = iqss_df["annee"].astype(int)
    iqss_df["finess"] = iqss_df["finess"].astype("string")
    iqss_df["raison_sociale"] = iqss_df["raison_sociale"].astype("string")
    if not iqss_df.loc[iqss_df["finess"].isnull(), :].empty:
        logger.warning("Pas de finess dans %s", source_name / file_path.name)
    return iqss_df


def drop_colums(iqss_df, metadata_df):
    """
    Supprime les colonnes qui ne se trouvent pas dans le fichier de métadonnées
    """
    # drop useless data
    metadata_df["code"] = metadata_df["code"].str.lower()
    columns = []
    for critere in iqss_df.columns:
        if (
            critere != "finess"
            and critere != "raison_sociale"
            and critere != "annee"
            and not metadata_df["code"].isin([critere]).any()
        ):
            columns.append(critere)
    iqss_df.drop(columns=columns, inplace=True)
    return iqss_df
