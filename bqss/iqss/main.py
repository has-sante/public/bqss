import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.iqss.constants import RESOURCES_IQSS_PATH
from bqss.iqss.data_acquisition import download_data
from bqss.iqss.processing import process_data
from bqss.iqss.validate_metadata import validate_metadata_file

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(download: bool = True):
    """
    Point d'entrée de tout le pipeline iqss
    """
    # acquisition des données
    if not download:
        logger.info("Téléchargement des données ignoré")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)

    # validation du fichier de métadonnées
    validate_metadata_file()

    # process des données
    process_data()
