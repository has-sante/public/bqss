import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.iqss.constants import (
    FINAL_IQSS_PATH,
    RESOURCES_IQSS_PATH,
    SCHEMAS_IQSS_PATH,
)
from bqss.iqss.data_acquisition import download_data
from bqss.iqss.processing import process_data
from bqss.iqss.validate_metadata import validate_metadata_file
from bqss.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée de tout le pipeline iqss
    """
    # acquisition des données
    if skip_data_download:
        logger.info("Téléchargement des données ignoré")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)

    # validation du fichier de metadonnées
    validate_metadata_file()

    # process des données
    process_data()

    # validation de la donnée
    if data_validation:
        logger.info("Validation des données")
        valid = is_data_valid(
            FINAL_IQSS_PATH / "iqss_key_value.csv",
            SCHEMAS_IQSS_PATH / "iqss_key_value.json",
        )
        logger.info(
            "Résultat de la validation : %s", "VALID" if valid else "INVALID"
        )
    else:
        logger.info("Validation des données ignorée")
