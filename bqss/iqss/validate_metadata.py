import logging
from typing import Final

import pandas as pd

from bqss.data_acquisition_utils import read_file
from bqss.iqss.constants import (
    ACRONYME,
    COL_2_DROP,
    RAW_IQSS_PATH,
    RESOURCES_IQSS_PATH,
    SECTEUR,
    THEME,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def get_missing_metadata(metadata_df):
    missing_metadata = set()

    # check que les variables du fichier metadata sont bien les variables des fichiers en open data
    for year_data_path in RAW_IQSS_PATH.iterdir():
        files = [
            file_path
            for file_path in year_data_path.iterdir()
            if (year_data_path / file_path).is_file()
        ]
        file_year = int(year_data_path.name) - 1
        iqss_df_columns = [
            list(read_file(file_path).columns) for file_path in files
        ]
        iqss_df_columns = {
            item for sublist in iqss_df_columns for item in sublist
        }

        metadata_file_year = set(
            metadata_df.loc[
                metadata_df["annee"]  # pylint: disable=unsubscriptable-object
                == file_year
            ]["ancienne_variable"]
        )
        diff = metadata_file_year - iqss_df_columns

        if diff and [i for i in diff if i not in COL_2_DROP]:
            logger.error(
                "Ces variables sont dans le fichier de metadata mais pas dans les données (%s): %s",
                files,
                [i for i in diff if i not in COL_2_DROP],
            )

        diff = iqss_df_columns - metadata_file_year

        ignore_cols = COL_2_DROP + [
            "Finess",
            "Raison Sociale",
            "finess",
            "raison_sociale",
            "code_region",
            "nom_ES",
            "Raison_sociale",
            "raison_sociale_2017",
            "rs",
            "raison_sociale_2016",
        ]

        for col in diff:
            if col not in ignore_cols:
                missing_metadata.add((int(year_data_path.name), col))

    return missing_metadata


def validate_metadata_file():
    """
    Valide le fichier de métadonnées du domaine IQSS
    """
    logger.info("Validation du fichier de metadata")
    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv")

    missing_metadata = get_missing_metadata(metadata_df)

    if missing_metadata:
        logger.error(
            "Ces variables sont dans les fichiers de données mais pas dans le fichier de metadata: %s",
            sorted(missing_metadata),
        )
    # le code contient la version telle qu'elle est donnée dans la colonne
    metadata_df["code_version"] = (
        metadata_df["code"].str.split("-").str[0].str.split("_").str[-1]
    )

    if not metadata_df[
        metadata_df["code_version"].astype(int) != metadata_df["version"]
    ].empty:
        logger.error(
            "Incohérence du code et de la version: %s",
            metadata_df[
                metadata_df["code_version"].astype(int)
                != metadata_df["version"]
            ][["code", "code_version", "version"]],
        )
    metadata_df.drop(columns=["code_version"], inplace=True)

    # code + annee dans le fichier de metadonnée
    if metadata_df.duplicated(subset=["code", "annee"]).any():
        logger.error(
            "Le couple (code, annee) n'est pas unique pour: %s",
            metadata_df[  # pylint: disable=unsubscriptable-object
                metadata_df.duplicated(subset=["code", "annee"])
            ][["code", "annee"]].to_dict(orient="records"),
        )

    # check secteur theme accronyme
    if not (
        metadata_df.secteur.isin(SECTEUR).any()
        & metadata_df.theme.isin(THEME).any()
        & metadata_df.acronyme.isin(ACRONYME).any()
    ):
        logger.error("Un code un secteur ou un accronyme n'est pas connu")

    # Check types identique pour un code donné
    types_count_df = metadata_df.groupby("code")["type"].nunique()
    wrong_codes_df = types_count_df[types_count_df > 1].index
    if not wrong_codes_df.empty:
        logger.error(
            "Les codes suivants ont des types hétérogènes: %s",
            wrong_codes_df.tolist(),
        )

    # Check types métiers uniques pour un indicateur donné
    type_metier_df = (
        metadata_df.groupby(["indicateur", "annee"])["type_metier"]
        .value_counts()
        .reset_index(name="count")
    )
    if not type_metier_df[type_metier_df["count"] > 1].empty:
        logger.error(
            "Ces indicateurs ont le même type métier pour plusieurs composantes: %s",
            type_metier_df[type_metier_df["count"] > 1],
        )

    # check coherence acronyme/variable
    accro_mask = metadata_df.apply(
        lambda x: str(x["acronyme"]).lower().replace(".", "")
        in x["variable"].replace("_", ""),
        axis=1,
    )

    incoherent_variables = metadata_df[
        (~accro_mask)
        & (~metadata_df["acronyme"].isna())
        & (metadata_df["acronyme"] != "DTN2")
    ]

    if not incoherent_variables.empty:
        logger.error(
            "Incohérences entre les noms de variables et les acronymes: %s",
            incoherent_variables[["acronyme", "variable"]],
        )

    logger.info("Validation du fichier de meta-data terminée")


TYPE_METIER_MAPPING = {
    "ic_haut": "intervalle_haut",
    "ic_bas": "intervalle_bas",
    "res": "resultat",
    "evol": "evolution",
    "moy_reg": "moyenne_regionale",
    "moy_nat": "moyenne_nationale",
    "fourchette": "intervalle",
    "alerte_sup": "status",
    "classe": "classe",
    "obligatoire": "obligatoire",
    "ratio": "ratio",
}


# pylint: disable=too-many-locals,too-many-branches,too-many-statements)
def infer_iqss_metadata():
    """
    Éssaie de deviner les métadonnées des nouveaux IQSS QualHAS
    """

    logger.info("Analyse du fichier de metadata")
    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv")

    missing_metadata = get_missing_metadata(metadata_df)

    if not missing_metadata:
        logger.info("Pas de metadonnées à ajouter")
        return

    data = []
    for year, iqss_composante in missing_metadata:
        iqss_d = {
            "ancienne_variable": iqss_composante,
        }
        iqss_composante = iqss_composante.replace("smr", "ssr")

        iqss_d["variable"] = iqss_composante
        iqss_d["annee"] = year - 1

        logger.info("Analyse pour: %s", iqss_composante)
        existing_secteur = metadata_df["secteur"].str.lower().unique().tolist()
        existing_theme = metadata_df["theme"].str.lower().unique().tolist()
        existing_acronyme = (
            metadata_df["acronyme"].str.lower().unique().tolist()
        )

        name_components = iqss_composante.split("_")

        for comp in name_components:
            if comp in existing_secteur:
                iqss_d["secteur"] = comp.upper()
            if comp in existing_theme:
                iqss_d["theme"] = comp.upper()
            if comp in existing_acronyme:
                iqss_d["acronyme"] = comp.upper()

        if iqss_d.get("theme") == "CA":
            iqss_d["secteur"] = "MCO"

        # on a renommé smr en ssr donc on peut faire ce filtre
        existing_df = metadata_df[
            metadata_df["ancienne_variable"] == iqss_composante
        ]
        if not existing_df.empty:
            last_metadata = existing_df.sort_values(
                by="version", ascending=True
            ).to_dict(orient="records")[-1]
            # overrides previous values like annee etc.
            iqss_d = {**last_metadata, **iqss_d}
            logger.info(
                "Indicateur existant trouvé: %s", last_metadata["code"]
            )
            data.append(iqss_d)
            continue

        if not iqss_d.get("version"):
            iqss_d["version"] = year - 1

        # type metier
        for pattern, type_metier in TYPE_METIER_MAPPING.items():
            if not pattern in iqss_composante:
                continue
            iqss_d["type_metier"] = type_metier
            racine = [p for p in iqss_composante.split(pattern, 1) if p][
                0
            ].strip("_")
            secteur = iqss_d["secteur"].lower()
            racine = [p for p in racine.rsplit(secteur, 1) if p][0].strip("_")
            theme = iqss_d.get("theme")
            if theme and theme in racine:
                racine = [p for p in racine.rsplit(theme.lower(), 1) if p][
                    0
                ].strip("_")

            if pattern == "obligatoire":
                iqss_d["acronyme"] = "NA"

            if not iqss_d.get("acronyme"):
                iqss_d["acronyme"] = (
                    racine if "_" not in racine else racine.split("_", 1)[-1]
                ).upper()
            if not iqss_d.get("theme"):
                iqss_d["theme"] = (
                    racine if "_" not in racine else racine.split("_", 1)[0]
                ).upper()

            iqss_d[
                "indicateur"
            ] = f"{iqss_d['secteur']}_{iqss_d['theme']}_{iqss_d['acronyme']}_{iqss_d['version']}"

            iqss_d["code"] = f"{iqss_d['indicateur']}-{racine}_{pattern}"
            break

        # racine indicateur

        # construction du nom de l'indicateur pour voir si c'est une nouvelle composante
        # d'un indicateur existant

        data.append(iqss_d)
    guessed_df = pd.DataFrame.from_dict(data).sort_values(by=["indicateur"])
    final_m_df = pd.concat([metadata_df, guessed_df])
    final_m_df["version"] = final_m_df["version"].astype("Int32")
    final_m_df.to_csv(RESOURCES_IQSS_PATH / "new_metadata.csv", index=False)
