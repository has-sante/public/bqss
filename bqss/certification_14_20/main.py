import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.certification_14_20.constants import RESOURCES_DOMAIN_PATH
from bqss.certification_14_20.data_download import download_data
from bqss.certification_14_20.processing import process_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


# pylint: disable=duplicate-code
def main(download: bool = True):
    """
    Point d'entrée du pipeline de données de certifications
    """
    # download data
    if not download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    process_data()
