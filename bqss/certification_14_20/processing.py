import datetime
import logging
from typing import Final

import pandas as pd
from frictionless import Schema

from bqss.certification_14_20.constants import (
    CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX,
    FINAL_DOMAIN_PATH,
    RAW_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.constants import FINESS_TYPE_GEO, ISO_8859_1, UTF_8
from bqss.data_acquisition_utils import clean_directory

logger: Final[logging.Logger] = logging.getLogger(__name__)

CERTIFICATIONS_DTYPES_DICT: Final[dict] = {
    "finess": "string",
    "nom": "string",
    "cp": pd.Int64Dtype(),
    "ville": "string",
    "typeStructure": "string",
    "courriel": "string",
    "urlSite": "string",
    "longitude": "float64",
    "latitude": "float64",
    "finessEJ": "string",
    "nomEJ": "string",
    "demarche": "int64",
    "typeVisite": "int64",
    "ordreVisite": "int64",
    "dateDebutVisite": "string",
    "dateFinVisite": "string",
    "nomUsuel": "string",
    "finessEP": "string",
    "nomEP": "string",
    "departementEP": "string",
    "typeEP": "string",
    "idNiveauCertification": "int64",
}

THEMATIQUES_DTYPES_DICT: Final[dict] = {
    "demarche": "int64",
    "thematique": "int64",
    "decision": "string",
}

THEMATIQUES_LABELS_DTYPES_DICT: Final[dict] = {
    "id": "int64",
    "Libellé": "string",
}

# all of these columns are not used in QualiScope
UNINTERESTING_COLUMNS: Final[list] = [
    "typeVisite",
    "ordreVisite",
    "dateDebutVisite",
    "dateFinVisite",
    "finessEP",
    "nomEP",
    "departementEP",
    "typeEP",
]

CERTIF_V2014_ID_NIVEAU_KEY: Final[str] = "certif_V2014_id_niveau"
CERTIF_DATE_KEY: Final[str] = "certif_date"
DEMARCHE_ID_KEY: Final[str] = "certif_V2014_id_demarche"


def process_data():
    """
    Traite les données de certifications
    """
    logger.info("Processing data...")

    clean_directory(FINAL_DOMAIN_PATH)

    build_metadata()

    certifications_df = process_certifications()
    thematiques_df = process_thematiques()
    certifications_df.to_csv(
        FINAL_DOMAIN_PATH / "certifications.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    thematiques_df.to_csv(
        FINAL_DOMAIN_PATH / "thematiques.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    certifications_kv_df = format_certifications_to_key_value(
        certifications_df
    )
    certifications_kv_df.to_csv(
        FINAL_DOMAIN_PATH / "certifications_key_value.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )

    logger.info("Processing data OK")


def process_certifications() -> pd.DataFrame:
    """
    Traite les données de certifications
    """
    certifications_df = pd.read_csv(
        RAW_DOMAIN_PATH / "certifications.csv",
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=CERTIFICATIONS_DTYPES_DICT,
        parse_dates=["dateCertification"],
    )

    # remove duplicates : apart from full duplicates, some rows are only
    # different by their visite or EP, which we are not interested in
    len_with_duplicates = len(certifications_df)
    certifications_df.drop_duplicates(
        subset=[
            col
            for col in CERTIFICATIONS_DTYPES_DICT
            if col not in UNINTERESTING_COLUMNS
        ],
        inplace=True,
    )
    logger.debug(
        "%d certifications lines removed as duplicates",
        len_with_duplicates - len(certifications_df),
    )
    return certifications_df


def process_thematiques() -> pd.DataFrame:
    """
    Traite les données des décisions par thématiques
    """
    thematiques_df = pd.read_csv(
        RAW_DOMAIN_PATH / "thematiques.csv",
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=THEMATIQUES_DTYPES_DICT,
    )
    thematiques_labels_df = pd.read_csv(
        RAW_DOMAIN_PATH / "thematiques_labels.csv",
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=THEMATIQUES_LABELS_DTYPES_DICT,
    )
    thematiques_df = pd.merge(
        left=thematiques_df,
        right=thematiques_labels_df,
        how="left",
        left_on="thematique",
        right_on="id",
    )
    # see https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/28
    thematiques_df.drop(
        thematiques_df[thematiques_df["thematique"] == 99].index, inplace=True
    )
    thematiques_df.rename(
        columns={"Libellé": "libelle_thematique"}, inplace=True
    )
    thematiques_df.drop(columns="id", inplace=True)
    return thematiques_df


def format_certifications_to_key_value(
    certifications_df: pd.DataFrame,
) -> pd.DataFrame:
    certifications_df = certifications_df[
        ["finess", "dateCertification", "idNiveauCertification", "demarche"]
    ].copy()
    certifications_df.rename(
        columns={
            "dateCertification": CERTIF_DATE_KEY,
            "idNiveauCertification": CERTIF_V2014_ID_NIVEAU_KEY,
            "demarche": DEMARCHE_ID_KEY,
        },
        inplace=True,
    )
    certifications_kv_df = certifications_df.melt(
        id_vars=["finess"],
        var_name="key",
    )
    # we enforce a year in our key-value file but this information is not
    # meaningful, please refer to the date of certification (key certif_date)
    # see https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/32
    certifications_kv_df.insert(0, "annee", datetime.datetime.now().year)
    certifications_kv_df.insert(2, "finess_type", FINESS_TYPE_GEO)

    certifications_kv_df["value_integer"] = certifications_kv_df[
        certifications_kv_df["key"].isin(
            [CERTIF_V2014_ID_NIVEAU_KEY, DEMARCHE_ID_KEY]
        )
    ]["value"]
    certifications_kv_df["value_integer"] = pd.to_numeric(
        certifications_kv_df["value_integer"],
        errors="raise",
        downcast="integer",
    ).astype("Int64")

    certifications_kv_df["value_date"] = certifications_kv_df[
        certifications_kv_df["key"] == CERTIF_DATE_KEY
    ]["value"]
    certifications_kv_df["value_date"] = pd.to_datetime(
        certifications_kv_df["value_date"],
        errors="raise",
    )

    certifications_kv_df.drop(columns="value", inplace=True)
    return certifications_kv_df


def build_metadata():
    """
    Construit et sauvegarde le fichier de métadonnées CSV du domaine
    certification
    """
    schema = Schema(SCHEMAS_DOMAIN_PATH / "certifications.json")
    id_niveau_certif = schema.get_field("idNiveauCertification")
    date_certif = schema.get_field("dateCertification")
    demarche_certif = schema.get_field("demarche")

    metadata = pd.DataFrame(
        columns=["name", "title", "description", "type"],
        data=[
            [
                CERTIF_V2014_ID_NIVEAU_KEY,
                id_niveau_certif.title,
                id_niveau_certif.description,
                id_niveau_certif.type,
            ],
            [
                CERTIF_DATE_KEY,
                date_certif.title,
                date_certif.description,
                date_certif.type,
            ],
            [
                DEMARCHE_ID_KEY,
                demarche_certif.title,
                demarche_certif.description,
                demarche_certif.type,
            ],
        ],
    )

    thematiques_metadata = pd.read_csv(
        RAW_DOMAIN_PATH / "thematiques_labels.csv",
        delimiter=";",
        encoding=ISO_8859_1,
        dtype=THEMATIQUES_LABELS_DTYPES_DICT,
    )
    thematiques_metadata[
        "name"
    ] = CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX + thematiques_metadata[
        "id"
    ].astype(
        str
    )
    thematiques_metadata[
        "title"
    ] = "Décision finale pour la thématique numéro " + thematiques_metadata[
        "id"
    ].astype(
        str
    )
    thematiques_metadata["description"] = (
        "Décision finale pour la thématique : "
        + thematiques_metadata["Libellé"]
    )
    thematiques_metadata["type"] = "string"
    thematiques_metadata.drop(columns=["id", "Libellé"], inplace=True)

    metadata = pd.concat([metadata, thematiques_metadata])
    metadata.to_csv(
        FINAL_DOMAIN_PATH / "certification_metadata.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
