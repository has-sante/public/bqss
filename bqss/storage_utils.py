import fsspec
import hvac

from . import constants


def get_remote_fs():
    """
    Récupère les identifiants de connexion à minio depuis vault.
    Utilise l'envvar VAULT_TOKEN pour les lire.
    Instancie un filesystem fsspec pour les operations sur le stockage objet
    """

    client = hvac.Client(url=constants.VAULT_HOST)

    sec_data = client.secrets.kv.v2.read_secret("storage/public-minio/bqss")

    key, secret_key = list(sec_data["data"]["data"].values())
    storage_opts = {
        "key": key,
        "secret": secret_key,
        "client_kwargs": {"endpoint_url": constants.MINIO_URL},
    }
    return fsspec.filesystem(
        "s3",
        **storage_opts,
    )
