import hashlib
import logging
import os
from pathlib import Path
from typing import Final

import hvac
import requests

from bqss import constants

logger: Final[logging.Logger] = logging.getLogger(__name__)

API_BASE_URL = "https://www.data.gouv.fr/api/1"


def get_data_gouv_api_key():
    """
    Récupère la clef d'api data_gouv depuis l'envvar `DATA_GOUV_API_KEY` avec un
    fallback sur vault
    """
    api_key = os.getenv("DATA_GOUV_API_KEY")

    if api_key:
        return api_key

    vault = hvac.Client(url=constants.VAULT_HOST)

    sec_data = vault.secrets.kv.v2.read_secret("storage/data-gouv")

    api_key = sec_data["data"]["data"]["API_KEY"]

    return api_key


def update_dataset_metadata(dataset_id, metadata_file_path, api_key):
    """
    Met à jour sur data.gouv.fr la description du dataset passé en paramètre
    avec le contenu du fichier au chemin passé en paramètre
    """
    url = API_BASE_URL + f"/datasets/{dataset_id}/"
    headers = {
        "X-API-KEY": api_key,
    }
    with open(metadata_file_path, encoding="utf-8") as metadata_file:
        metadata = metadata_file.read()
    response = requests.put(
        url,
        headers=headers,
        json={
            "description": metadata,
        },
        timeout=30,
    )
    response.raise_for_status()


def get_file_hash(remote_fs, file_path: Path, blocksize=2**16):
    mhash = hashlib.sha256()
    with remote_fs.open(file_path, "rb") as file_fp:
        while True:
            buf = file_fp.read(blocksize)
            if not buf:
                break
            mhash.update(buf)
    return mhash.hexdigest()


def update_remote_resource_data(
    dataset_id,
    remote_fs,
    environment,
    resource,
    api_key,
):
    """
    Met à jour sur data.gouv.fr la ressource passée en paramètre avec les données
    présentes sur disque au chemin passé en paramètre. À noter que la ressource
    doit avoir été préalablement créée, cette fonction ne réalise qu'un update.
    """
    url = API_BASE_URL + f"/datasets/{dataset_id}/resources/{resource['id']}/"
    headers = {
        "X-API-KEY": api_key,
    }
    data_file = Path(resource["filename"])

    remote_file_path = (
        f"{constants.STORAGE_BUCKET}/{environment}/{data_file.name}"
    )

    body = {
        "title": data_file.name,
        "url": (
            f"{constants.MINIO_URL}/{constants.STORAGE_BUCKET}/"
            f"{environment}/{data_file.name}"
        ),
        "filetype": "remote",
        "filesize": remote_fs.size(remote_file_path),
        "type": resource["type"],
        "format": data_file.suffix.strip("."),
        "description": resource["description"],
        "checksum": {
            "type": "sha256",
            "value": get_file_hash(remote_fs, remote_file_path),
        },
    }
    response = requests.put(url, headers=headers, json=body, timeout=10)
    response.raise_for_status()
