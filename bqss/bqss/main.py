import logging
from typing import Final

from bqss.bqss.processing import process_document_data, process_key_value_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main():
    """
    Point d'entrée du pipeline BQSS. Ce pipeline agrège les sorties des
    pipelines des autres domaines de données constitutifs des données BQSS
    """
    process_key_value_data()
    process_document_data()
