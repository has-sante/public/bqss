import logging
from typing import Final

from bqss.constants import RESOURCES_PATH
from bqss.data_gouv_utils import (
    update_dataset_metadata,
    update_remote_resource_data,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def update_metadata(data_sources: dict, api_key: str):
    """
    Met à jour la description du jeu de données sur la plateforme d'open data
    "Data Gouv" d'après le fichier se trouvant dans
    resources/data_gouv/doc_page_data_gouv.md.
    """
    logger.info("Mise à jour des métadonnées du dataset sur data.gouv.fr...")
    update_dataset_metadata(
        data_sources["dataset"]["id"],
        RESOURCES_PATH / "data_gouv/doc_page_data_gouv.md",
        api_key=api_key,
    )
    logger.info("Mise à jour des métadonnées du dataset sur data.gouv.fr: OK")


def update_open_data(
    data_sources: dict, remote_fs, environment: str, api_key: str
):
    logger.info(
        "Mise à jour des métadonnées des resources sur data.gouv.fr..."
    )
    for resource in data_sources["resources"]:
        logger.info("Updating: %s", resource["filename"])
        update_remote_resource_data(
            data_sources["dataset"]["id"],
            remote_fs,
            environment,
            resource,
            api_key=api_key,
        )
    logger.info(
        "Mise à jour des métadonnées des resources sur data.gouv.fr: OK"
    )
