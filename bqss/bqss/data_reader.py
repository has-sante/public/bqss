from functools import lru_cache

import pandas as pd

from bqss.bqss.constants import (
    KEY_VALUE_CERTIF_21_25_DTYPES_DICT,
    KEY_VALUE_DTYPES_DICT,
    KEY_VALUE_DTYPES_DICT_NO_DATE,
)
from bqss.certification_14_20 import constants as certification_14_20_constants
from bqss.certification_14_20.processing import (
    CERTIFICATIONS_DTYPES_DICT,
    THEMATIQUES_DTYPES_DICT,
)
from bqss.certification_21_25 import constants as certification_21_25_constants
from bqss.constants import UTF_8
from bqss.esatis import constants as esatis_constants
from bqss.finess import constants as finess_constants
from bqss.finess.processing_autorisations_as import FINAL_AS_DTYPES_DICT
from bqss.iqss import constants as iqss_constants
from bqss.sae import constants as sae_constants


@lru_cache
def get_finess() -> pd.DataFrame:
    return pd.read_parquet(
        finess_constants.FINAL_DOMAIN_PATH / "finess.parquet",
    )


@lru_cache
def get_autorisations_as() -> pd.DataFrame:
    autorisations_as = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "autorisations-as.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_AS_DTYPES_DICT,
        parse_dates=["date_export", "date_fin"],
        date_parser=pd.to_datetime,
    )
    return autorisations_as


@lru_cache
def get_certifications_14_20() -> pd.DataFrame:
    certifications = pd.read_csv(
        certification_14_20_constants.FINAL_DOMAIN_PATH / "certifications.csv",
        sep=",",
        encoding=UTF_8,
        dtype=CERTIFICATIONS_DTYPES_DICT,
    )
    return certifications


@lru_cache
def get_certifications_14_20_kv() -> pd.DataFrame:
    certifications_kv = pd.read_csv(
        certification_14_20_constants.FINAL_DOMAIN_PATH
        / "certifications_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT_NO_DATE,
        parse_dates=["value_date"],
    )
    return certifications_kv


@lru_cache
def get_certifications_21_25_kv() -> pd.DataFrame:
    certifications_kv = pd.read_csv(
        certification_21_25_constants.FINAL_DOMAIN_PATH
        / "certifications_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_CERTIF_21_25_DTYPES_DICT,
    )
    return certifications_kv


@lru_cache
def get_thematiques() -> pd.DataFrame:
    thematiques = pd.read_csv(
        certification_14_20_constants.FINAL_DOMAIN_PATH / "thematiques.csv",
        sep=",",
        encoding=UTF_8,
        dtype=THEMATIQUES_DTYPES_DICT,
    )
    return thematiques


@lru_cache
def get_sae() -> pd.DataFrame:
    sae = pd.read_csv(
        sae_constants.FINAL_DOMAIN_PATH / "sae_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )
    return sae


@lru_cache
def get_iqss() -> pd.DataFrame:
    iqss = pd.read_parquet(
        iqss_constants.FINAL_IQSS_PATH / "iqss_key_value.parquet"
    )
    return iqss


@lru_cache
def get_esatis() -> pd.DataFrame:
    esatis = pd.read_parquet(
        esatis_constants.FINAL_ESATIS_PATH / "esatis_key_value.parquet"
    )
    return esatis


@lru_cache
def get_activites_et() -> pd.DataFrame:
    activites_et = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "activites_et_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )
    return activites_et
