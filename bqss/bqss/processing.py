import json
import logging
from typing import Final

import pandas as pd
import tqdm

from bqss.bqss import constants as bqss_constants, data_reader
from bqss.bqss.constants import FINAL_DOMAIN_PATH
from bqss.bqss.processing_document import build_document_database
from bqss.bqss.processing_finess import (
    export_finess_criterias,
    get_scope_finess,
    set_finess_type,
)
from bqss.bqss.processing_metadata import build_metadata, build_nomenclatures
from bqss.bqss.processing_valeurs import build_key_value_table
from bqss.constants import FINESS_TYPE_UNKNOWN, UTF_8
from bqss.data_acquisition_utils import clean_directory
from bqss.finess.processing_autorisations_as import FINAL_AS_DTYPES_DICT

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_key_value_data():
    """Aggrège les données des différents domaines"""
    logger.info("Processing data...")

    clean_directory(FINAL_DOMAIN_PATH)

    valeurs_df = build_key_value_table()
    export_finess_criterias(
        valeurs_df=valeurs_df.copy(deep=True),
        export_path=FINAL_DOMAIN_PATH / "criteres-qualiscope-finess.xlsx",
    )
    finess_df = get_scope_finess(valeurs_df)
    valeurs_df = set_finess_type(valeurs_df, finess_df)

    # clean unknown finess
    unknown_finess_index = valeurs_df["finess_type"] == FINESS_TYPE_UNKNOWN
    logger.info(
        "Les finess suivants ne sont pas référencés et vont être nettoyés : %s",
        valeurs_df[unknown_finess_index]["finess"].unique(),
    )
    valeurs_df = valeurs_df[~unknown_finess_index]

    logger.info("construction des métadonnées")

    metadata_df = build_metadata(valeurs_df)
    logger.info("construction des nomenclatures")
    nomenclatures_df = build_nomenclatures()
    logger.info("Lecture des autorisations")
    autorisation_df = data_reader.get_autorisations_as()

    logger.info("Écriture des fichiers finaux")
    # save data to disk
    finess_df.to_parquet(
        FINAL_DOMAIN_PATH / "finess.parquet",
    )
    finess_df.to_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    autorisation_df.to_csv(
        FINAL_DOMAIN_PATH / "autorisations-as.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    valeurs_df.to_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    metadata_df.to_csv(
        FINAL_DOMAIN_PATH / "metadata.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    metadata_df.to_excel(
        FINAL_DOMAIN_PATH / "metadata.xlsx",
        index=False,
    )
    nomenclatures_df.to_csv(
        FINAL_DOMAIN_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    nomenclatures_df.to_excel(
        FINAL_DOMAIN_PATH / "nomenclatures.xlsx",
        index=False,
    )

    logger.info("Processing data OK")


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        try:
            return json.JSONEncoder.default(self, o)
        except TypeError as exce:
            if isinstance(o, set):
                return list(o)
            if isinstance(o, pd.Timestamp):
                return o.strftime("%Y-%m-%d")
            if pd.isna(o):
                return None
            raise exce


def process_document_data():
    """
    Construit une base document à partir des données clefs-valeurs.
    Un document contient les données d'un ES
    """
    finess_df = pd.read_parquet(
        bqss_constants.FINAL_DOMAIN_PATH / "finess.parquet",
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        dtype=bqss_constants.KEY_VALUE_DTYPES_DICT_NO_DATE,
        parse_dates=["value_date"],
    )
    autorisations_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "autorisations-as.csv",
        dtype=FINAL_AS_DTYPES_DICT,
        parse_dates=["date_maj_autorisation", "date_maj_mise_en_oeuvre"],
    )
    metadata_df = pd.read_csv(FINAL_DOMAIN_PATH / "metadata.csv")
    base_document_l, stats_d = build_document_database(
        finess_df, valeurs_df, metadata_df, autorisations_df
    )

    with (FINAL_DOMAIN_PATH / "base-document-etablissements.jsonl").open(
        "w"
    ) as filep:
        for finess_d in tqdm.tqdm(
            base_document_l, desc="Ecriture de la base document"
        ):
            filep.write(
                f"{json.dumps(finess_d, cls=CustomJSONEncoder, ensure_ascii=False, allow_nan=False)}\n"
            )
    with (FINAL_DOMAIN_PATH / "base-document-statistiques.json").open(
        "w"
    ) as filep:
        json.dump(stats_d, filep, ensure_ascii=False, indent=True)
