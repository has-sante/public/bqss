import datetime
import logging

import pandas as pd

from bqss.bqss import data_reader
from bqss.bqss.constants import (
    CERTIF_14_20_THEMATIQUE_BIOLOGIE,
    CERTIF_14_20_THEMATIQUE_BLOC,
    CERTIF_14_20_THEMATIQUE_DON_ORGANE,
    CERTIF_14_20_THEMATIQUE_ENDOSCOPIE,
    CERTIF_14_20_THEMATIQUE_FIN_DE_VIE,
    CERTIF_14_20_THEMATIQUE_ID_PATIENT,
    CERTIF_14_20_THEMATIQUE_IMAG_INTERV,
    CERTIF_14_20_THEMATIQUE_IMAGERIE,
    CERTIF_14_20_THEMATIQUE_NAISSANCE,
    CERTIF_14_20_THEMATIQUE_RADIOTHERAPIE,
    CERTIF_14_20_THEMATIQUE_URGENCES,
)
from bqss.certification_14_20.constants import (
    CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX,
)
from bqss.constants import FINESS_TYPE_GEO

logger = logging.getLogger(__name__)


def build_key_value_table() -> pd.DataFrame:
    """
    Construit et retourne la table valeurs agrégeant les tables clé-valeurs des
    domaines de données suivants : SAE, IQSS, e-Satis, certification
    """
    sae_df = data_reader.get_sae()
    sae_df = sae_df.drop(columns=["raison_sociale", "finess_ej"])

    iqss_df = get_iqss_key_value(sae_df)
    esatis_df = data_reader.get_esatis()

    certification_14_20_df = get_certification_14_20_key_value()
    certification_21_25_df = data_reader.get_certifications_21_25_kv()

    activites_et = data_reader.get_activites_et()

    valeurs_df = pd.concat(
        [
            sae_df,
            iqss_df,
            esatis_df,
            certification_14_20_df,
            certification_21_25_df,
            activites_et,
        ]
    )
    return valeurs_df


def get_certification_14_20_key_value() -> pd.DataFrame:
    """
    Retourne la version finale des données de certification en clé-valeur, i.e
    la version enrichie de la redescente des décisions de certification par
    thématiques.
    """
    certifications_df = data_reader.get_certifications_14_20()
    certifications_df = certifications_df[["finess", "demarche"]]
    thematiques_df = data_reader.get_thematiques()
    thematiques_df = thematiques_df[["demarche", "thematique", "decision"]]
    # Ce merge distribue toutes les décisions de certification par thématiques
    # sur tous les établissements géo associés à la démarche de certification
    # => il faut donc filtrer les lignes pour lesquelles cette redescente brute
    # n'a pas de sens
    certification_kv_df = pd.merge(
        left=certifications_df,
        right=thematiques_df,
        left_on="demarche",
        right_on="demarche",
        validate="many_to_many",
    )

    sae_df = data_reader.get_sae()
    sae_df = sae_df[sae_df["annee"] == max(sae_df["annee"].unique())]

    # thématique : urgences
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_urg") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_URGENCES,
        rightly_certified_finess,
    )
    # thématique : bloc opératoire
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_bloc") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_BLOC,
        rightly_certified_finess,
    )
    # thématique : radiothérapie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_rth") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_RADIOTHERAPIE,
        rightly_certified_finess,
    )
    # thématique : imagerie interventionnelle
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_neuro_int") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_IMAG_INTERV,
        rightly_certified_finess,
    )
    # thématique : biologie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_bio") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_BIOLOGIE,
        rightly_certified_finess,
    )
    # thématique : imagerie
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "filtre_imag") & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_IMAGERIE,
        rightly_certified_finess,
    )
    # thématique : endoscopie
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                ["blocs_salend", "blocs_blocs_a14", "blocs_blocs_b14"]
            )
        )
        & (sae_df["value_integer"] > 0)
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_ENDOSCOPIE,
        rightly_certified_finess,
    )
    # thématique : salle de naissance
    rightly_certified_finess = sae_df[
        (sae_df["key"] == "perinat_jouacc") & (sae_df["value_integer"] > 0)
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_NAISSANCE,
        rightly_certified_finess,
    )
    # thématique : fin de vie
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                    # SSR
                    "filtre_heb_ssr",
                    "filtre_ssr",
                    # SLD
                    "filtre_heb_sld",
                    # HAD
                    "filtre_had",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_FIN_DE_VIE,
        rightly_certified_finess,
    )
    # thématique : identification du patient
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                    # SSR
                    "filtre_heb_ssr",
                    "filtre_ssr",
                    # PSY
                    "filtre_heb_psy",
                    "filtre_psy",
                    # SLD
                    "filtre_heb_sld",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_ID_PATIENT,
        rightly_certified_finess,
    )
    # thématique : don d'organes et de tissus
    rightly_certified_finess = sae_df[
        (
            sae_df["key"].isin(
                [
                    # MCO
                    "filtre_heb_med",
                    "filtre_heb_chir",
                    "filtre_heb_perinat",
                    "filtre_med",
                    "filtre_chirambu",
                    "filtre_ivgamp",
                ]
            )
        )
        & sae_df["value_boolean"]
    ]["finess"]
    certification_kv_df = filter_wrongly_certified_finess(
        certification_kv_df,
        CERTIF_14_20_THEMATIQUE_DON_ORGANE,
        rightly_certified_finess,
    )

    # we enforce a year in our key-value file but this information is not
    # meaningful, please refer to the date of certification (key certif_date)
    # see https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/32
    certification_kv_df["annee"] = datetime.datetime.now().year
    certification_kv_df["finess_type"] = FINESS_TYPE_GEO
    certification_kv_df[
        "key"
    ] = CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX + certification_kv_df[
        "thematique"
    ].astype(
        str
    )
    certification_kv_df.rename(
        columns={"decision": "value_string"}, inplace=True
    )
    certification_kv_df.drop(columns=["demarche", "thematique"], inplace=True)

    niveaux_certif_df = data_reader.get_certifications_14_20_kv()
    certification_kv_df = pd.concat([niveaux_certif_df, certification_kv_df])
    return certification_kv_df


def filter_wrongly_certified_finess(
    certification_kv_df: pd.DataFrame,
    thematiques: list,
    rightly_certified_finess: pd.DataFrame,
) -> pd.DataFrame:
    """
    Cette fonction filtre les décisions de certification par thématiques pour
    les finess qui se sont incorrectement vu attribuer une décision pour une
    thématique qui ne les concerne pas
    """
    all_certified_finess = certification_kv_df[
        (certification_kv_df["thematique"].isin(thematiques))
    ]["finess"]
    wrongly_certified_finess = all_certified_finess[
        ~all_certified_finess.isin(rightly_certified_finess)
    ]
    certification_kv_df = certification_kv_df.drop(
        certification_kv_df[
            (certification_kv_df["thematique"].isin(thematiques))
            & (certification_kv_df["finess"].isin(wrongly_certified_finess))
        ].index
    )
    return certification_kv_df


# pylint: disable=too-many-locals
def redescente_psy_jur_geo(
    annee: int,
    finess_df: pd.DataFrame,
    iqss_df: pd.DataFrame,
    sae_df: pd.DataFrame,
) -> tuple[pd.DataFrame, pd.Series]:
    """
    Cette fonction procède à la redescente des résultats des IQSS PSY 2022.
    Les IQSS PSY 2022 sont parfois déclarés au niveau juridique, or on a besoin
    d'avoir les résultats au niveau géographique (pour qualiscope).

    On utilise donc la SAE pour redescendre les résultats uniquement sur les FINESS
    géographiques qui ont une activité de Psychiatrie.
    """

    # Étape 1: Sélection des finess juridiques concernés
    psy_finess_mask = (
        (iqss_df["key"].str.match(r"psy_smhosp_.*"))
        & (iqss_df["finess_type"] == "jur")
        & (iqss_df["annee"] == annee)
    )

    jur_psy_df = iqss_df.loc[psy_finess_mask]
    psy_fdf = finess_df[finess_df["num_finess_ej"].isin(jur_psy_df["finess"])]
    # Déduplication car le finess est historisé
    last_jur_psy_fdf = psy_fdf[
        psy_fdf["date_export"] == f"{annee}-12-31"
    ].drop_duplicates(subset="num_finess_ej", keep="last")

    # On récupère l'ensemble des finess géo correspondants au juridiques sélectionnés
    # un `.merge` marcherait mais serait plus verbeux
    final_psy_fdf = psy_fdf[
        (psy_fdf["date_export"].astype("str") + psy_fdf["num_finess_ej"]).isin(
            last_jur_psy_fdf["date_export"].astype("str")
            + last_jur_psy_fdf["num_finess_ej"]
        )
    ]
    nb_finess_psy = final_psy_fdf["num_finess_ej"].nunique()
    if nb_finess_psy > 0:
        logger.info(
            "Il y a %s Finess juridiques avec des IQSS PSY", nb_finess_psy
        )

    # Étape 2: Récupération des données de la SAE correspondant

    # On ne garde que les finess Géo qui ont plus de une journée
    # d'hospitalisation à temps complet en psychiatrie pour l'année concernée
    geo_psy_sae_vdf = sae_df[
        (sae_df["finess"].isin(final_psy_fdf["num_finess_et"]))
        & (
            (
                (sae_df["key"] == "psy_jou_htp_gen")
                | (sae_df["key"] == "psy_jou_htp_inf")
            )
            & (sae_df["value_integer"] > 1)
        )
        & (sae_df["annee"] == annee)
    ]
    final_psy_geo_df = final_psy_fdf[
        final_psy_fdf["num_finess_et"].isin(geo_psy_sae_vdf["finess"])
    ]
    # Certains Finess géographiques changent de finess juridiques,
    # Il faut donc les dédupliquer en gardant la valeure la plus récente
    final_psy_geo_df = final_psy_geo_df.sort_values(
        "date_export"
    ).drop_duplicates("num_finess_et", keep="last")

    if nb_finess_psy > 0:
        logger.info(
            "On redescend les IQSS PSY sur %s Finess géographiques",
            final_psy_geo_df["num_finess_et"].nunique(),
        )

    # Étape 3: Redescente sur les finess géographiques avec activité SAE de psy
    desc_df = jur_psy_df.merge(
        final_psy_geo_df[
            ["num_finess_et", "num_finess_ej", "raison_sociale_et"]
        ],
        left_on=["finess"],
        right_on=["num_finess_ej"],
        how="inner",
    )
    logger.info(
        "On a réussi à redescendre les données de %s Finess juridiques",
        desc_df["num_finess_ej"].nunique(),
    )

    desc_df = desc_df.drop(
        columns=["finess", "finess_type", "raison_sociale", "num_finess_ej"]
    )
    desc_df = desc_df.rename(
        columns={
            "raison_sociale_et": "raison_sociale",
            "num_finess_et": "finess",
        }
    )
    desc_df["finess_type"] = FINESS_TYPE_GEO

    desc_df = desc_df[
        [
            "annee",
            "finess",
            "finess_type",
            "key",
            "value_string",
            "value_integer",
            "value_float",
            "missing_value",
        ]
    ]

    # Étape 4: Gestion des finess juridiques restants:
    no_sae_finess_mapping = {}
    if annee == 2023:
        no_sae_finess_mapping = {
            "110786324": "110785516",
            "190000117": "190000711",
            "500010384": "500000237",
            "810100008": "810002022",
            "940110042": "940000599",
        }
    if annee == 2022:
        no_sae_finess_mapping = {
            "110786324": "110785516",
            "190000117": "190000711",
            "500010384": "500000237",
            "810100008": "810002022",
        }
    if annee == 2021:
        no_sae_finess_mapping = {
            "110786324": "110785516",
            "190000117": "190000711",
            "500010384": "500000237",
            "810100008": "810002022",
        }

    # on veut la liste des EJ ayant des IQSS psy
    # et qui n'ont pas du tout été redescendu sur au moins un finess geo
    # même manuellement
    at_least_one_geo = final_psy_fdf[
        final_psy_fdf["num_finess_et"].isin(desc_df["finess"])
    ]
    remaining_fdf = final_psy_fdf[
        ~final_psy_fdf["num_finess_ej"].isin(at_least_one_geo["num_finess_ej"])
    ]
    missed_by_hardcode = remaining_fdf[
        ~remaining_fdf["num_finess_ej"].isin(no_sae_finess_mapping.keys())
    ]
    if not missed_by_hardcode.empty:
        logger.warning(
            "Les finess juridiques suivants n'ont pas pu être redescendus %s",
            missed_by_hardcode["num_finess_ej"].unique().tolist(),
        )

    no_sae_iqss_df = jur_psy_df.loc[
        jur_psy_df["finess"].isin(no_sae_finess_mapping.keys())
    ].copy()

    no_sae_iqss_df["finess"] = no_sae_iqss_df["finess"].map(
        no_sae_finess_mapping
    )
    no_sae_iqss_df["finess_type"] = FINESS_TYPE_GEO

    # Étape 5: Concaténation des différents IQSS (redescendu et non redescendus)

    desc_df = pd.concat([desc_df, no_sae_iqss_df])

    return desc_df, psy_finess_mask


def get_iqss_key_value(sae_df: pd.DataFrame) -> pd.DataFrame:
    """
    Cette fonction construit le dataframe des iqss en effectuant les redescentes
    nécessaires des valeurs entre les Finess juridiques et les Finess géographiques
    pour les IQSS PSY.
    """
    finess_df = data_reader.get_finess()
    iqss_df = data_reader.get_iqss()
    # 2021
    desc_2021_df, psy_finess_2021_mask = redescente_psy_jur_geo(
        2021, finess_df, iqss_df, sae_df
    )

    logger.info(
        "Redescente 2021 terminée: %s Finess géographiques ciblés",
        desc_2021_df["finess"].nunique(),
    )

    desc_2021_mask = (
        (iqss_df["key"].str.match(r"psy_smhosp_.*"))
        & (iqss_df["finess"].isin(desc_2021_df["finess"]))
        & (iqss_df["annee"] == 2021)
    )

    # 2022
    desc_2022_df, psy_finess_2022_mask = redescente_psy_jur_geo(
        2022, finess_df, iqss_df, sae_df
    )

    logger.info(
        "Redescente 2022 terminée: %s Finess géographiques ciblés",
        desc_2022_df["finess"].nunique(),
    )

    desc_2022_mask = (
        (iqss_df["key"].str.match(r"psy_smhosp_.*"))
        & (iqss_df["finess"].isin(desc_2022_df["finess"]))
        & (iqss_df["annee"] == 2022)
    )
    # 2023
    desc_2023_df, psy_finess_2023_mask = redescente_psy_jur_geo(
        2023, finess_df, iqss_df, sae_df
    )

    logger.info(
        "Redescente 2023 terminée: %s Finess géographiques ciblés",
        desc_2023_df["finess"].nunique(),
    )

    desc_2023_mask = (
        (iqss_df["key"].str.match(r"psy_smhosp_.*"))
        & (iqss_df["finess"].isin(desc_2023_df["finess"]))
        & (iqss_df["annee"] == 2023)
    )

    iqss_df = pd.concat(
        [
            iqss_df[
                (~psy_finess_2021_mask)
                & (~desc_2021_mask)
                & (~psy_finess_2022_mask)
                & (~desc_2022_mask)
                & (~psy_finess_2023_mask)
                & (~desc_2023_mask)
            ],
            desc_2021_df,
            desc_2022_df,
            desc_2023_df,
        ]
    )

    iqss_df = iqss_df.drop(columns=["raison_sociale"])

    return iqss_df
