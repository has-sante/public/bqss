def create_float_df(merged_df, list_idx):
    """
    Créée data_frame avec critère de type float
    """

    list_float = list(merged_df.select_dtypes("float").columns)
    float_df = merged_df[list_float + list_idx]
    float_df = float_df.melt(
        id_vars=list_idx,
        value_vars=list_float,
        var_name="key",
        value_name="value_float",
    )
    float_df.dropna(subset=["value_float"], inplace=True)
    return float_df


def create_object_df(merged_df, list_idx):
    """
    Créée data_frame avec critère de type string
    """

    list_object = list(merged_df.select_dtypes("object").columns)
    list_object = [
        critere for critere in list_object if critere not in list_idx
    ]
    float_df = merged_df[list_object + list_idx]
    str_df = float_df.melt(
        id_vars=list_idx,
        value_vars=list_object,
        var_name="key",
        value_name="value_string",
    )
    str_df.dropna(subset=["value_string"], inplace=True)
    return str_df
