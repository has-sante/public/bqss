import logging

import numpy as np
import pandas as pd

from bqss.bqss import data_reader, metadata_reader
from bqss.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)
from bqss.finess.processing_activites_et import ACTIVITES_MAPPING_DICT

logger = logging.getLogger(__name__)


# pylint: disable=too-many-locals
def get_scope_finess(
    valeurs_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Règles de gestion pour la prise en compte de l'évolution des Finess au cours
    du temps.
    Il y a quatre critères qui sont combinés:
    1. Critère d'activité du finess:
        Ont été actifs dans les deux dernières années, c'est à dire:
            - Tous les établissements de l'export Finess de l'année en cours sont `actifs`
            - Tous les établissements de l'export Finess qui ont fermé dans l'année N-1 sont `actifs`
    2. Critère d'IQSS récent sur le finess
    3. Critère de certification sur le finess:
        - A les bonnes autorisation d'activité
        - A au moins un résultat de certification
    4. Critère de site principal d'une démarche de certification (référentiel v2021)
        - le finess géographique est le site principal d'une démarche de certification dur référentiel 2021

    On combine ces critère de la façon suivante:
    (finess_actif) ET (iqss_recent OU certification_autorisation OU site_principal_demarche)
    """
    finess_df = data_reader.get_finess()
    autorisations_as = get_scope_autorisations_as()
    has_autorisations = finess_df["num_finess_et"].isin(
        autorisations_as["num_finess_et"]
    )

    # le filtre par autorisations AS seul ne permettant pas d'inclure tous les
    # établissements pour lesquels nous avons des indicateurs (tous domaines
    # confondus), il a été élargi pour inclure tous ces établissements
    in_qualiscope = (
        has_autorisations
        | finess_df["num_finess_et"].isin(valeurs_df["finess"])
        | finess_df["num_finess_ej"].isin(valeurs_df["finess"])
    )

    finess_df = finess_df.copy()

    year_n = pd.to_datetime(max(finess_df["date_export"].unique())).year

    # Indicateurs pour déterminer les établissements actifs
    metadata_df = metadata_reader.get_all_metadata()
    certif_data_keys = metadata_df[
        metadata_df["source"].isin(
            ["Certification v2014", "Certification v2021"]
        )
    ]["name"].tolist()

    certif_valeurs_df = valeurs_df.loc[
        (valeurs_df["key"].isin(certif_data_keys))
    ]

    data_names = []
    iqss_data_sources = [
        "IQSS dossier patient",
        "IQSS questionnaire patient",
        "IQSS PMSI",
        "IQSS questionnaire établissement",
    ]
    data_names = metadata_df[metadata_df["source"].isin(iqss_data_sources)][
        "name"
    ].tolist()
    iqss_data_keys = set(data_names)

    # certains finess ne sont pas autorisés mais ont quand même des iqss
    # récents
    iqss_valeurs_df = valeurs_df.loc[
        (valeurs_df["key"].isin(iqss_data_keys))
        # Une valeure manquante est une information en soit, à garder
        # & (valeurs_df["missing_value"].isna())
    ]

    most_recent_iqss_year = iqss_valeurs_df["annee"].max() - 2
    logger.info(
        "Utilisation des IQSS de l'année %s ou plus récent "
        "pour le filtrage des finess actifs",
        most_recent_iqss_year,
    )
    iqss_valeurs_df = iqss_valeurs_df.loc[
        (iqss_valeurs_df["annee"] >= most_recent_iqss_year)
    ]

    has_recent_iqss = finess_df["num_finess_et"].isin(
        iqss_valeurs_df["finess"]
    )

    has_certification = (
        finess_df["num_finess_et"].isin(certif_valeurs_df["finess"])
    ) | (finess_df["num_finess_ej"].isin(certif_valeurs_df["finess"]))

    site_principal_key = [k for k in certif_data_keys if "principal" in k][0]
    site_principal_valeurs = certif_valeurs_df.loc[
        (certif_valeurs_df["key"] == site_principal_key)
        & certif_valeurs_df["value_boolean"]
    ]
    is_site_principal = (
        finess_df["num_finess_et"].isin(site_principal_valeurs["finess"])
    ) | (finess_df["num_finess_ej"].isin(site_principal_valeurs["finess"]))
    # ajout du flag actif_qualiscope indiquant les établissements concernés
    # par Scope Santé cette année
    finess_df["actif_qualiscope"] = False

    finess_df.loc[
        (in_qualiscope)
        & (  # finess de l'année N (ouverts et fermés)
            (finess_df["date_export"] > f"{year_n}-01-01")
            | (  # finess fermés en année N-1
                (f"{year_n - 1}-01-01" < finess_df["date_export"])
                & (finess_df["date_export"] < f"{year_n}-01-01")
                & (finess_df["ferme_cette_annee"])
            )
        )
        & (
            (has_recent_iqss)
            | (has_autorisations & has_certification)
            | (is_site_principal)
        ),
        "actif_qualiscope",
    ] = True

    mask = (finess_df["type_etablissement"] == "CHU") & (
        ~finess_df["raison_sociale_et"].str.contains(
            "CHU|CHRU|C H U|C.H.U.|GHU", na=False
        )
    )

    finess_df.loc[mask, "raison_sociale_et"] = (
        "CHU/" + finess_df.loc[mask, "raison_sociale_et"]
    )

    finess_df.loc[mask, "raison_sociale_longue_et"] = (
        "CHU/" + finess_df.loc[mask, "raison_sociale_longue_et"]
    )

    # ajout du flag dernier_enregistrement indiquant qu'une ligne de donnée est
    # la plus récente pour un établissement donné
    finess_df["dernier_enregistrement"] = False
    finess_df["temp_key"] = finess_df["num_finess_et"].astype(str) + finess_df[
        "date_export"
    ].astype(str)
    latest_finess_df = finess_df.groupby(
        ["num_finess_et"], as_index=False
    ).agg({"date_export": np.max})
    latest_finess_df["temp_key"] = latest_finess_df["num_finess_et"].astype(
        str
    ) + latest_finess_df["date_export"].astype(str)
    finess_df.loc[
        finess_df["temp_key"].isin(latest_finess_df["temp_key"]),
        "dernier_enregistrement",
    ] = True
    finess_df.drop(columns="temp_key", inplace=True)

    return finess_df


def get_scope_autorisations_as() -> pd.DataFrame:
    """
    Les établissements disposant des autorisations d'activités de soins
    suivantes sont affichés dans QualiScope : Médecine, Chirurgie,
    Gynécologie/Obstétrique, Psychiatrie (en hospitalisation complète),
    Soins de longue durée, Soins de suite et de réadaptation et
    Hospitalisation à domicile.
    """
    # first we filter on active autorisations
    autorisations_as = data_reader.get_autorisations_as()
    autorisations_as = autorisations_as[
        autorisations_as["annee_derniere_activite"].astype("float")
        >= autorisations_as["date_export"].max().year
    ]

    # then we filter by activite
    autorisations_as = autorisations_as[
        autorisations_as["activite"].isin(
            [
                1,  # Médecine
                2,  # Chirurgie
                3,  # Gynécologie/obstétrique
                4,  # Psychiatrie
                7,  # Soins de longue durée
                16,  # Nephrologie
                *range(50, 60),  # SSR spécialisés et non spécialisés
            ]
        )
        | (autorisations_as["forme"] == 5)  # Hospitalisation à domicile
    ]
    # Psychiatrie en hospitalisation complète uniquement (forme = 1)
    autorisations_as = autorisations_as.drop(
        autorisations_as[
            (autorisations_as["activite"] == 4)
            & (autorisations_as["forme"] != 1)
        ].index
    )

    return autorisations_as


def set_finess_type(
    valeurs_df: pd.DataFrame,
    finess_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Spécifie le type de finess des lignes clé-valeur en accord avec le
    référentiel finess passé en paramètre. Tous les finess non référencés
    sont considérés comme inconnus dans les données en clé-valeur.
    """
    valeurs_df.loc[
        valeurs_df["finess"].isin(finess_df["num_finess_ej"]), "finess_type"
    ] = FINESS_TYPE_JUR
    valeurs_df.loc[
        valeurs_df["finess"].isin(finess_df["num_finess_et"]), "finess_type"
    ] = FINESS_TYPE_GEO
    valeurs_df.loc[
        ~valeurs_df["finess"].isin(finess_df["num_finess_et"])
        & ~valeurs_df["finess"].isin(finess_df["num_finess_ej"]),
        "finess_type",
    ] = FINESS_TYPE_UNKNOWN
    return valeurs_df


def export_finess_criterias(valeurs_df, export_path):
    """
    Fonction pour explorer les choix possibles dans le filtrage des finess
    """
    # copy to prevent source df mutations
    finess_df = data_reader.get_finess().copy()
    autorisations_as = get_scope_autorisations_as()

    has_autorisations = finess_df["num_finess_et"].isin(
        autorisations_as["num_finess_et"]
    )
    raw_autorisations_as = data_reader.get_autorisations_as()
    raw_autorisations_as = raw_autorisations_as[
        raw_autorisations_as["annee_derniere_activite"].astype("float")
        >= autorisations_as["date_export"].max().year
    ]

    raw_autorisations_as = raw_autorisations_as.replace(
        {"libelle_activite": ACTIVITES_MAPPING_DICT}
    )
    activites = set(ACTIVITES_MAPPING_DICT.values())

    activite_cols = []
    for activite in activites:
        col = f"has_activite_{activite}"
        finess_df[col] = finess_df["num_finess_et"].isin(
            raw_autorisations_as.loc[
                raw_autorisations_as["libelle_activite"] == activite,
                "num_finess_et",
            ]
        )
        activite_cols.append(col)

    # le filtre par autorisations AS seul ne permettant pas d'inclure tous les
    # établissements pour lesquels nous avons des indicateurs (tous domaines
    # confondus), il a été élargi pour inclure tous ces établissements
    finess_df = finess_df[
        has_autorisations
        | finess_df["num_finess_et"].isin(valeurs_df["finess"])
        | finess_df["num_finess_ej"].isin(valeurs_df["finess"])
    ].copy()

    year_n = pd.to_datetime(max(finess_df["date_export"].unique())).year

    # Les finess actifs sont ceux qui ont des iqss recents
    metadata_df = metadata_reader.get_all_metadata()

    data_names = []
    iqss_data_sources = [
        "IQSS dossier patient",
        "IQSS questionnaire patient",
        "IQSS PMSI",
        "IQSS questionnaire établissement",
    ]
    data_names = metadata_df[metadata_df["source"].isin(iqss_data_sources)][
        "name"
    ].tolist()
    iqss_data_keys = set(data_names)

    # certains finess ne sont pas autorisés mais ont quand même des iqss
    # récents
    iqss_valeurs_df = valeurs_df.loc[
        (valeurs_df["key"].isin(iqss_data_keys))
        # Une valeure manquante est une information en soit, à garder
        # & (valeurs_df["missing_value"].isna())
    ]

    most_recent_iqss_year = iqss_valeurs_df["annee"].max() - 2
    iqss_valeurs_df = iqss_valeurs_df.loc[
        (iqss_valeurs_df["annee"] >= most_recent_iqss_year)
    ]

    has_missing_valeurs_df = iqss_valeurs_df.loc[
        (iqss_valeurs_df["missing_value"].notna())
    ]
    not_missing_valeurs_df = iqss_valeurs_df.loc[
        (iqss_valeurs_df["missing_value"].isna())
    ]

    has_recent_iqss = finess_df["num_finess_et"].isin(
        iqss_valeurs_df["finess"]
    )
    finess_df["has_recent_iqss"] = has_recent_iqss
    # `only_missing_iqss` et `non_missing_iqss` peuvent être tous les deux à False
    # Lorsque l'établissement n'a aucun IQSS recent
    finess_df["has_missing_iqss"] = finess_df["num_finess_et"].isin(
        has_missing_valeurs_df["finess"]
    )
    finess_df["only_missing_iqss"] = finess_df["num_finess_et"].isin(
        has_missing_valeurs_df["finess"]
    ) & ~finess_df["num_finess_et"].isin(not_missing_valeurs_df["finess"])
    finess_df["non_missing_iqss"] = finess_df["num_finess_et"].isin(
        not_missing_valeurs_df["finess"]
    )

    c2014_keys = metadata_df[metadata_df["source"] == "Certification v2014"][
        "name"
    ].tolist()
    c2021_keys = metadata_df[metadata_df["source"] == "Certification v2021"][
        "name"
    ].tolist()

    c2014_valeurs_df = valeurs_df.loc[(valeurs_df["key"].isin(c2014_keys))]
    c2021_valeurs_df = valeurs_df.loc[(valeurs_df["key"].isin(c2021_keys))]

    has_certification = (
        finess_df["num_finess_et"].isin(c2014_valeurs_df["finess"])
        | finess_df["num_finess_et"].isin(c2021_valeurs_df["finess"])
    ) | (
        finess_df["num_finess_ej"].isin(c2014_valeurs_df["finess"])
        | finess_df["num_finess_ej"].isin(c2021_valeurs_df["finess"])
    )

    site_principal_key = [k for k in c2021_keys if "principal" in k][0]
    site_principal_valeurs = c2021_valeurs_df.loc[
        (c2021_valeurs_df["key"] == site_principal_key)
        & c2021_valeurs_df["value_boolean"]
    ]
    is_site_principal = (
        finess_df["num_finess_et"].isin(site_principal_valeurs["finess"])
    ) | (finess_df["num_finess_ej"].isin(site_principal_valeurs["finess"]))

    finess_df["has_certif"] = has_certification

    finess_df["has_certif_2014"] = finess_df["num_finess_et"].isin(
        c2014_valeurs_df["finess"]
    ) | finess_df["num_finess_ej"].isin(c2014_valeurs_df["finess"])
    finess_df["has_certif_2021"] = finess_df["num_finess_et"].isin(
        c2021_valeurs_df["finess"]
    ) | finess_df["num_finess_ej"].isin(c2021_valeurs_df["finess"])

    finess_df["is_certif_2021_site_principal"] = is_site_principal

    finess_df["actif_annee_n_n-1"] = (
        finess_df["date_export"] > f"{year_n}-01-01"
    ) | (  # finess fermés en année N-1
        (f"{year_n - 1}-01-01" < finess_df["date_export"])
        & (finess_df["date_export"] < f"{year_n}-01-01")
        & (finess_df["ferme_cette_annee"])
    )

    finess_df["has_autorisations"] = has_autorisations

    # ajout du flag actif_qualiscope indiquant les établissements concernés
    # par Scope Santé cette année
    finess_df["actif_qualiscope"] = False

    finess_df.loc[
        (  # finess de l'année N (ouverts et fermés)
            (finess_df["date_export"] > f"{year_n}-01-01")
            | (  # finess fermés en année N-1
                (f"{year_n - 1}-01-01" < finess_df["date_export"])
                & (finess_df["date_export"] < f"{year_n}-01-01")
                & (finess_df["ferme_cette_annee"])
            )
        )
        & (
            (has_recent_iqss)
            | (has_autorisations & has_certification)
            | (is_site_principal)
        ),
        "actif_qualiscope",
    ] = True

    export_df = (
        finess_df[
            [
                "date_export",
                "num_finess_et",
                "raison_sociale_et",
                "raison_sociale_longue_et",
                "libelle_categorie_agregat_et",
                "categorie_agregat_et",
                "libelle_categorie_et",
                "categorie_et",
                "type_etablissement",
                "statut_juridique",
                "actif_qualiscope",
                "actif_annee_n_n-1",
                "has_autorisations",
                "has_recent_iqss",
                "has_missing_iqss",
                "only_missing_iqss",
                "non_missing_iqss",
                "has_certif",
                "has_certif_2014",
                "has_certif_2021",
                "is_certif_2021_site_principal",
            ]
            + activite_cols
        ]
        .sort_values("date_export")
        .drop_duplicates(subset=["num_finess_et"], keep="last")
    )
    export_df.to_excel(export_path)
