import logging

import pandas as pd

from bqss import storage_utils
from bqss.bqss.constants import FINAL_DOMAIN_PATH, KEY_VALUE_DTYPES_DICT
from bqss.constants import STORAGE_BUCKET

logger = logging.getLogger(__name__)


def compare_valeurs(
    source_env, ref_env, export_comp_file
):  # pylint: disable=too-many-locals
    """
    Compare les données de la bqss entre deux sources (local vs prod par exemple)
    """
    remote_fs = storage_utils.get_remote_fs()

    logger.info("Chargement des données source")
    if source_env == "local":
        source_df = pd.read_csv(
            FINAL_DOMAIN_PATH / "valeurs.csv", dtype=KEY_VALUE_DTYPES_DICT
        )
        source_fdf = pd.read_parquet(FINAL_DOMAIN_PATH / "finess.parquet")
    else:
        with remote_fs.open(
            f"{STORAGE_BUCKET}/{source_env}/valeurs.csv"
        ) as fp:
            source_df = pd.read_csv(fp, dtype=KEY_VALUE_DTYPES_DICT)
        with remote_fs.open(
            f"{STORAGE_BUCKET}/{source_env}/finess.parquet"
        ) as fp:
            source_fdf = pd.read_parquet(fp)

    logger.info("Chargement des données de référence")
    if ref_env == "local":
        ref_df = pd.read_csv(
            FINAL_DOMAIN_PATH / "valeurs.csv", dtype=KEY_VALUE_DTYPES_DICT
        )
        ref_fdf = pd.read_parquet(FINAL_DOMAIN_PATH / "finess.parquet")
    else:
        with remote_fs.open(f"{STORAGE_BUCKET}/{ref_env}/valeurs.csv") as fp:
            ref_df = pd.read_csv(fp, dtype=KEY_VALUE_DTYPES_DICT)
        with remote_fs.open(
            f"{STORAGE_BUCKET}/{ref_env}/finess.parquet"
        ) as fp:
            ref_fdf = pd.read_parquet(fp)

    source_q_finess = set(
        source_fdf[source_fdf["actif_qualiscope"]]["num_finess_et"].unique()
    )
    ref_q_finess = set(
        ref_fdf[ref_fdf["actif_qualiscope"]]["num_finess_et"].unique()
    )

    no_longer_active_finess = ref_q_finess.difference(source_q_finess)
    newly_added_finess = source_q_finess.difference(ref_q_finess)
    kept_finess = ref_q_finess.intersection(source_q_finess)

    logger.info(
        "%s finess ne sont plus présent dans la nouvelle base, ex: %s",
        len(no_longer_active_finess),
        list(no_longer_active_finess)[:1],
    )
    logger.info(
        "%s nouveaux finess sont ajoutés dans la nouvelle base, ex: %s",
        len(newly_added_finess),
        list(newly_added_finess)[:1],
    )

    # Finess modifiés

    k_source_df = source_df.loc[source_df["finess"].isin(kept_finess)]
    k_ref_df = ref_df.loc[ref_df["finess"].isin(kept_finess)]

    # prendre la dernière année pour chaque finess
    k_source_df = (
        k_source_df.sort_values(["annee", "finess"])
        .drop_duplicates(["finess", "key"], keep="last")
        .sort_values("finess")
    )
    k_ref_df = (
        k_ref_df.sort_values(["annee", "finess"])
        .drop_duplicates(["finess", "key"], keep="last")
        .sort_values("finess")
    )

    # Changements d'activités
    # Changements d'IQSS

    merged_df = k_source_df.merge(
        k_ref_df,
        left_on=["finess", "key", "finess_type"],
        right_on=["finess", "key", "finess_type"],
        suffixes=["_source", "_ref"],
        how="outer",
        indicator=True,
    )

    logger.info(
        "Répartition des changements sur les finess restant dans la base: %s",
        merged_df["_merge"]
        .value_counts()
        .reset_index()
        .replace(
            {
                "_merge": {
                    "left_only": "nouvelles clefs",
                    "right_only": "clefs supprimées",
                    "both": "clefs conservées",
                }
            }
        ),
    )

    logger.info(
        "Clefs ajoutés (nb finess concernés): %s",
        merged_df[merged_df["_merge"] == "left_only"]["key"].value_counts(),
    )

    logger.info(
        "Clefs supprimés (nb finess concernés): %s",
        merged_df[merged_df["_merge"] == "right_only"]["key"].value_counts(),
    )

    only_changed_df = merged_df.loc[
        (merged_df["_merge"] == "both")
        & (
            (
                merged_df["value_integer_source"].notna()
                & (
                    merged_df["value_integer_source"]
                    != merged_df["value_integer_ref"]
                )
            )
            | (
                merged_df["value_string_source"].notna()
                & (
                    merged_df["value_string_source"]
                    != merged_df["value_string_ref"]
                )
            )
            | (
                merged_df["value_boolean_source"].notna()
                & (
                    merged_df["value_boolean_source"]
                    != merged_df["value_boolean_ref"]
                )
            )
            | (
                merged_df["value_date_source"].notna()
                & (
                    merged_df["value_date_source"]
                    != merged_df["value_boolean_ref"]
                )
            )
            | (
                merged_df["missing_value_source"].notna()
                & (
                    merged_df["missing_value_source"]
                    != merged_df["missing_value_ref"]
                )
            )
        )
    ]

    logger.info(
        "Clefs avec des modifications de valeurs (nb finess concernés): %s",
        only_changed_df["key"].value_counts(),
    )

    if not export_comp_file:
        return
    with open(
        f"{FINAL_DOMAIN_PATH}/comparaison-{source_env}-{ref_env}.xlsx", "wb"
    ) as fp:
        with pd.ExcelWriter(  # pylint: disable=abstract-class-instantiated
            fp
        ) as writer:
            # Chaque datafrmae correspond à un onglet du fichier excel
            only_changed_df.to_excel(
                writer,
                sheet_name="Changements de valeurs",
                index=False,
            )

            merged_df[merged_df["_merge"] == "left_only"].to_excel(
                writer,
                sheet_name="Ajouts de valeurs",
                index=False,
            )

            merged_df[merged_df["_merge"] == "right_only"].to_excel(
                writer,
                sheet_name="Suppresion de valeurs",
                index=False,
            )
