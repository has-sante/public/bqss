import logging
import shutil
from pathlib import Path
from typing import Final

import pandas as pd
import yaml

logger: Final[logging.Logger] = logging.getLogger(__name__)


def load_data_sources_file(data_sources_path: Path):
    """
    Charge le fichier de sources de données fourni en paramètre.

    Les fichiers de sources de données sont des fichiers YML contenant des
    métadonnées (principalement des URLs) à propos des sources de données
    utilisées pour construire la base de données BQSS.
    """
    file_path = data_sources_path / "data_sources.yml"
    with open(file_path, encoding="UTF-8") as data_sources_file:
        data_sources = yaml.safe_load(data_sources_file)
    return data_sources


def clean_directory(path: Path):
    """
    Efface et reconstruit l'arborescence du dossier en paramètre
    """
    shutil.rmtree(path, ignore_errors=True)
    path.mkdir(parents=True)


def read_file(file_path, decimal_sep=".", na_values=None):
    """
    Lis en pandas les fichiers raw provenant d'opendata
    """
    # lire le fichier
    if file_path.suffix in [".xlsx", ".xlx"]:
        logger.debug("Le fichier %s est un excel ", file_path)
        return pd.read_excel(
            file_path,
            dtype={
                "finess": "string",
                "nom_ES": "string",
            },
            na_values=na_values,
        )
    params = {
        "sep": ";",
        "index_col": False,
        "encoding": "Latin-1",
        "decimal": decimal_sep,
        "dtype": {
            "finess": "string",
            "raison_sociale": "string",
            "Raison_sociale": "string",
        },
    }
    if na_values is not None:
        params["na_values"] = na_values

    iqss_df = pd.read_csv(file_path, **params)
    if "," in iqss_df.columns.tolist()[0]:
        params["sep"] = ","
        iqss_df = pd.read_csv(file_path, **params)

    return iqss_df
