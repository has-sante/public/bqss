import logging
from typing import Final

from bqss import data_acquisition_utils
from bqss.esatis.constants import (
    CLEAN_ESATIS_PATH,
    FINAL_ESATIS_PATH,
    RESOURCES_ESATIS_PATH,
    SCHEMAS_ESATIS_PATH,
)
from bqss.esatis.data_acquisition import download_data
from bqss.esatis.processing_clean import clean_data
from bqss.esatis.processing_merge import merge_data
from bqss.esatis.processing_reformate import reformate_historized
from bqss.metadata_utils import table_schema_to_csv

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(download=True):
    """
    Point d'entrée de tout le pipeline esatis
    """
    # acquisition des données
    if not download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_ESATIS_PATH
        )
        download_data(data_sources)

    # process des données

    logger.info("Processing data...")
    for directory in [CLEAN_ESATIS_PATH, FINAL_ESATIS_PATH]:
        data_acquisition_utils.clean_directory(directory)

    clean_data()
    logger.info("cleaned directory")

    merge_data(missing_files=False)
    merge_data(missing_files=True)

    logger.info("merged directory")

    # genere un fichier metadata à partir du table schema
    table_schema_to_csv(
        SCHEMAS_ESATIS_PATH / "esatis.json",
        FINAL_ESATIS_PATH / "esatis_metadata.csv",
    )
    logger.info("created metadata for esatis")

    # reformate le merge pour avoir un fichier en clé valeur
    reformate_historized()

    logger.info("data processing done")
