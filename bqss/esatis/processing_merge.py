import pandas as pd

from bqss.esatis.constants import CLEAN_ESATIS_PATH, FINAL_ESATIS_PATH

FINAL_COLUMNS = [
    "finess_geo",
    "finess_pmsi",
    "finess_ej",
    "annee",
    "nb_rep_score_all_rea_ajust",
    "score_all_rea_ajust",
    "score_all_rea_ajust_dp",
    "score_ajust_esatis_region_48h",
    "score_ajust_esatis_type_48h",
    "classement_48h",
    "evolution_48h",
    "nb_rep_score_accueil_rea_ajust",
    "score_accueil_rea_ajust",
    "nb_rep_score_pecinf_rea_ajust",
    "score_pecinf_rea_ajust",
    "nb_rep_score_pecmed_rea_ajust",
    "score_pecmed_rea_ajust",
    "nb_rep_score_chambre_rea_ajust",
    "score_chambre_rea_ajust",
    "nb_rep_score_repas_rea_ajust",
    "score_repas_rea_ajust",
    "nb_rep_score_sortie_rea_ajust",
    "score_sortie_rea_ajust",
    "taux_reco_brut_48h",
    "nb_reco_brut_48h",
    "nb_rep_score_all_ajust_ca",
    "score_all_ajust_ca",
    "score_all_ajust_dp_ca",
    "score_ajust_esatis_region_ca",
    "score_ajust_esatis_type_ca",
    "classement_ca",
    "evolution_ca",
    "score_avh_ajust",
    "nb_rep_score_avh_ajust",
    "score_acc_ajust",
    "nb_rep_score_acc_ajust",
    "score_pec_ajust",
    "nb_rep_score_pec_ajust",
    "score_cer_ajust",
    "nb_rep_score_cer_ajust",
    "score_ovs_ajust",
    "nb_rep_score_ovs_ajust",
    "taux_reco_brut_ca",
    "nb_reco_brut_ca",
    "nb_rep_score_all_ssr_ajust",
    "score_all_ssr_ajust",
    "score_all_ssr_ajust_dp",
    "score_ajust_esatis_region_ssr",
    "score_ajust_esatis_type_ssr",
    "classement_ssr",
    "evolution_ssr",
    "score_accueil_ssr_ajust",
    "nb_rep_score_accueil_ssr_ajust",
    "score_pec_ssr_ajust",
    "nb_rep_score_pec_ssr_ajust",
    "score_lieu_ssr_ajust",
    "nb_rep_score_lieu_ssr_ajust",
    "score_repas_ssr_ajust",
    "nb_rep_score_repas_ssr_ajust",
    "score_sortie_ssr_ajust",
    "nb_rep_score_sortie_ssr_ajust",
    "taux_reco_brut_ssr",
    "nb_reco_brut_ssr",
]


def merge_data(missing_files=False):
    """
    Merge les fichiers et sauvegarde l'historisation d'e-satis

    missing_files: flag pour indiquer si on génère les fichiers de valeurs manquantes
    """

    list_common_columns = [
        "finess_geo",
        "finess_ej",
        "annee",
    ]

    merged_dfs = []
    for year_data_path in sorted(CLEAN_ESATIS_PATH.iterdir()):
        files = []

        for file_path in sorted(year_data_path.iterdir()):
            if not (year_data_path / file_path).is_file():
                continue
            if (missing_files and "missing" in str(file_path)) or (
                not missing_files and "missing" not in str(file_path)
            ):
                files.append(file_path)

        dfs = [pd.read_parquet(file_path) for file_path in files]

        if len(dfs) >= 2:
            year_merged_df = pd.concat(
                [df.set_index(list_common_columns) for df in dfs],
                axis=1,
                join="outer",
            ).reset_index()
        else:
            year_merged_df = dfs[0]

        merged_dfs.append(year_merged_df)
    merged_df = pd.concat(merged_dfs, ignore_index=True)

    if not merged_df.columns.difference(FINAL_COLUMNS).empty:
        raise NotImplementedError(
            "Colonnes inconnues dans le fichier de sortie"
        )
    merged_df = merged_df[FINAL_COLUMNS]
    merged_df.to_parquet(
        FINAL_ESATIS_PATH
        / f"{'missing_' if missing_files else ''}esatis.parquet"
    )
