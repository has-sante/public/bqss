import logging
from typing import Final

import numpy as np
import pandas as pd

from bqss.esatis.constants import (
    FINAL_ESATIS_PATH,
    MISSING_VALUES_DICT,
    NON_CALCULABLE_MISSING_VALUE,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)

ORDERED_COLUMNS = [
    "finess",
    "finess_type",
    "annee",
    "key",
    "value_string",
    "value_integer",
    "value_float",
    "missing_value",
]


def create_value_df(merged_df, index_columns, source_type, target_type):
    typed_columns = list(merged_df.select_dtypes(source_type).columns)
    typed_columns = [
        column for column in typed_columns if column not in index_columns
    ]
    value_df = merged_df[typed_columns + index_columns]
    value_df = value_df.melt(
        id_vars=index_columns,
        value_vars=typed_columns,
        var_name="key",
        value_name=f"value_{target_type}",
    )
    value_df.dropna(subset=[f"value_{target_type}"], inplace=True)
    value_df[f"value_{target_type}"] = value_df[f"value_{target_type}"].astype(
        source_type
    )
    return value_df


def _map_evolution_variables(typed_df):
    """
    Convertit les valeurs des variables d'évolution vers un format commun
    """
    evol_mask = typed_df["key"].str.contains("evol")
    negative_mask = evol_mask & (typed_df["value_string"] == "1-Diminution")
    stable_mask = evol_mask & (typed_df["value_string"] == "2-Stable")
    positive_mask = evol_mask & (typed_df["value_string"] == "3-Amélioration")
    missing_mask = evol_mask & (typed_df["value_string"] == "4-Non calculable")

    typed_df.loc[stable_mask, "value_integer"] = 0
    typed_df.loc[stable_mask, "value_string"] = np.nan

    typed_df.loc[positive_mask, "value_integer"] = 1
    typed_df.loc[positive_mask, "value_string"] = np.nan

    typed_df.loc[negative_mask, "value_integer"] = -1
    typed_df.loc[negative_mask, "value_string"] = np.nan

    typed_df.loc[missing_mask, "missing_value"] = NON_CALCULABLE_MISSING_VALUE
    typed_df.loc[missing_mask, "value_string"] = np.nan

    return typed_df


def reformate_historized():
    """
    Merge les fichiers et sauvegarde l'historisation d'e-satis
    """
    historized_esatis = pd.read_parquet(FINAL_ESATIS_PATH / "esatis.parquet")
    missing_historized_esatis = pd.read_parquet(
        FINAL_ESATIS_PATH / "missing_esatis.parquet"
    )

    historized_esatis[["finess_geo", "finess_ej", "finess_pmsi"]].astype(
        dtype=str
    )

    index_columns = ["finess_geo", "finess_ej", "finess_pmsi", "annee"]
    df_str = create_value_df(
        historized_esatis, index_columns, "object", "string"
    )
    df_float = create_value_df(
        historized_esatis, index_columns, "float", "float"
    )
    df_integer = create_value_df(
        historized_esatis, index_columns, "Int64", "integer"
    )
    reformate_df = pd.merge(
        df_float,
        df_str,
        how="outer",
        on=["finess_geo", "finess_ej", "finess_pmsi", "annee", "key"],
    )
    reformate_df = pd.merge(
        reformate_df,
        df_integer,
        how="outer",
        on=["finess_geo", "finess_ej", "finess_pmsi", "annee", "key"],
    )
    reformate_df.drop_duplicates(inplace=True)
    if reformate_df["finess_geo"].isnull().any():
        raise ValueError("Il existe des finess géographique à null")

    reformate_missing_df = missing_historized_esatis.melt(
        id_vars=index_columns,
        value_vars=missing_historized_esatis.columns.difference(index_columns),
        var_name="key",
        value_name="missing_value",
    )
    reformate_missing_df["missing_value"].replace(
        regex=MISSING_VALUES_DICT, inplace=True
    )

    reformate_df = pd.concat([reformate_df, reformate_missing_df])
    reformate_df = reformate_df.sort_values(
        ["finess_geo", "annee", "key"]
    ).dropna(
        subset=[
            "value_float",
            "value_string",
            "value_integer",
            "missing_value",
        ],
        how="all",
    )

    reformate_df["finess_type"] = "geo"
    reformate_df.rename(columns={"finess_geo": "finess"}, inplace=True)

    reformate_df.drop(
        columns=["finess_ej", "finess_pmsi"], inplace=True, errors="ignore"
    )

    reformate_df = reformate_df[ORDERED_COLUMNS]

    reformate_df = _map_evolution_variables(reformate_df)
    reformate_df.to_parquet(
        FINAL_ESATIS_PATH / "esatis_key_value.parquet",
    )
