import logging
from typing import Final

from bqss.data_acquisition_utils import read_file
from bqss.esatis.constants import (
    CLEAN_ESATIS_PATH,
    MISSING_VALUES_DICT,
    RAW_ESATIS_PATH,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def clean_columns(esatis_df, file_path, source_name):
    """
    Nettoie les colonnes des fichiers
    """
    # Colonnes dont le nom est le même entre les différent secteurs (ca, 48h, ssr)
    # on les suffixes par le secteur pour éviter un écrasement
    common_columns = [
        "taux_reco_brut",
        "nb_reco_brut",
        "classement",
        "evolution",
        "score_ajust_esatis_type",
        "score_ajust_esatis_region",
    ]

    columns_to_specify = [
        "score_all_ajust",
        "score_all_ajust_dp",
        "nb_rep_score_all_ajust",
    ]

    colums_2_drop = [
        "participation",
        "depot",
        "rs_finess",
        "rs_finess_geo",
        "region",
        "type",
        "region_id",
        "",
    ]
    ext = "_ssr"
    if "ssr" not in file_path.name:
        ext = "_" + file_path.name.split("_")[1].split(".")[0]

    esatis_df.columns = [column.lower() for column in esatis_df.columns]
    # drop useless columns
    esatis_df.drop(columns=colums_2_drop, inplace=True, errors="ignore")
    # add year data
    year = int(source_name.name.split("_")[-1])
    esatis_df["annee"] = year
    # rename columns names and set good type
    esatis_df.rename(
        columns={"finess": "finess_ej", "finess_plage": "finess_pmsi"},
        inplace=True,
        errors="ignore",
    )

    # harmonisation des noms des colonnes, en 2016 on n'a que des données 48h+
    if year == 2016:
        col_mapping_2016 = {
            "score_all_ajust": "score_all_rea_ajust",
            "nb_rep": "nb_rep_score_all_rea_ajust",
        }
        esatis_df.rename(
            columns=col_mapping_2016,
            inplace=True,
        )

    for column_name in common_columns + columns_to_specify:
        esatis_df = esatis_df.rename(
            columns={column_name: column_name + ext}, errors="ignore"
        )

    return esatis_df


def clean_values(esatis_df):
    """
    Nettoie les colonnes des fichiers
    """
    missing_value_cols = ["classement_48h", "classement_ca", "classement_ssr"]

    columns_string_list = missing_value_cols + [
        "evolution_48h",
        "evolution_ca",
        "evolution_ssr",
        "finess_ej",
        "finess_geo",
        "finess_pmsi",
    ]
    columns_float_list = [
        "nb_rep_score_all_rea_ajust",
        "score_acc_ajust",
        "score_accueil_rea_ajust",
        "score_accueil_ssr_ajust",
        "score_all_ajust_ca",
        "score_all_ajust_48h",
        "score_all_rea_ajust",
        "score_all_ssr_ajust",
        "score_avh_ajust",
        "score_cer_ajust",
        "score_chambre_rea_ajust",
        "score_ovs_ajust",
        "score_pec_ajust",
        "score_pecinf_rea_ajust",
        "score_pecmed_rea_ajust",
        "score_repas_rea_ajust",
        "score_sortie_rea_ajust",
        "score_pec_ssr_ajust",
        "score_lieu_ssr_ajust",
        "score_repas_ssr_ajust",
        "score_sortie_ssr_ajust",
        "taux_reco_brut_48h",
        "taux_reco_brut_ca",
        "taux_reco_brut_ssr",
    ]

    missing_value_col = [
        column for column in missing_value_cols if column in esatis_df.columns
    ][0]

    columns_2_string = [
        column for column in columns_string_list if column in esatis_df.columns
    ]
    columns_2_float = [
        column for column in columns_float_list if column in esatis_df.columns
    ]
    columns_2_int = [
        column for column in esatis_df.columns if column.startswith("nb_")
    ]
    if columns_2_string:
        esatis_df[columns_2_string] = esatis_df[columns_2_string].astype(
            "object"
        )
    if columns_2_float:
        esatis_df[columns_2_float] = esatis_df[columns_2_float].astype(float)
    if columns_2_int:
        esatis_df[columns_2_int] = esatis_df[columns_2_int].astype("Int64")

    value_cols = columns_2_float + columns_2_int
    missing_mask = esatis_df[missing_value_col].isin(
        MISSING_VALUES_DICT.keys()
    )
    missing_df = esatis_df.loc[missing_mask].copy()
    for col in value_cols:
        missing_df[col] = missing_df[missing_value_col]

    esatis_df = esatis_df.loc[~missing_mask]

    return esatis_df, missing_df


def clean_data():
    """
    Nettoyer les fichiers esatis
    """

    for year_data_path in sorted(RAW_ESATIS_PATH.iterdir()):
        files = [
            file_path
            for file_path in sorted(year_data_path.iterdir())
            if (year_data_path / file_path).is_file()
        ]
        for file_path in files:
            source_name = CLEAN_ESATIS_PATH / year_data_path.name
            # créer un sous dossier dans clean si il n'existe pas
            if not source_name.exists():
                source_name.mkdir(parents=True)

            decimal_sep = "."
            na_values = "."
            if year_data_path.stem == "2019":
                decimal_sep = ","
            # read file
            esatis_df = read_file(
                file_path, decimal_sep=decimal_sep, na_values=na_values
            )
            # clean file
            esatis_df = clean_columns(esatis_df, file_path, source_name)
            esatis_df, missing_df = clean_values(esatis_df)
            # save file
            name = file_path.name.split(".")[0] + ".parquet"

            esatis_df.to_parquet(source_name / name)
            missing_df.to_parquet(source_name / f"missing_{name}")
