# Création

Cette section regroupe l'ensemble de la documentation décrivant le processus de création de la base, et en particulier les traitements des différents domaines la constituant.

```{toctree}
   :maxdepth: 1
   :glob:

   Description générale du processus <data-processing-description>
   Domaines <domaines>
   Schémas tables intermédiaires <schemas-intermediaires>
```
