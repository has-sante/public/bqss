# IQSS

La HAS développe, avec les professionnels, des **I**ndicateurs de **Q**ualité et de **S**écurité des **S**oins (IQSS), utilisés par les établissements de santé comme outils d’amélioration de la qualité des soins et de la sécurité des patients.
Outre l’usage en termes de pilotage interne, ils sont aussi utilisés pour répondre à l’exigence de transparence portée par les usagers et aider à la décision et au pilotage des politiques d’intervention à l’échelon régional et national.

Ces IQSS sont construits grâce à des données collectées de quatre façons :

- les `questionnaires patient` correspondant aux enquêtes e-Satis (voir [domaine e-Satis](esatis.md))
- les données tirées des `dossiers patients` couvrant la majorité des indicateurs
- les `questionnaires établissement`
- les données tirées du `PMSI`

Une partie des IQSS est diffusée publiquement.
Tous les IQSS d'une campagne donnée sont regroupés sur la même page data-gouv.
Par exemple pour 2021, ils sont accessibles sur [cette page](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2021/).

Au niveau de la BQQS, on sépare ces IQSS en deux groupes:

- les IQSS issus d'e-Satis (Questionnaires satisfaction patient)
- les IQSS issus de QualHas (Plateforme de recueil des indicateur):
    - `dossiers patients`
    - `questionnaires établissement`
    - `PMSI`

Les raisons de cette séparation sont multiples:

- Les données proviennent de système d'information distincts
- Les données sont fournies dans des fichiers distincts
- La nature et la structure des données est différente:
  - ESATIS: les IQSS ont une forme stable et sont recueillis tous les ans.
  - QualHAS: les IQSS ont une forme variable. Les IQSS recueillis varient d'une année à une autre.

Vous trouverez ci-dessous les pages dédiées à chacun de ces groupes d'IQSS.

```{toctree}
   :maxdepth: 1

esatis
qualhas
```
