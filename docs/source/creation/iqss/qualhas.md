# IQSS QualHas

Cette page concerne tous les IQSS **hors e-Satis**.
Une page dédiée aux IQSS e-Satis est disponible [ici](./esatis.md).

## Description Générale

Les IQSS QualHas sont obtenus grâce à l’interface QualHas (plateforme de recueil des indicateurs).
La BQSS consolide l'ensemble des données traitées dans un fichier suivant le format clé-valeur.
Le schéma des données produite pat la BQSS est décrit sur [cette page](../../schemas/iqss/iqss_key_value.md).

Les IQSS ont plusieurs composantes (par exemple un numérateur et un dénominateur).
En règle générale, IQSS a une composante _"résultat"_ et une composante _"classe"_.
Le _"résultat"_ est une valeur numérique.
La classe est en général `A`, `B` ou `C` (parfois plus).

Pour certains IQSS, d'autres "composantes" sont aussi exposées.
Par exemple, on peut avoir une évolution qui permet de connaître la variation du statut d'un établissement entre deux campagnes de recueil d'un IQSS donné.
Il est aussi possible que les bornes de l'intervalle de confiance soient données.

La BQSS se base sur un fichier de métadonnées (`bqss/resources/iqss/metadata.csv`) pour regrouper les différentes composantes des IQSS.
Ce fichier permet aussi de suivre le "cycle de vie" des IQSS au fil des campagnes.
Il est le point de départ du traitement des IQSS QualHas (voir la [section de présentation](#présentation-du-fichier-csv-de-métadonnées) correspondante).

## Constitution du fichier CSV de métadonnées

Pour expliquer comment le fichier de métadonnées est rempli, on prend l'exemple du recueil des IQSS réalisé en 2019.

Toutefois, les opérations de production du fichier de métadonnées pour ce recueil en particulier ne s'appliquent pas forcément à toutes les années (sinon cette étape serait facilement automatisable).
Comme les IQSS QualHas varient sur la forme et sur le fond d'une année à l'autre, les métadonnées les caractérisant varient elles aussi.
Les variations ne sont pas prévisibles et on est donc obligé de recourir à un travail manuel à chaque publication IQSS.

Les métadonnées à consolider proviennent des données IQSS en open-data (voir [section correspondante](#acquisition-des-données)).
Ici, il s'agit du fichier de données du recueil 2019 sur le site data.gouv.fr accessible via [ce lien](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2019/).

Sur la page du portail, 8 fichiers sont mis à disposition :

- 5 fichiers correspondent aux [IQSS e-satis](esatis.md) (mention de esatis dans le nom du fichier)
- 1 fichier correspond aux fiches descriptive des IQSS "dossiers patients" recueillis en 2019.
- 2 fichiers qui servent justement à remplir les métadonnées :
  - `resultats-iqss-open-data-2019.csv` = les données
  - `lexique-iqss-open-data-2019.csv` = le lexique des variables

> ⚠ en fonction des années, le nombre et le format des fichiers varient.
> Un effort de stabilité est fait depuis quelques années.

> 📝 **Note :**
> Une commande de la `cli`, `cli infer-iqss-metadata`, permet de générer une première version du fichier de metadata lors de l'intégration de nouveaux indicateurs.
> Il faut néanmoins compléter manuellement cette version pour avoir un fichier valide.

### Secteur, Thème et acronyme d'un IQSS

On commence l'enrichissement du fichier de métadonnées en déterminant, pour chaque IQSS:

- son `secteur`: le secteur médical concerné (`MCO`, `PSY`, `HAD` etc.)
- son `thème`: l'axe d'évaluation de l'IQSS (`DPA`, `ETEORTHO`, etc.)
- son `acronyme`: l'acte(s) concerné par l'IQSS (`QLS`, `PCD`, `CV` etc.)

Dans la BQSS on identifie de façon **unique** un IQSS en utilisant ces champs (et la version, voir la [section dédiée](#version-dun-iqss)).
Il est donc important de les déterminer.

Le fichier contenant les données ne contient pas directement d'information sur le `secteur`, le `theme` ou l'`acronyme` ([voir section](#présentation-du-fichier-csv-de-métadonnées) décrivant ces variables).
Pour inférer ces trois champs, il faut utiliser le nom des variables, l'information s'y trouvant.
Par exemple, la variable nommée `res_ias_icsha_v3_psy` dans le fichier d'origine a un nom composé notamment de `psy` pour son secteur, `ias` pour son thème, et `icsha` pour son acronyme.

Une fois ces 3 champs complétés, cette variable est renommée `resultat_icsha` (champ `variable`).
Le nom d'origine est stocké dans le champ `ancienne_variable` dans le fichier de métadonnées.

Cette phase de renommage permet d'harmoniser le nom des variables entre les différentes années.
En effet, certaines composantes des IQSS changent de noms d'une année à l'autre tout en exposant la même information.
La BQSS se charge donc d'harmoniser ces noms pour essayer de garder une cohérence sur plusieurs années de recueil.

### Version d'un IQSS

Une fois cette étape réalisée, on définie la `version` de l'IQSS.

Certains IQSS ont subis des changements au fil des campagnes (`HAD_DPA_TRE` par exemple).
Bien qu'ils évaluent toujours le même acte, la façon dont ils l'évaluent peut évoluer au fil des campagnes.
On utilise donc la notion de `version` d'un IQSS qui permet de tracer la "parentalité" d'un IQSS au fil de son cycle de vie.

Par défaut, un IQSS est considéré comme non comparable d'une année sur l'autre.
Deux versions d'un IQSS sont considérées comme deux **indicateurs distincts**, non comparables par défaut.
On leur affecte donc une `version` différente à chaque nouvelle campagne.

La `version` d'un IQSS est l'année de la collecte des données (pour un recueil de l'année N, il s'agit de l'année N-1 ; en l'occurrence 2018 dans le cas de la variable `res_ias_icsha_v3_psy`).

Néanmoins, pour un IQSS donné, les données de la campagne de l'année en cours peuvent contenir une information d'_évolution_.
Il y a alors présence de la chaîne de caractère `evol` dans le nom d'une de ses composantes, par exemple `evol_dpa_tre_had`.
Dans ce cas, la version de l'IQSS serait celle de la campagne précédente (par exemple la variable `classe_DPA_dtn_HAD` dont l'année est `2018` et le numéro de version est `2017`).

Dans tous les cas, on conserve l'année de collecte dans le champ `annee`.

### `indicateur`: identifiant unique d'un IQSS

Une fois que l'on a déterminé la version, on peut construire un identifiant unique d'un IQSS, contenu dans le champ `indicateur`.
On construit aussi un champ `code` qui est un identifiant unique des _composantes_ d'un IQSS.

Le champ `indicateur` est une clef qui permet de regrouper les différentes composantes d'un indicateur (numérateur, dénominateur etc.).
Il est construit de la façon suivante:

> {secteur}\_{theme}\_{acronyme}\_{version}

Pour `res_ias_icsha_v3_psy`, cela donne donc :

> MHS_IAS_ICSHA_2018

Le champ `code` est constitué comme concaténation des champs `secteur`, `theme`, `acronyme`, `version` et `variable` suivant le pattern suivant :

> {secteur}\_{theme}\_{acronyme}\_{version}-{variable}

Pour `res_ias_icsha_v3_psy`, cela donne donc :

> MHS_IAS_ICSHA_2018-resultat_icsha

### Autres champs

Le champ `libelle` est renseigné grâce au fichier de lexique (il s'agit ici de `lexique-iqss-open-data-2019.csv`).

Le champ `type` est construit sur la base des observations faites des données et permet de préciser le type majoritaire des variables pouvant être :

- `classe`: variable de type catégorielle, sous la forme de strings
- `int-string`: variable entière avec des valeurs manquantes en string
- `int`: variable entière sans valeurs manquantes
- `float-string`: variable float avec des valeurs manquantes en string
- `float`: variable float sans valeur manquante
- `string`: variable en chaîne de caractère

Le champ `url_rapport` a été complété grâce au site de la HAS, via son moteur de recherche avec des mots clés appropriés comme _`ca qls rapport`_ pour le rapport [iqss_rapport_ca_qls_2019.pdf](https://www.has-sante.fr/upload/docs/application/pdf/2019-12/iqss_rapport_ca_qls_2019.pdf) ou bien _`ete ortho`_ pour le rapport [iqss_rapport_ete_ortho_2019.pdf](https://www.has-sante.fr/upload/docs/application/pdf/2019-12/iqss_rapport_ete_ortho_2019.pdf).

Le champ `type_metier` (voir la [description générale](#description-générale) des IQSS)) dénote le type métier de la variable que l'on observe, un indicateur étant composé de plusieurs de ces variables.
Le type métier d'une variable est inféré grâce à son nom qui contient le plus souvent cette information.

Il existe actuellement les types métiers suivants :

- `classe`: si le nom de la variable contient `classe`, il s'agit d'une classe
- `controle`: si le nom de la variable contient `controle`, il s'agit d'un contrôle
- `denominateur` si le nom de la variable contient `den`, il s'agit d'un dénominateur
- `effectif_cible`: si le nom de la variable contient `cible`, il s'agit d'un effectif cible
- `effectif_observe`: si le nom de la variable contient `obs`, il s'agit d'un effectif observé
- `effective_attendu`: si le nom de la variable contient `att`, il s'agit d'un effectif attendu
- `evolution` si le nom de la variable contient `evol`, il s'agit d'une évolution
- `intervalle` si le nom de la variable contient `fourchette`, il s'agit des deux bornes de l'intervalle de confiance sous la forme `[<bas>, <haut>]` par exemple `[38 - 57]`.
- `intervalle_bas`: si le nom de la variable contient `icbas`, il s'agit de la borne basse de l'intervalle de confiance
- `intervalle_haut`: si le nom de la variable contient `ichaut`, il s'agit de la borne haute de l'intervalle de confiance
- `obligatoire`: si le nom de la variable contient `obligatoire`, il s'agit d'un recueil obligatoire, `0` ou `1`
- `positionnement`; si le nom de la variable contient `pos`, il s'agit d'un positionnement
- `resultat_min`: si le nom de la variable contient `c1`, il s'agit d'un résultat minimum
- `resultat_strict`: si le nom de la variable contient `c2`, il s'agit d'un résultat strict
- `resultat`: si la valeur de la variable est la valeur de l'indicateur
- `score`: si le nom de la variable contient `score`, il s'agit d'un score
- `score_arrondi`: si le nom de la variable contient `score` et `dp`, il s'agit d'un score utilisant le bon arrondi pour la diffusion publique
- `ratio`: si le nom de la variable contient `ratio`, il s'agit d'un ratio
- `status`: Défini pour quelques indicateurs avec les services métiers, `0` ou `1`
- `moyenne_nationale`: si le nom de la variable contient `moy_nat`, il s'agit de la moyenne nationale de l'indicateur
- `moyenne_regionale`: si le nom de la variable contient `moy_reg`, il s'agit de la moyenne régionale de l'indicateur

En règle générale, le résultat d'un IQSS a le type métier `resultat`.
Cependant, pour certains IQSS, il n'y a pas de composantes `resultat`.
Dans ce cas, une composante d'un autre type métier constitue le résultat de l'IQSS.
Pour plus d'information:

- issue [#36](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/36))
- issue [#92](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/92#note_52612)

Le champ `commentaire` est un champ libre permettant à l'opérateur d'annoter les données si besoin de plus de précisions sur la base des observations effectuées.

## Acquisition des données

Les données IQSS sont téléchargées depuis le portail [data.gouv.fr](https://www.data.gouv.fr/fr/), où elles sont regroupées par année:

- [recueil 2014](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-source-dossier-patient-recueil-2014/)
- [recueil 2015](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-source-dossier-patient-recueil-2015/)
- [recueil 2016](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-source-dossier-patient-recueil-2016/)
- [recueil 2017](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-source-dossier-patient-recueil-2017/)
- [recueil 2018](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-infections-associees-aux-soins-recueil-2018/)
- [recueil 2019](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2019/)
- [recueil 2020](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/)
- [recueil 2021](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2021/)
- [recueil 2022](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2022/)
- [recueil 2023](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2023/)

Les sources sont exhaustivement répertoriées dans le fichier `resources/iqss/data_sources.yml`.
Les fichiers correspondants sont stockés par année de campagne au format CSV dans le dossier `data/iqss/raw/`.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : Haute Autorité de Santé - service Évaluation et Outils pour la Qualité et la Sécurité des Soins (EvOQSS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Traitements et agrégation des données

Les traitements appliqués aux données se décomposent en 4 étapes décrites dans la présente section.

**Nettoyage des données** : Plusieurs opérations sont menées sur les données d'origine :

- Les colonnes des données annuelles ont été renommées grâce au champ `code` du fichier CSV de métadonnées précédemment constitué afin d’harmoniser les différentes sources.
- Certaines colonnes n'étant pas des variables d'intérêts, elles furent abandonnées (voir la liste `COL_2_DROP` dans le fichier `bqss/iqss/constants.py`).
- Certains champs ayant des formats mixtes (des valeurs numériques stockées sous forme d’entiers et de chaîne de caractères), une uniformisation de ces derniers est effectuée grâce au champ `type` du fichier CSV de métadonnées.

Les fichiers résultants sont stockés dans le dossier `data/iqss/cleaned` par année de collecte.

**Agrégation des données** : Tous les fichiers de données des différentes années sont fusionnés dans le fichier `data/iqss/final/iqss.csv`.

**Ajout du type de FINESS** : Grâce au fichier final du domaine FINESS, une nouvelle colonne est ajoutée au fichier des IQSS agrégé, celle sur le type du FINESS sur le même fichier résultant de l'opération précédente.

**Reformatage du fichier final** : Le fichier `data/iqss/final/iqss.csv` est enfin converti au format clé-valeur dans le fichier `data/iqss/final/iqss_key_value.csv`.
On effectue les opérations suivantes:

- Conversion des données de type `10%` vers leur représentation numérique.
- Remplacement des valeurs manquantes sous la forme de `.`.
- Harmonisation des valeurs d'évolution vers une représentation numérique (`0`: Stable, `-1`: Dégradation, `1`: Amélioration)

## Validation des données

La validation des données des IQSS est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/).

Cette validation s'appuie sur un fichier de métadonnées au format JSON et se trouvant dans le dossier `schemas/iqss` : `iqss_key_value.json`.

## Présentation du fichier CSV de métadonnées

Le fichier de métadonnées des IQSS est un fichier réalisé à la main (voir [section](#constitution-du-fichier-csv-de-métadonnées) décrivant ce processus).

Il contient 13 champs :

- `code` : une combinaison de plusieurs colonnes (`secteur`, `theme`, `acronyme`, `version`, `variable`) permettant d’avoir une unique modalité de la variable en fonction de sa méthode de calcul.
- `secteur` : défini grâce soit au nom de la variable, soit au nom du fichier issu du portail d’Open Data ou soit a été explicité par la HAS. Il en existe 5:
  - **MCO** : **M**édecine, **C**hirurgie, **O**bstétrique,
  - **SSR** : **S**oins de **S**uite et de **R**éadaptation,
  - **HAD** : **H**ospitalisation **A** **D**omicile,
  - **PSY** : **PSY**chiatrie,
  - **MHS** (MCO-HAD-SSR) : secteur créé par la HAS pour les variables calculées de la même façon pour ces 3 secteurs,
  - **TCH** : correspond à "Tout CHamp", c'est à dire qui s'applique à tous les secteurs ci-dessus.
- `theme` : il en existe plusieurs listés ci-dessous et sont définis de la même façon que le secteur (nom de variable, ou nom de fichier ou choix HAS) :
  - **DIA** : qualité de la prise en charge des patients hémo**DIA**lysés chroniques,
  - **IDM** : prise en charge hospitalière de l’**I**nfarctus **D**u **M**yocarde,
  - **DPA** : qualité du **D**ossier **PA**atient,
  - **AVC** : prise en charge initiale de l’**A**ccident **V**asculaire **C**érébral
  - **HPP** : prévention et prise en charge de l’**H**émorragie du **P**ost**P**artum immédiat
  - **ETEORTHO** : **É**vènements **T**hrombo-**E**mboliques sur prothèse en chirurgie **ORTHO**pédique,
  - **IAS** : **I**nfections **A**ssociées aux **S**oins
  - **ISOORTHO** : **I**nfections du **S**ite **O**pératoire en chirurgie **ORTHO**pédique,
  - **CA** : qualité en **C**hirurgie **A**mbulatoire,
  - **RCP** : **R**éunion de **C**oncertation **P**luridisciplinaire en cancérologie,
  - **DAN** : **D**ossier d’**AN**esthésie,
- `acronyme` : créé par la HAS, il sont listés ci-dessous et sont définis de la même façon que le secteur et le thème :
  - **AAT** : **A**ccès **A** la **T**ransplantation,
  - **AQD** : **A**ppréciation de la **Q**ualité de la **D**ialyse,
  - **ASE** : **A**gent **S**timulant de l’**E**rythropoïèse,
  - **BASI** : **B**êtabloquant-**A**ntiagrégant plaquettaire-**S**tatine-**I**nhibiteur de l’enzyme (score agrégé des indicateurs évaluant les prescriptions médicamenteuses),
  - **COORD** : **COOR**dination de la prise en charge,
  - **CPA** : **C**onsultation **P**ost-**A**vc,
  - **DEC** : **D**élai d'**E**nvoi du **C**ourrier,
  - **DEL1** : **DEL**ivrance après un accouchement – niveau **1**,
  - **DHS** : **D**ate et **H**eure de **S**urvenue des symptômes d'avc,
  - **DOC** : **DO**cument de **S**ortie,
  - **DTD** : **D**épistage des **T**roubles de la **D**églutition,
  - **DTN** : **D**épistage des **T**roubles **N**utritionnels,
  - **DTN1** : **D**épistage des **T**roubles **N**utritionnels – niveau **1**,
  - **DTN2** : **D**épistage des **T**roubles **N**utritionnels – niveau **2**,
  - **EAT** : **É**valuation de l'**A**ccès à la **T**ransplantation,
  - **ENV** : **E**xpertise **N**euro**V**asculaire,
  - **EPR1** : **É**valuation par un **P**rofessionnel de la **R**ééducation – niveau **1**,
  - **ETEORTHO** : **É**vènements **T**hrombo-**E**mboliques après pose de prothèse totale de hanche -hors fracture- ou de genou en **ORTHO**pédique,
  - **HYG** : règles **HYG**iéno-diététiques,
  - **ICSHA.2** : **I**ndicateur de **C**onsommation des produits **H**ydro-**A**lcooliques – niveau **2**,
  - **ICSHA** : **I**ndicateur de **C**onsommation des produits **H**ydro-**A**lcooliques,
  - **ICSHA.3** : **I**ndicateur de **C**onsommation des produits **H**ydro-**A**lcooliques – niveau **3**,
  - **ISOORTHO** : **I**nfections du **S**ite **O**pératoire en chirurgie orthopédique en **ORTHO**pédique,
  - **MDD** : **M**esure de la **D**ose de **D**ialyse,
  - **NUT** : suivi **NUT**ritionnel,
  - **PCD** : évaluation et **P**rise en **C**harge de la **D**ouleur,
  - **PECIHPPI** : **P**rise En **C**harge **I**nitiale de l’**HPPI**,
  - **PHO** : surveillance du bilan **PHO**sphocalcique
  - **PSH** : **P**rescription **S**éances et heures **H**ebdomadaires
  - **PSPV** : **P**rojet de **S**oins, **P**rojet de **V**ie
  - **QLS** : **Q**ualité de la lettre de **L**iaison à la **S**ortie,
  - **RCP2** : **P**luridisciplinarité de la **C**oncertation – niveau **2**
  - **SER** : surveillance **SÉR**ologique des hépatites,
  - **SPH** : **S**uivi des **P**atients **H**émodialysés,
  - **SURMIN** : **SUR**veillance **MIN**imale,
  - **TDA** : **T**enue du **D**ossier **A**nesthésique,
  - **TDP** : **T**enue du **D**ossier **P**atient,
  - **TRD** : **TR**açabilité de l’évaluation de la **D**ouleur,
  - **TRE** : **TR**açabilité de l’évaluation du risque d'**E**scarre,
- `version` : correspond à l'année de première collecte de cet IQSS, permettant ainsi de savoir si un IQSS est comparable d’une année sur l’autre.
- `variable` : npm d'une composante d'un IQSS. Les composantes peuvent être renommées afin d’harmoniser le nom des ressources. Les noms d'origine sont conservés dans le champ `ancienne_variable`,
- `libelle` : définition des variables. Elles se retrouvent dans les données issues du portail d'Open Data, en particulier dans les lexiques,
- `annee` : année de collecte de la donnée (les données sont collectées l'année qui précède l'année de publication d'un recueil),
- `ancienne_variable` : correspond à l'ancien nom de la variable se trouvant dans les données issues du portail d'Open Data (nom des colonnes correspondantes),
- `type` : type de la variable (classe, int-string, float, string, int, float-string, nan),
- `commentaire` : compléments d’information concernant les données sur la base d'observations de l'opérateur,
- `url_rapport` : URL du rapport correspond à chaque variable selon l’année de la campagne, du thème et du secteur,
- `type_metier` : typologie de la valeur attendue pour la variable, et pouvant être d'au moins 5 types (classe, dénominateur, évolution, positionnement et résultat ; voir la [description générale](#description-générale) des IQSS). Cette information est parfois manquante.
- `source` : provenance de l'IQSS, peuvent être de trois types pour les IQSS QualHas :
  - IQSS dossier patient
  - IQSS questionnaire établissement
  - IQSS PMSI
