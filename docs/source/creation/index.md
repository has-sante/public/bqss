# Création

Cette section regroupe l'ensemble de la documentation décrivant le processus de création de la base, et en particulier les traitements des différents domaines la constituant.

```{toctree}
   :maxdepth: 1
   :glob:

data-processing-description
domaines
schemas-intermediaires
```
