# Sélection des Finess

Au cours du processus, les données Finess subissent diverses opérations de filtrage/selection.
Ces opérations sont les suivantes:

- Filtrage sur les catégories d'aggrégat
- Filtrage sur les autorisations d'activité et les données de qualité et de sécurité des soins
- Création du filtre `actif_qualiscope`

> 📝 **Note :**
> Les chiffres présentés ci-dessous ont étés extraits le `05/02/2024`.
> Ils évoluent avec le temps et ne seront pas maintenus à jour.
> Le but est d'illustrer les volumes de Finess filtrés par chacun des filtres.

## Base Finess complète

Le Finess historisé compte:

- Tous exports confondus: `1 950 561` enregistrements de Finess Géographiques.
- Exports depuis 2019: `483 839` enregistrements de Finess Géographiques.
- Dernier export (`2023-11-09`): `97 877` enregistrements de Finess Géographiques.

## Filtrage sur les autorisations d'activité et les données de qualité et de sécurité des soins

Dans le [Domaine bqss](../bqss.md#exclusion-de-certains-finess), on garde les Finess qui vérifient ces conditions:

- OU
  - Qui ont les bonnes autorisations, c'est à dire:
    - au moins une autorisations d'activité de soin parmi celles concernées
    - et dont l'année de fin est supérieure ou égale à l'année en cours
  - Qui ont des données dans la table valeurs, c'est à dire:
    - au moins une donnée de la SAE
    - ou au moins une donnée d'IQSS (tout type confondu)
    - ou au moins une donnée de certification (tout référentiel confondu)

Après filtrage, le finess historisé compte:

- Tous exports confondus: `11 864` Finess Géographiques uniques.
- Exports depuis 2019: `9 751` Finess Géographiques uniques.
- Dernier export (`2023-11-09`): `8 956` Finess Géographiques uniques.

## Création du filtre `actif_qualiscope`

Dans le [Domaine bqss](../bqss.md#colonne-actif_qualiscope), on crée une colonne `actif_qualiscope` sur les Finess.
Cette colonne indique si le Finess doit être affiché sur le site QualiScope ou pas.

Pour décider si un Finess géographique est affiché sur le site, on utilise les critères suivants :

1. Critère d'activité du finess: `finess_actif`
   Ont été actifs dans les deux dernières années, c'est à dire:
   - Tous les établissements de l'export Finess de l'année en cours sont `actifs`
   - Tous les établissements de l'export Finess qui ont fermé dans l'année N-1 sont `actifs`
2. Critère d'IQSS récent sur le finess: `iqss_recent`
3. Critère de certification et d'autorisations d'activité sur le finess: `certification_autorisation`
   - A les bonnes autorisation d'activité
   - A au moins un résultat de certification
4. Critère de site principal d'une démarche de certification (référentiel v2021): `site_principal_demarche`
   - le finess géographique est le site principal d'une démarche de certification du référentiel 2021

On combine ces critères de la façon suivante:

```
actif_qualiscope =
  (finess_actif)
  ET (
      iqss_recent
      OU certification_autorisation
      OU site_principal_demarche
    )
```

Ce qui donne sous forme graphique:

```mermaid
flowchart TB
  finess_actif[Finess actif année N ou N - 1]
  iqss_recent[A des IQSS récents]
  has_certification[A un résultat de certification]
  site_principal_demarche[Site principal \n d'une démarche de certification]
  certification_autorisation[A les bonnes autorisations]
  actif_qualiscope[Actif Qualiscope]

  finess_actif --> iqss_recent
  finess_actif --> has_certification

  has_certification --> certification_autorisation
  has_certification --> site_principal_demarche

  iqss_recent --> actif_qualiscope
  site_principal_demarche --> actif_qualiscope
  certification_autorisation --> actif_qualiscope
```

Le filtre ainsi créé sélectionne environ `4330` Finess.
