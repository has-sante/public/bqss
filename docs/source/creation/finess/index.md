# FINESS

## Description générale

Le domaine FINESS est composé de deux sources de données distinctes – les données FINESS et les données d'autorisation – alimentant respectivement deux tables du schéma final.
Par ailleurs, les données d'autorisation sont utilisées pour construire le référentiel des données d'activité.
Ces différentes données sont décrites ci-dessous.

### Données FINESS

FINESS signifie **FI**chier **N**ational des **E**tablissements **S**anitaires et **S**ociaux. Un FINESS est principalement de 2 types, géographique ou juridique, abrégés respectivement en ET et EJ au sein du domaine FINESS :

- FINESS ET : un établissement (ET) correspond à une implantation géographique.
- FINESS EJ : une entité juridique (EJ) correspond à la notion de personne morale.

Les données contenues dans le domaine FINESS regroupent notamment des informations d'identification (raison sociale, numéro FINESS, siret, etc.), de coordonnées (adresse, téléphone, etc.), de codification (APE, MFT, SPH, etc.), de datation et de catégorie d'établissement.

Concernant la géolocalisation des établissements de santé, les données récupérées disposent de coordonnées dans différents systèmes géodésiques selon leur emplacement sur le globe :

- Lambert 93 (RFG93) pour les établissements en France métropolitaine
- UTM N20 (RGAF09) pour la Guadeloupe et la Martinique
- UTM N21 (RGSPM06) pour Saint-Pierre-et-Miquelon
- UTM N22 (RGFG95) pour la Guyane
- UTM S38 (RGM04) pour Mayotte
- UTM S40 (RGR92) pour La Réunion

Afin de faciliter l'affichage de ces établissements sur une carte, leurs coordonnées sont convertis en latitude/longitude WGS 84.

Concernant le statut juridique des FINESS ET, ceux-ci sont récupérés à partir du statut juridique de leur FINESS EJ, c'est pour cette raison que nous avons besoin de télécharger les données des entités juridiques en plus de celles des établissements (le référentiel des établissements restant le plus important et le seul à être publié en fin de pipeline).

Il existe des mouvements de recomposition des FINESS, modifiant les autorisations et/ou les FINESS géographiques et/ou les FINESS juridiques. Ces mouvements sont décrits sur [cette page](./mutation-finess.md).

Le schéma des données FINESS est décrit sur [cette page](../../schemas/finess/finess.md).

### Données d'autorisation

Les projets relatifs à la création de tout établissement de santé, la création, la conversion et le regroupement des activités de soins, y compris sous la forme d'alternatives à l'hospitalisation, et l'installation des équipements matériels lourds sont soumis à l'autorisation de **l'Agence Régionale de Santé** (ARS), dans le respect du projet régional de santé.
Toute autorisation est accordée par l'ARS à un établissement pour une durée déterminée.

Une autorisation d'activité de soins (parfois abrégée en autorisation AS dans notre pipeline) peut être identifiée par 3 variables : l'activité à laquelle elle se rapporte, sa modalité et sa forme. Ce triplet **activité-modalité-forme** accompagné du finess de l'établissement concerné ainsi que de dates de début et de fin (date de début de mise en oeuvre et de fin de mise en oeuvre de l'autorisation) permettent de modéliser une autorisation d'activité de soins.

#### Activités

Quelques exemples d'activités : médecine, chirurgie, gynécologie-obstétrique, psychiatrie, soins de suite et réadaptation, greffes d'organes, traitement des grands brûlés, neurochirurgie, réanimation, traitement de l'insuffisance rénale, traitement du cancer, ...

#### Modalité

Les modalités sont des modes d’application ou des types de soins prévus par les textes réglementaires encadrant chaque activité de soins. Les modalités ont été harmonisées pour assurer une cohérence entre les diverses modalités relatives aux publics "adulte" ou "enfant". Suite à cette harmonisation, le champ modalité prendra donc généralement une de ces 2 valeurs : "adulte" ou "enfant".

#### Forme

Une forme est un type d’organisation de prise en charge. Ces formes sont donc :

- hospitalisation à temps complet
- hospitalisation à temps partielle de jour ou de nuit
- hospitalisation à domicile
- les alternatives à l’hospitalisation complète spécifiques de la psychiatrie

Il est à noter que certaines activités de soins ont une organisation spécifique et ne relèvent pas des formes précitées : dans ce cas, il a été conservé le code "pas de forme".

Toutes ces informations peuvent être retrouvées dans la [circulaire relative à la nomenclature des activités de soins](https://solidarites-sante.gouv.fr/fichiers/bo/2013/13-06/ste_20130006_0000_0030.pdf).

Le schéma des données d'autorisations est décrit sur [cette page](../../schemas/finess/autorisations_as.md).

### Données d'activités

Ces données sont générées à partir des données d'autorisation, et en particulier des champs d'activités et de modalité permettant de construire un référentiel s'appuyant sur 12 catégories d'activités :

- Assistance médicale à la procréation - Diagnostic prénatal (AMP-DPN)
- Cancérologie
- Chirurgie
- Imagerie Médicale
- Médecine
- Néphrologie
- Obstétrique
- Psychiatrie
- Réanimation
- Soins de Suite et de Réadaptation
- Soins de longue durée
- Diagnostic génétique

Les données d'activités sont historisées : il y a une ligne par année d'activité.
Cependant, seules les données d'activités strictement postérieures à 2014 sont intégrées à la base.

Le schéma des données d'activités est décrit sur [cette page](../../schemas/finess/activites_et_key_value.md), et celui des métadonnées correspondantes sur [cette page](../../schemas/finess/activites_et.md).

## Acquisition des données

Les données FINESS sources sont téléchargées à partir du [jeu de données FINESS](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/) disponible sur data.gouv.fr (les sources sont exhaustivement répertoriées dans le fichier `resources/finess/data_sources.yml`). Elles sont composées d'une extraction récente mise à jour sur un rythme bimestriel ainsi que d'un historique des données à raison d'une extraction par an établie au 31/12 de chaque année depuis 2004.

De manière similaire, les données d'autorisations d'activités de soins sont téléchargées à partir du jeu de données nommé [FINESS Extraction des autorisations d'activités de soins](https://www.data.gouv.fr/fr/datasets/finess-extraction-des-autorisations-dactivites-de-soins/). On y retrouve une extraction récente et un historique produits dans les mêmes conditions que les données FINESS (respectivement bimensuel et une extraction par an établie au 31/12 de chaque année depuis 2004).

Ces données sont toutes téléchargées dans le dossier `data/finess/raw` avant d'être traitées dans la seconde partie du pipeline.

Pour toutes demandes concernant spécifiquement ces données, s'adresser au producteur : l'Agence du Numérique en Santé (ANS).
Un formulaire de prise de contact est disponible sur la page correspondante du portail data.gouv.fr.

## Traitement des données

Les données sont pré-traitées et agrégées pour former 4 tables distinctes : une pour les données FINESS, une autre pour les autorisations d'activités de soins, une table des données d'activités au format clé-valeur et la table de métadonnées associées.

Parmi les opérations de pré-traitement pour les données FINESS, il est à noter :

- le nettoyage du numéro de SIRET erroné 242600666²²²²² qui devient 24260066600083 (le bon numéro de SIRET est tiré d'une recherche effectuée sur l'historique des données de l'établissement comportant le SIRET défectueux)
- l'ajout du champ `dateexport` qui indique l'année à laquelle la ligne de donnée a été produite et exportée (puisque les données sont historisées annuellement comme décrit dans la section [](#acquisition-des-données) )
- l'ajout du champ booléen `ferme_cette_annee` indiquant que l'établissement a fermé lors de l'année correspondante (celle renseignée par le champ `dateexport`)
- la conversion des coordonnées depuis leur système géodésique d'origine vers le système WGS 84

Parmi les opérations de pré-traitement pour les données d'autorisations d'activités de soins, il est à noter :

- la suppression des autorisations d'activités de soins ayant une date de fin supérieure au 1er janvier 2099 (les autorisations d'activités de soins sont délivrées sur une dizaine d'années environ)
- l'ajout des champs:
  - `annee_premiere_activite`: Dernière année de présence de l'autorisation d'activité pour l'établissement
  - `annee_derniere_activite`: Premiere année de présence de l'autorisation d'activité pour l'établissement

> 📝 **Note :**
> Après discussion avec l'ANS, il apparaît que le champ `datefin` peut être dans le passé pour l'export le plus récent du FINESS (trimestriel).
> Ce cas correspond à des autorisations que les ARS n'ont pas encore eu le temps de mettre à jour.
> Dans ce cas, on considère qu'elles sont encore valides, malgré le fait que `datefin` soit passé.

Pour la constitution des données d'activités, un mapping est effectué entre les valeurs des champs `activités` / `modalités` des données d'autorisation et les 11 types d'activités génériques (voir description ci-dessus). La table des métadonnées est constituée à partir du table schema correspondant (consultable [ici](../../schemas/finess/activites_et.md)).

Les 4 tables résultantes sont sauvegardées dans le dossier `data/finess/final` au format csv.

Il est à noter que pour ce domaine de données et contrairement aux autres, aucun fichier CSV de métadonnées n'est généré par le pipeline.

## Validation des données

La validation des données est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur 4 fichiers de métadonnées (un par table) au format JSON et se trouvant dans le dossier `schemas/finess` : `finess.json`, `autorisations_as.json`, `activites_et_key_value.json` et `activites_et.json`.

## Spécificités des données

Les FINESS peuvent être soumis à des mouvements de recomposition au cours de leur histoire.
Ils sont détaillés dans une section dédiée:

```{eval-rst}
.. toctree::
    :maxdepth: 1

    selection-finess
    Documentation des mutations des finess <mutation-finess>
```
