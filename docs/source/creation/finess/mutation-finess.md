Mouvements de recomposition
===========================

Cette page décrit les nombreux types de mouvements de recomposition des FINESS, pouvant s’appliquer aux autorisations et/ou aux FINESS géographiques et/ou aux FINESS juridique.

Les types de mouvements sont décrits par typologie unitaire : il n’est pas exclu qu’un établissement présente plusieurs mouvements de recompositions simultanées (il suffit alors de sommer les effets de chaque mouvement).

## Mouvements sur les seules autorisations

Dans cette section sont décrits les mouvements ne concernant que les autorisations.

### Création sèche d’une autorisation

Il s’agit de la création d’une autorisation dans un EJ A et implantée sur un le site W d’un ET préexistant et rattaché à cet EJ.  
**Exemple :** l’ET W – rattaché l’EJ A et possédant l’autorisation de SSR – obtient une nouvelle autorisation concernant l’activité de médecine, venant s’ajouter aux autorisations existantes.

### Fermeture sèche d’une autorisation

Il s’agit de l’arrêt d’une autorisation appartenant à un ET W rattachée à un EJ A.  
**Exemple :** l’ET W – rattaché l’EJ A et possédant les autorisations de SSR et de médecine – perd son autorisation de médecine, ne possédant donc plus que l’autorisation de SSR.

### Regroupement d’activités de soins de l’EJ A entre site ayant la même autorisation

Il s’agit du regroupement d’une autorisation d’activité du site X sur le site W ayant déjà l’autorisation et appartenant au même EJ (l’ET X poursuit ses autres activités).  
**Exemple :** l’EJ A est composé de deux établissements : l’ET X avec l’autorisation de SSR et de médecine, et l’ET W avec l’autorisation de médecine. L’ET X perd son autorisation de médecine (tout en conservant son autorisation de SSR), et son activité est basculée sur l’ET W qui possédait déjà l’autorisation correspondante.

### Changement d’implantation de l’autorisation d’activité de l’EJ A entre 2 sites

Il s’agit du changement d’implantation d’une autorisation d’activité du site X vers le site W de la même EJ mais n’ayant pas l’autorisation (l’ET X poursuit ses autres activités).  
**Exemple :** l’EJ A est composé de deux établissements : l’ET X avec l’autorisation de SSR et de médecine, et l’ET W avec l’autorisation de médecine. L’ET X perd son autorisation de SSR (tout en conservant son autorisation de médecine), et son activité est basculée sur l’ET W, obtenant ainsi une nouvelle autorisation concernant l’activité de SSR venant compléter l’autorisation de médecine.

### Conversion d’activité dans le site W

Il s’agit de la conversion des activités de l’EJ « A » implantées sur un site W.  
**Exemple :** l’ET W associé à l’EJ A voit son unique autorisation portant sur la médecine transformée en autorisation de SSR (autrement dit, il n’aura plus d’autorisation de médecine).

## Mouvements sur les seuls ET

Dans cette section sont décrits les mouvements ne concernant que les ET.
### Création Sèche d’un ET

Il s’agit d’une création d’un ET dans lequel des autorisations seront implantées pour la première fois.  
**Exemple :** l’ET W est créé avec une autorisation de médecine et est rattaché au l’EJ A qui préexiste.

### Fermeture sèche d’un ET

Il s’agit de la fermeture d’un ET. Les autorisations implantées dans l’ET sont caduques, non renouvelées ou retirées.  
**Exemple :** l’EJ A est associé aux ET X et W ayant respectivement les autorisations d’activité SSR/Médecine et Médecine. L’ET W vient à fermer, avec le retrait de l’autorisation de médecine correspondante. L’ET X perdure, et conserve ses autorisations.

## Mouvements sur les seuls EJ

Dans cette section sont décrits les mouvements ne concernant que les EJ.

### Création Sèche d’un EJ

Il s’agit de la création d’un EJ dans lequel des autorisations seront implantées pour la première fois.  
**Exemple :** l’EJ A est créé, et l’ET W qui lui est associé obtient une première autorisation d’activité en SSR.

### Fermeture sèche d’un EJ (liquidation)

Il s’agit de la fermeture de l’EJ pour liquidation. Les autorisations implantées dans les ET appartenant à ladite EJ sont caduques, non renouvelées ou retirées.  
**Exemple :** l’EJ A ferme et les autorisations de SSR et de médecine associées à l’ET W dépendant de l’EJ A sont retirées.

## Mouvements mixtes (mêlant autorisations, ET et/ou EJ)

### Regroupement d’autorisations de 2 sites dans l’EJ A avec création d’un nouveau site (sites regroupés ayant la même autorisation)

Il s’agit du transfert d’une même autorisation d’activité du site W et du site X sur un nouveau site Y appartenant au même EJ.  
**Exemple :** l’EJ A est composé de deux établissements : l’ET X avec l’autorisation de médecine, et l’ET W avec l’autorisation de médecine. Ces deux ET fusionnent pour créer l’ET Y dépendant toujours de l’EJ A, et associé à une autorisation de médecine.

### Regroupement d’autorisations de 2 sites dans l’EJ A avec création d’un nouveau site (sites regroupés n’ayant pas la même autorisation)

Il s’agit du transfert de deux autorisations d’activités distinctes relevant respectivement du site W et du site X sur un nouveau site Y appartenant au même EJ.  
**Exemple :** l’EJ A est composé de deux établissements : l’ET X avec l’autorisation de SSR, et l’ET W avec l’autorisation de médecine. Ces deux ET fusionnent pour créer l’ET Y dépendant toujours de l’EJ A, et associé à une autorisation de médecine ainsi qu’une autorisation de SSR.

### Regroupement d’autorisations de 2 sites dans l’EJ A sur un 3e site existant

Il s’agit du transfert de toutes les autorisations des sites W et X sur un site Y déjà existant et appartenant au même EJ.  
**Exemple :** l’EJ A est composé de trois établissements : l’ET X avec l’autorisation de SSR et de maternité, l’ET W avec l’autorisation de médecine et de SSR, et l’ET Y avec l’autorisation de médecine. Les deux ET X et W sont absorbés par l’ET Y dépendant toujours de l’EJ A. L’ET Y obtient les nouvelles autorisations de SSR et de maternité des autres établissements. Par ailleurs, l’ET Y récupère l’activité de médecine de l’ET W.

### Transformation d’activités sanitaires en activités médico-sociales.

Il s’agit de la transformation des activités sanitaires en médico-social.  
**Exemple :** l’ET W dépendant de l’EJ A se voit retirer son autorisation de médecine au profit d’une autorisation médico-sociale, restant rattaché à l’EJ A.

### Cession d’autorisations de l’EJ A et l’EJ B

Il s’agit de la cession d’une autorisation de l’EJ A sur site W à l’EJ B sans changement de site.  
**Exemple :** l’ET W – disposant d’une autorisation de médecine et initialement rattaché à l’EJ A – est cédé à l’EJ B en conservant son autorisation de médecine.

### Cession d’autorisations de l’EJ A à l’EJ B suivie d’un changement d’implantation

Il s’agit de la cession d’une autorisation d’activité de l’EJ A sur l’EJ B accompagnée d’un changement d’implantation du site W vers le site X (le site W et l’EJ A perdurent).  
**Exemple :** l’ET W est rattaché à l’EJ A avec les autorisations de médecine et de SSR. L’ET X est rattaché à l’EJ B avec une autorisation de médecine. L’activité de SSR de l’ET W est alors intégrée à l’ET X dépendant de l’EJ B. L’ET X obtient ainsi l’autorisation d’activité de SSR, quand l’ET W perd l’autorisation correspondante.

### Fusion de deux EJ avec création d’une nouvelle EJ

Il s’agit d’une fusion de l’EJ A et de l’EJ B avec la création d’un EJ C et du rattachement des sites des EJ A et EJ B à l’EJ C avec disparition des EJ A et B.  
**Exemple :** l’ET W est rattaché à l’EJ A avec les autorisations de médecine et de SSR. L’ET X est rattaché à l’EJ B avec une autorisation de médecine. Les EJ A et B fusionnent en un EJ C, regroupant ainsi les ET W et X qui conservent leurs autorisations d’origine.

### Fusion absorption d’EJ

Il s’agit d’une fusion et d’une absorption de l’EJ B par l’EJ A avec rattachement des sites de l’EJ B à l’EJ A.  
**Exemple :** l’ET W est rattaché à l’EJ A avec les autorisations de médecine et de SSR. L’ET X est rattaché à l’EJ B avec une autorisation de médecine. L’EJ A absorbe l’EJ B, récupérant ainsi l’ET X et son autorisation de médecine (et restant distinct de l’ET W).

### Eclatement d’EJ

Il s’agit de l’éclatement de l’EJ A en deux EJ, et du rattachement respectif des sites W et X aux nouveaux EJ B et C.  
**Exemple :** l’EJ A possède l’ET W ayant les autorisations de médecine et de SSR ainsi que l’ET X ayant l’autorisation de médecine. L’EJ A éclate en deux EJ : l’EJ B associé à l’ET W et ses autorisations d’origine ; et l’EJ C associé à l’ET X et son autorisation d’origine.
