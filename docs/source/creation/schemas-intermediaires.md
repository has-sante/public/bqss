# Schémas des données intermédiaires

Les schémas des tables intermédiaires utilisées lors du processus de création de la base sont accessibles dans cette section.

```{eval-rst}
.. toctree::
    :maxdepth: 1

    certification_14_20/certifications.csv <../schemas/certification_14_20/certifications.md>
    certification_14_20/thematiques.csv <../schemas/certification_14_20/thematiques.md>
    certification_21_25/certifications.csv <../schemas/certification_21_25/certifications.md>
    certification_21_25/chapitres.csv <../schemas/certification_21_25/chapitres.md>
    certification_21_25/objectifs.csv <../schemas/certification_21_25/objectifs.md>
    esatis/esatis_key_value.csv <../schemas/esatis/esatis_key_value.md>
    esatis/esatis.csv <../schemas/esatis/esatis.md>
    iqss/iqss_key_value.csv <../schemas/iqss/iqss_key_value.md>
    sae/sae.csv <../schemas/sae/sae.md>
    finess/activites_et_key_value.csv <../schemas/finess/activites_et_key_value.md>
```
