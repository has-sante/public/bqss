# Mise en Production

La mise en production de la base est réalisée :

- De façon routinière après chaque mise à jour des données open data de certification,
- Annuellement après l'ajout de nouveaux IQSS.

Le processus _routinier_ de mise en production de la base est le suivant :

1. Déclenchement sur gitlab : Construction de la base et dépôt des données dans l'environnement de **preprod**
2. Automatiquement : Mise à jour du site Qualiscope de **preprod** pour recette (une fois par jour, la nuit)
3. Recette du site Qualiscope de **preprod** pour validation de la mise en production
4. Déclenchement sur gitlab : Dépôt des données dans l'environnement de **production**
5. Automatiquement : Mise à jour du site Qualiscope de **production** (une fois par jour, la nuit)
6. Recette du site QualiScope de production

L'ajout annuel des nouveaux IQSS nécessite des développements sur la BQSS, ainsi qu'un processus de développement et de recette spécifique sur QualiScope.

## Hébergement des données

Les fichiers de sorties du projet sont hébergés sur le stockage objet.

Il existe deux zones de dépôt sur le stockage objet :

- `bqss/preprod` : héberge les données pour le site de preprod
- `bqss/prod` : héberge les données de production

Les fichiers exposés sur data.gouv.fr sont en fait des liens vers les données de production hébergées sur le stockage objet.

## Dépôt des données

Il existe deux moyens de mettre à jour les données :

- `Depuis gitlab` : en utilisant une action manuelle du pipeline gitlab
- `Ligne de commande` : via la cli du projet

### Lancement depuis Gitlab

Il possible d'effectuer le téléversement des données directement depuis Gitlab.
C'est maintenant la méthode privilégiée pour déployer une nouvelle version des données.

Pour cela :

- Sur la page des pipeline du projet [gitlab](https://gitlab.has-sante.fr/has-sante/public/bqss/-/pipelines), lancer l'action manuelle `preprod-data-push` qui produit la base et dépose les données en preprod. La nuit suivante, les données sont intégrées par la preprod de QualiScope.
- Le lendemain, faire la recette sur la preprod de QualiScope.
- Retourner sur la page des pipelines et lancez l'action manuelle `copy-preprod-to-prod`. La nuit suivante, les données sont intégrées par la prod de QualiScope.

> 📝 **Note**
> Cette dernière action copie les données de la preprod vers la prod, elle ne relance pas tout le pipeline.

### En ligne de commande

Pour déposer les données depuis la ligne de commande, il faut :

- Avoir généré les données au préalable :

```shell
cli bqss --force --validate
```

> 📝 **Note**
> Ceci relance l'intégralité du pipeline ainsi que les tests automatiques associés

- Avoir configuré la connexion au vault :
  - Avoir défini l'envvar `VAULT_HOST` qui pointe vers le vault :
  ```shell
  export VAULT_HOST=https://vault.remplace.moi
  ```
  - Avoir défini dans l'envvar `VAULT_TOKEN` un token vault valide avec le role `bqss`:
  ```shell
  export VAULT_TOKEN=`vault login -address=${VAULT_HOST} -method=oidc -token-only role="bqss"`
  ```

> 📝 **Note**
> Il est aussi possible d'obtenir le token directement depuis l'interface web de vault

- Téléverser les données sur le stockage objet dans la zone de **preprod**:

```shell
cli release-data --env=preprod
```

- Une fois la recette de la **preprod** faite, téléverser les données dans la zone de **prod**:

```shell
cli release-data --env=prod
```

## Validation des données

### Tests automatiques

Une suite de tests automatiques permet de valider que les données respectent un certains nombre de contraintes.
Cette suite est maintenue à jour au fil des développement.

### Recette en préproduction

La suite de tests automatiques n'est pas exhaustive, c'est pourquoi une validation sur le site Qualiscope de preprod est nécessaire.

### Comparaison avant mise en production

Une commande de la cli est disponible pour effectuer une comparaison des données avant mise en production.

En local, elle peut s'utiliser de la façon suivante:

```shell
cli compare-data --source-env local --ref-env prod --export-comp-file
```

Vous pouvez ensuite aller consulter le fichier dans `data/final/comparaison_local_prod.xlsx`

Cette comparaison est également inclue dans les log du job de CI qui effectue la création et le dépôt des données en preprod.
