# Execution du projet

Le point d'entrée principal pour lancer le pipeline de constitution de la base est une CLI dont le code est situé dans `bin/cli`.

Pour lancer la CLI, il faut avoir d'abord avoir configuré [l'environnement](development.md).
Une fois cela fait, on peut accéder à la CLI avec:

```shell
poetry run cli
```

Ou en activant le virtualenv python:

```shell
poetry shell
cli
```

Cela devrait afficher l'ensemble des commandes disponibles.

## Pipeline complet

Pour faire tourner l'ensemble du pipeline, utilisez:

```shell
cli bqss --force --validate
```

## Upload des données

Pour faire l'upload sur data.gouv.fr, il faut avoir un token Vault valide avec le role `bqss`.
Le token doit être contenu dans la variable d'environnement `VAULT_TOKEN`.

Il peut être obtenu en ligne de commande avec:

```shell
export VAULT_TOKEN=`vault login -address=${VAULT_ADDR} -method=oidc -token-only role="bqss"`
```

### en preprod

```shell
cli release-data --env=preprod
```

### en prod

```shell
cli release-data --env=prod
```

## Étapes individuelles du pipeline

Il est aussi possible d'executer individuellement chaque étape du pipeline en utilisant les commandes associées.
Par exemple:

```shell
cli bqss finess --validate
```

Permet de ne lancer que la partie relative au FINESS.

## Détails des commandes disponibles


**Usage**:

```{eval-rst}
.. click:: bin.cli:cli
    :prog: cli
    :nested: full
```
