# Historique

Toutes les évolutions notables du projet BQSS sont listées ici.

Le code source du projet est accessible sur le dépôt gitlab: [https://gitlab.has-sante.fr-sante.fr/has-sante/public/bqss](https://gitlab.has-sante.fr/has-sante/public/bqss)

NB: Le format est inspiré de [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)
et ce projet adhère à la [Gestion Sémantique de Version](https://semver.org/lang/fr/spec/v2.0.0.html).

## [2.6.1] - 28/01/2025

### Ajouts

- Commande pour extraire des statistiques sur la différence entre deux versions de la base [#227](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/227)

### Changements

- Mise à jour du finess annuel 2024 [#226](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/226)

### Corrections

- Gestions des changements de format de la SAE pour le fichier `ID_2021r.csv` [#225](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/225)

## [2.6.0] - 16/12/2024

### Ajouts

- IQSS 2024 [#221](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/221)
- SAE 2023 [#223](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/223)

## [2.5.1] - 14/11/2024

### Corrections

- Ajout des nouveaux identifiants de référentiels géographiques suite à la mise à jour du finess [#222](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/222)

## [2.5.0] - 15/10/2024

### Changements

- Élargissement du périmètre du fichier FINESS pour inclure **tous** les établissements [#220](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/220)

## [2.4.2] - 23/09/2024

### Améliorations

- Rattrapage des coordonnées géographiques manquantes sur le finess [#218](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/218)

## [2.4.1] - 04/03/2024

### Améliorations

- Gestion des erreurs dans le finess [#216](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/216)
- Ajout d'un test de cohérence pour les données certification ref 2021 [#215](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/215)

## [2.4.0] - 12/02/2024

### Ajouts

- IQSS 2023 [#212](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/212)
- SAE 2022 [#210](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/210)
- Site principal des démarches de certification v2021 [#214](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/214)

## [2.3.3] - 12/12/2023

### Améliorations

- Utilisation de pandera pour le finess final [#211](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/211)

### Corrections

- Suppression des colonnes d'activité du fichier finess [#209](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/209)

## [2.3.2] - 03/10/2023

### Améliorations

- Report des valeurs manquantes e-Satis [#203](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/203)

## [2.3.1] - 27/09/2023

### Corrections

- Correction d'un bug dans la redescente des IQSS PSY [#208](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/208)
- Correction d'un test du finess [#204](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/204)
- Correction de l'upload en prod [#202](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/202)
- Utilisation de la méthode d'authentification JWT/Vault pour le push de la prod [#201](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/201)

# [2.3.0] - 11/04/2023

### Améliorations

- Meilleure gestion des date de fin des autorisations [#194](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/194)

## [2.2.1] - 07/04/2023

### Ajouts

- Ajout du fichier sur les critères d'inclusion Qualiscope [#200](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/200)

## [2.2.0] - 30/03/2023

Cette release ne fait pas l'objet d'un majeur mais elle contient des changements "Breaking".
Ces changements concernent uniquement la CLI, pas la données.

### Changements

- **BREAKING** changement des paramètres de la CLI pour automatisation [#89](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/89)

### Améliorations

- Automatisation de la mise en production des données [#89](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/89)

## [2.1.1] - 23/03/2023

### Corrections

- Correction du fichier de metadata des IQSS [#198](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/198)
- Correction d'un doublon FINESS géo dans la redescente de la psy [#199](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/199)

## [2.1.0] - 13/02/2023

### Corrections

- Correction du filtre `actif_qualiscope` [#192](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/192)

## [2.0.0] - 08/02/2023

### Corrections

- Correction des schemas de la doc qui n'étaient plus générés [#178](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/178)
- Pour les données e-Satis (questionnaire satisfaction patient), on converti les variables `nb_...` en `integer`, précédemment elles étaient en `float`.
- **BREAKING** Correction des versions de certains IQSS dans le fichier de metadata [#181](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/181)
- Correction de la colonne `finess_type` dans la sae [#188](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/188)
- Changement de l'encoding des fichiers FINESS "latest" brutes [#191](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/191)

### Changements

- **BREAKING** Harmonisation des valeurs d'évolution pour les IQSS QualHas [#177](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/177)
- **BREAKING** Harmonisation des valeurs d'évolution pour les IQSS e-Satis [#190](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/190)
- Amélioration de la documentation autour des IQSS [#167](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/167)
- **BREAKING** Changement du groupement des composantes des IQSS e-Satis [#189](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/189)

## [1.2.6] - 11/01/2023

### Corrections

- Correction du calcul des autorisations qui n'utilisait pas la bonne date [#187](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/187)
- Correction des schemas de la doc qui n'étaient plus générés [#178](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/178)

## [1.2.5] - 09/11/2022

### Corrections

- Correction du parsing des dates de la certification ref 2021 [#176](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/176)

## [1.2.4] - 08/11/2022

### Ajouts

- Ajout d'un tutoriel en SAS [#162](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/162)

### Changements

- Migration de la CI sur le cloud
- Allongement du timeout pour le tutoriel python

### Corrections

- Fix du saut de ligne dans `metadata.csv` [#174](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/174)

## [1.2.3] - 12/10/2022

### Changements

- Migration du repo vers https://gitlab.has-sante.fr

## [1.2.2] - 26/09/2022

### Corrections

- Ajout de l'activité "Diagnostic génétique" à la place d'AMP DPN [#170](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/166)

## [1.2.1] - 29/08/2022

### Changements

- Changement du libelle de l'activité AMP DPN [#169](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/169)

## [1.2.0] - 24/08/2022

### Ajouts

- Ajout de la SAE 2020 [#163](https://gitlab.has-sante.fr/has-sante/public/alpha/scogitlab.has-sante.frata/-/issues/163)
- Catégorie d'activité à part entière pour les Soins de longue durée [#166](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/166)

### Corrections

- Patch d'un fichier corrompu de la SAE [#164](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/164)
- Suppression de l'activité "Pédiatrie" [#165](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/165)

## [1.1.0] - 27/06/2022

### Ajouts

- Ajout des dates de début et fin effective des autorisations d'activités [#160](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/160)

### Changements

- Ajout des établissements des établissements ayant l'activité de dialyse dans le filtre `actif_qualiscope` [#105](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/105#note_1007153424)

### Corrections

- Meilleure gestion des autorisations d'activité [#160](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/160)

## [1.0.0] - 22/06/2022

Le principal changement de cette release est le renommage complet du projet.
On passe de `scope-sante-data` à `BQSS` ce qui engendre des breaking changes.gitlab.has-sante.fr
Le repository du projet a changé de https://gitlab.has-sante.fr/has-sante/public/bqss à https://gitlab.has-sante.fr/has-sante/public/bqss.

### Ajouts

- Ajouts des indicateurs "binaires" aux statistiques nationales [#151](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/151)
- Ajouts des données de la certification référentiel 2021 [#125](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/125)
- Ajouts des codes démarche de certification [#152](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/152)

### Changements

- `[BREAKING]` renommage en BQSS [#136](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/136):
  - Renommage de la colonne `actif_scope_sante` dans la table `valeurs.csv` en `actif_qualiscope`
  - Urls de la doc change: https://has-sante.pages.has-sante.fr/public/bqss devient https://has-sante.pages.has-sante.fr/public/bqss
- Changement du filtre `actif_qualiscope [#105](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/105#note_998363597)
- Mise à jour des dépendances du projet
- Utilisation de `pydantic-autodoc` pour documenter la base document
- Amélioration du `type_etablissement` [#155](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/155)

### Corrections

- Correction d'un bug dans le traitement des Finess, qui n'étaient pas filtré sur les catégories d'aggrégats sur l'extract le plus récent.
- Correction des types et des types métiers de certains IQSS [#150](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/150):
  - `MHS_IAS_ICSHA_2018-resultat_icsha`, `PSY_IAS_ICSHA_2018-resultat_icsha` et `mco_eteortho_eteortho_2017-ete_ortho_etbt` changent de type métier: `ratio` à la place de `resultat`
  - Les types de certains IQSS ont été revus et passent de `classe` à `int-sgitlab.has-sante.fr
- Correction de l'url de téléchargement FINESS géographiques [#153](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/153)

## [0.1.0]

Cette section liste les changements de la version `0.1.0`.

### Ajouts

- Ajout d'un changelog [#147](https:/gitlab.has-sante.fr/has-sante/public/bqss/-/issues/147)
- Ajout des IQSS 2021 [#145](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/145)gitlab.has-sante.fr
- Utilisation de [pandera](https://pandera.readthedocs.io) pour la validation de certains Dataframes. [#146](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/146)

### Corrections

- Correction d'un bug donnant des IQSS satisfaction patient au mauvais finess [#148](https://gitlab.has-sante.fr/has-sgitlab.has-sante.frc/bqss/-/issues/148)
- Correction d'un bug dans les metadata finess provoquant la perte des activités dans la base document [#149](https://gitlab.has-sante.fr/has-sante/public/bqss/-/issues/149)
