# Tutoriels d'utilisation des données

```{toctree}
:maxdepth: 1
:glob:

Python - Exploitation des données <006-py-tutorial-bqss>
SAS - Exploitation des données <007-sas-tutorial-bqss>
```
