# Tutoriel SAS - Usage de la Base sur la Qualité et la Sécurité des Soins (BQSS)

Ce tutoriel vise à montrer un usage type dont peut être l'objet BQSS, de façon programmatique grâce au langage SAS.

## Import des librairies et des données

Les URLS des données sont récupérable sur la page de la Base sur la Qualité et la Sécurité des Soins sur la plateforme Data Gouv accessible via [ce lien](https://www.data.gouv.fr/fr/datasets/base-sur-la-qualite-et-la-securite-des-soins-anciennement-scope-sante/). La section **Fichiers** contient les données disponibles, et pour chaque fichier, une description est présente incluant notamment un **URL stable** qu'il est recommandé d'utiliser.

La base BQSS adoptant un format clé-valeurs, il est nécessaire de récupérer la table des valeurs d'intérêts (concernant la qualité et la sécurité des soins) ainsi que les métadonnées associées spécifiant ces valeurs et les données caractérisant les établissements de santé via les numéro FINESS.

```sas
/*Chargement du fichier valeurs.csv dans une table SAS*/
filename csvFile url "https://www.data.gouv.fr/fr/datasets/r/da295ccc-2011-4995-b810-ad1f1a82a83f" encoding="utf-8";

proc import datafile=csvFile
	out=valeurs_df
	dbms=csv
	replace;
	delimiter=',';
	getnames=yes;
	guessingrows=max ;
run;

/*Chargement du fichier metadata.csv dans une table SAS*/
filename csvFile url "https://www.data.gouv.fr/fr/datasets/r/e56fb5a5-5a74-4507-ba77-e0411d4aa234" encoding="utf-8" ;

proc import datafile=csvFile
	out=metadata_df
	dbms=csv
	replace;
	delimiter=',';
	getnames=yes;
	guessingrows=max ;
run;

/*Chargement du fichier finess.csv dans une table SAS*/
filename csvFile url "https://www.data.gouv.fr/fr/datasets/r/48dadbce-56a8-4dc4-ac51-befcdfd82521" encoding="utf-8";

proc import datafile=csvFile
	out=finess_df
	dbms=csv
	replace;
	delimiter=',';
	getnames=yes;
	guessingrows=max ;
run;
```

## Observation des données

Il est désormais possible de consulter les informations caractérisant ces données et en observer un échantillon.

### Caractérisation des données valeurs_df

```sas
proc contents data=valeurs_df;
run;

proc print data=valeurs_df (obs=5);
	where ranuni(100)<=0.0001;
run;
```

```markdown
| #   | Variable      | Type  | Long. | Format  | Informat |
| --- | ------------- | ----- | ----- | ------- | -------- |
| 1   | annee         | Num.  | 8     | BEST12. | BEST32.  |
| 2   | finess        | Texte | 9     | $9.     | $9.      |
| 3   | finess_type   | Texte | 3     | $3.     | $3.      |
| 4   | key           | Texte | 51    | $51.    | $51.     |
| 9   | missing_value | Texte | 21    | $21.    | $21.     |
| 5   | value_boolean | Texte | 5     | $5.     | $5.      |
| 10  | value_date    | Texte | 19    | $19.    | $19.     |
| 8   | value_float   | Num.  | 8     | BEST12. | BEST32.  |
| 7   | value_integer | Num.  | 8     | BEST12. | BEST32.  |
| 6   | value_string  | Texte | 97    | $97.    | $97.     |
```

```markdown
| Obs.  | annee | finess    | finess_type | key            | value_boolean | value_string | value_integer | value_float | missing_value | value_date |
| ----- | ----- | --------- | ----------- | -------------- | ------------- | ------------ | ------------- | ----------- | ------------- | ---------- |
| 2184  | 2018  | 590024618 | geo         | filtre_heb_med | False         |              | .             | .           |               |            |
| 2198  | 2018  | 590040317 | geo         | filtre_heb_med | False         |              | .             | .           |               |            |
| 4117  | 2019  | 030000160 | geo         | filtre_heb_med | False         |              | .             | .           |               |            |
| 11744 | 2020  | 920711512 | geo         | filtre_heb_med | False         |              | .             | .           |               |            |
| 14308 | 2017  | 590790473 | geo         | filtre_heb_med | False         |              | .             | .           |               |            |
```

Un grand nombre de variables de nature différente sont regroupées dans cette table (d'où la table metadata venant spécifier la nature de chaque variables). Le nom des variables est stocké dans la colonne `key`.

Afin de pouvoir distinguer les différents types de valeurs (booléen, entier, décimal, chaîne de caractères, valeurs manquantes, dates), des colonnes dédiées leur sont attachées.

Ainsi, pour une variable donnée, un seul type est possible... :

- sur l'ensemble de la classe (hormis la colonne `missing_value`)
- pour chaque valeur (justifiant la nature lacunaire de ces colonnes, toutes les colonnes autre que celle prenant la valeur étant des valeurs manquantes)

### Caractérisation des données metadata_df

```sas
proc contents data=metadata_df;
run;

proc print data=metadata_df (obs=5);
	where ranuni(100)<=0.01;
run;
```

```markdown
| #   | Variable          | Type  | Long. | Format  | Informat |
| --- | ----------------- | ----- | ----- | ------- | -------- |
| 9   | acronyme          | Texte | 8     | $8.     | $8.      |
| 13  | ancienne_variable | Texte | 26    | $26.    | $26.     |
| 12  | annee             | Num.  | 8     | BEST12. | BEST32.  |
| 3   | description       | Texte | 247   | $247.   | $247.    |
| 16  | finess_type       | Texte | 5     | $5.     | $5.      |
| 6   | indicateur        | Texte | 26    | $26.    | $26.     |
| 1   | name              | Texte | 329   | $329.   | $329.    |
| 7   | secteur           | Texte | 3     | $3.     | $3.      |
| 5   | source            | Texte | 32    | $32.    | $32.     |
| 8   | theme             | Texte | 8     | $8.     | $8.      |
| 2   | title             | Texte | 167   | $167.   | $167.    |
| 4   | type              | Texte | 12    | $12.    | $12.     |
| 15  | type_metier       | Texte | 16    | $16.    | $16.     |
| 14  | url_rapport       | Texte | 115   | $115.   | $115.    |
| 11  | variable          | Texte | 26    | $26.    | $26.     |
| 10  | version           | Num.  | 8     | BEST12. | BEST32.  |
```

```markdown
| Obs. | name                                          | title                                                                      | description                                                              | type         | source                           | indicateur            | secteur | theme | acronyme | version | variable                 | annee | ancienne_variable        | url_rapport                                                                                           | type_metier     | finess_type |
| ---- | --------------------------------------------- | -------------------------------------------------------------------------- | ------------------------------------------------------------------------ | ------------ | -------------------------------- | --------------------- | ------- | ----- | -------- | ------- | ------------------------ | ----- | ------------------------ | ----------------------------------------------------------------------------------------------------- | --------------- | ----------- |
| 2    | certif_date                                   | Date de certification                                                      | Date de délibération du collège de la HAS qui a donné lieu à la décision | date         | Certification v2014              |                       |         |       |          | .       |                          | .     |                          |                                                                                                       |                 | mixte       |
| 139  | mco_avc_dhs_2014-dhs_c_ichaut_etbt            | Intervalle de confiance haut date et heure de survenue des symptômes d'AVC |                                                                          | float        | IQSS dossier patient             | MCO_AVC_DHS_2014      | MCO     | AVC   | DHS      | 2014    | dhs_c_ichaut_etbt        | 2014  | dhs_c_ichaut_etbt        | https://www.has-sante.fr/upload/docs/application/pdf/2015-11/rapport_long_avc_2015_vd.pdf             | intervalle_haut | mixte       |
| 190  | psy_dpa_dtn1_2015-dtn1_c1_icbas_etbt          | Intervalle de confiance bas dépistage de troubles nutrionnels (2 niveaux)  |                                                                          | float        | IQSS dossier patient             | PSY_DPA_DTN1_2015     | PSY     | DPA   | DTN1     | 2015    | dtn1_c1_icbas_etbt       | 2015  | dtn1_c1_icbas_etbt       | https://www.has-sante.fr/upload/docs/application/pdf/2016-12/rapport_2016_dpa_psy_vf.pdf              | intervalle_bas  | mixte       |
| 262  | psy_ias_icsha_2018-obligatoire_icsha_v3_psy   | Recueil obligatoire pour ICSHA (Secteur : Psychiatrie)                     |                                                                          | float-string | IQSS questionnaire établissement | PSY_IAS_ICSHA_2018    | PSY     | IAS   | ICSHA    | 2018    | obligatoire_icsha_v3_psy | 2018  | obligatoire_icsha_v3_psy | https://pprod-web.has-sante.fr/upload/docs/application/pdf/2019-04/fiche_descriptive_icsha.3_2019.pdf | obligatoire     | mixte       |
| 301  | mco_hpp_pecihppi_2014-peci_hppi_c_ichaut_etbt | Intervalle de confiance haut prise en charge initiale de l'HPPI            |                                                                          | classe       | IQSS dossier patient             | MCO_HPP_PECIHPPI_2014 | MCO     | HPP   | PECIHPPI | 2014    | peci_hppi_c_ichaut_etbt  | 2014  | peci_hppi_c_ichaut_etbt  | https://www.has-sante.fr/upload/docs/application/pdf/2015-11/synthese_hpp_mco_2015_vd.pdf             | intervalle_haut | mixte       |
```

La colonne `name` de cette table permet de faire la jointure avec la table `valeurs_df` via la colonne `key` associée.

Les métadonnées permettent de compléter les informations associées aux variables, en particulier leur `type` (encodé implicitement dans `valeur.csv`, voir ci-dessus), leur `source` (provenant d'origines variées), leur `secteur`, leur `theme ` et leur `acronyme`.

Pour une recherche rapide, vous pouvez aussi utiliser directement le fichier `metadata.xlsx` qui permet de faire des filtres rapides sur les différentes colonnes.

### Caractérisation des données finess_df

```sas
proc contents data=finess_df;
run;

proc print data=finess_df (obs=5);
	where ranuni(100)<=0.0001;
run;
```

```markdown
| #   | Variable                     | Type  | Long. | Format    | Informat  |
| --- | ---------------------------- | ----- | ----- | --------- | --------- |
| 55  | actif_qualiscope             | Texte | 5     | $5.       | $5.       |
| 47  | adresse_postale_ligne_1      | Texte | 41    | $41.      | $41.      |
| 48  | adresse_postale_ligne_2      | Texte | 32    | $32.      | $32.      |
| 21  | categorie_agregat_et         | Num.  | 8     | BEST12.   | BEST32.   |
| 19  | categorie_et                 | Num.  | 8     | BEST12.   | BEST32.   |
| 24  | code_ape                     | Texte | 5     | $5.       | $5.       |
| 25  | code_mft                     | Num.  | 8     | BEST12.   | BEST32.   |
| 39  | code_officiel_geo            | Texte | 5     | $5.       | $5.       |
| 40  | code_postal                  | Num.  | 8     | BEST12.   | BEST32.   |
| 27  | code_sph                     | Num.  | 8     | BEST12.   | BEST32.   |
| 13  | commune                      | Num.  | 8     | BEST12.   | BEST32.   |
| 7   | complement_distribution      | Texte | 36    | $36.      | $36.      |
| 6   | complement_raison_sociale    | Texte | 36    | $36.      | $36.      |
| 11  | complement_voie              | Texte | 1     | $1.       | $1.       |
| 33  | coord_x_et                   | Num.  | 8     | BEST12.   | BEST32.   |
| 34  | coord_y_et                   | Num.  | 8     | BEST12.   | BEST32.   |
| 30  | date_autorisation            | Texte | 10    | $10.      | $10.      |
| 1   | date_export                  | Num.  | 8     | YYMMDD10. | YYMMDD10. |
| 36  | date_geocodage               | Num.  | 8     | YYMMDD10. | YYMMDD10. |
| 31  | date_maj                     | Num.  | 8     | YYMMDD10. | YYMMDD10. |
| 29  | date_ouverture               | Texte | 10    | $10.      | $10.      |
| 14  | departement                  | Texte | 2     | $2.       | $2.       |
| 56  | dernier_enregistrement       | Texte | 5     | $5.       | $5.       |
| 43  | ferme_cette_annee            | Texte | 5     | $5.       | $5.       |
| 44  | latitude                     | Num.  | 8     | BEST12.   | BEST32.   |
| 22  | libelle_categorie_agregat_et | Texte | 59    | $59.      | $59.      |
| 20  | libelle_categorie_et         | Texte | 60    | $60.      | $60.      |
| 42  | libelle_code_ape             | Texte | 131   | $131.     | $131.     |
| 46  | libelle_commune              | Texte | 26    | $26.      | $26.      |
| 15  | libelle_departement          | Texte | 24    | $24.      | $24.      |
| 26  | libelle_mft                  | Texte | 60    | $60.      | $60.      |
| 38  | libelle_region               | Texte | 27    | $27.      | $27.      |
| 41  | libelle_routage              | Texte | 26    | $26.      | $26.      |
| 28  | libelle_sph                  | Texte | 62    | $62.      | $62.      |
| 52  | libelle_statut_juridique_ej  | Texte | 60    | $60.      | $60.      |
| 10  | libelle_voie                 | Texte | 28    | $28.      | $28.      |
| 12  | lieu_dit_bp                  | Texte | 34    | $34.      | $34.      |
| 16  | ligne_acheminement           | Texte | 32    | $32.      | $32.      |
| 45  | longitude                    | Num.  | 8     | BEST12.   | BEST32.   |
| 3   | num_finess_ej                | Texte | 9     | $9.       | $9.       |
| 2   | num_finess_et                | Texte | 9     | $9.       | $9.       |
| 32  | num_uai                      | Texte | 8     | $8.       | $8.       |
| 8   | num_voie                     | Texte | 6     | $6.       | $6.       |
| 49  | raison_sociale_ej            | Texte | 42    | $42.      | $42.      |
| 4   | raison_sociale_et            | Texte | 42    | $42.      | $42.      |
| 50  | raison_sociale_longue_ej     | Texte | 64    | $64.      | $64.      |
| 5   | raison_sociale_longue_et     | Texte | 68    | $68.      | $68.      |
| 37  | region                       | Num.  | 8     | BEST12.   | BEST32.   |
| 23  | siret                        | Num.  | 8     | BEST12.   | BEST32.   |
| 35  | source_coord_et              | Texte | 48    | $48.      | $48.      |
| 53  | statut_juridique             | Texte | 24    | $24.      | $24.      |
| 51  | statut_juridique_ej          | Num.  | 8     | BEST12.   | BEST32.   |
| 18  | telecopie                    | Num.  | 8     | NLNUM12.  | NLNUM32.  |
| 17  | telephone                    | Texte | 14    | $14.      | $14.      |
| 54  | type_etablissement           | Texte | 24    | $24.      | $24.      |
| 9   | type_voie                    | Texte | 28    | $28.      | $28.      |
```

```markdown
| Obs.  | date_export | num_finess_et | num_finess_ej | raison_sociale_et                | raison_sociale_longue_et               | complement_raison_sociale | complement_distribution | num_voie | type_voie | libelle_voie               | complement_voie | lieu_dit_bp | commune | departement | libelle_departement | ligne_acheminement | telephone      | telecopie | categorie_et | libelle_categorie_et                           | categorie_agregat_et | libelle_categorie_agregat_et                                | siret        | code_ape | code_mft | libelle_mft                                               | code_sph | libelle_sph                   | date_ouverture | date_autorisation | date_maj   | num_uai | coord_x_et | coord_y_et | source_coord_et                                | date_geocodage | region | libelle_region       | code_officiel_geo | code_postal | libelle_routage | libelle_code_ape        | ferme_cette_annee | latitude     | longitude    | libelle_commune | adresse_postale_ligne_1           | adresse_postale_ligne_2 | raison_sociale_ej                | raison_sociale_longue_ej                        | statut_juridique_ej | libelle_statut_juridique_ej                     | statut_juridique         | type_etablissement       | actif_qualiscope | dernier_enregistrement |
| ----- | ----------- | ------------- | ------------- | -------------------------------- | -------------------------------------- | ------------------------- | ----------------------- | -------- | --------- | -------------------------- | --------------- | ----------- | ------- | ----------- | ------------------- | ------------------ | -------------- | --------- | ------------ | ---------------------------------------------- | -------------------- | ----------------------------------------------------------- | ------------ | -------- | -------- | --------------------------------------------------------- | -------- | ----------------------------- | -------------- | ----------------- | ---------- | ------- | ---------- | ---------- | ---------------------------------------------- | -------------- | ------ | -------------------- | ----------------- | ----------- | --------------- | ----------------------- | ----------------- | ------------ | ------------ | --------------- | --------------------------------- | ----------------------- | -------------------------------- | ----------------------------------------------- | ------------------- | ----------------------------------------------- | ------------------------ | ------------------------ | ---------------- | ---------------------- |
| 2184  | 2022-09-07  | 300781440     | 300000700     | CTRE SSR DOMAINE DU CROS QUISSAC | CSSR CARDIO PULMONAIRE DOMAINE DU CROS |                           |                         |          | Domaine   | DU CROS                    |                 |             | 210     | 30          | GARD                | 30260 QUISSAC      | 0466808000     | 466808081 | 109          | Etablissement de santé privé autorisé en SSR   | 1107                 | Etablissements de santé privé autorisés en SSR              | 3.0192105E13 | 8610Z    | 7        | ARS établissements de santé non financés dotation globale | 0        | Non concerné                  | 1974-11-15     | 1974-11-15        | 2022-07-26 |         | 782254.5   | 6312098.8  | 2,ATLASANTE,84,IGN,BD_ADRESSE,V2.2,LAMBERT_93  | 2022-09-05     | .      |                      |                   | 30260       |                 |                         | False             | 43.903095299 | 4.0238260792 | QUISSAC         |                                   | 30260 QUISSAC           | SAS SOCIETE EXPLOITATION DU CROS | SAS SOCIETE D'EXPLOITATION DOMAINE DU CROS      | 95                  | Société par Actions Simplifiée (S.A.S.)         | Privé                    | Privé                    | True             | True                   |
| 2198  | 2022-09-07  | 300786779     | 300780046     | CMPEA LES MAGES                  | CMPEA LES MAGES                        |                           |                         | 203      | Rue       | DU LOT DES TRUQUAILLES     |                 |             | 152     | 30          | GARD                | 30960 LES MAGES    | 0466241867     | .         | 156          | Centre Médico-Psychologique (C.M.P.)           | 1111                 | Autres Etablissements de Lutte contre les Maladies Mentales | .            |          | 3        | ARS établissements Publics de santé dotation globale      | 1        | Etablissement public de santé | 1904-04-04     | 1904-04-04        | 2022-01-20 |         | 793163.8   | 6348084.9  | 1,ATLASANTE,94,IGN,BD_ADRESSE,V2.2,LAMBERT_93  | 2022-09-05     | .      |                      |                   | 30960       |                 |                         | False             | 44.225585658 | 4.1662111722 | LES MAGES       | 203 Rue Du Lot Des Truquailles    | 30960 LES MAGES         | CH ALES CEVENNES                 | CENTRE HOSPITALIER ALES CEVENNES                | 13                  | Etablissement Public Communal d'Hospitalisation | Public                   | Public                   | False            | True                   |
| 4117  | 2022-09-07  | 530030121     | 530000371     | HOPITAL DE JOUR LAVAL EST        | HOPITAL DE JOUR EN SANTE MENTALE       | PSY ADULTE HTP            |                         | 45       | Rue       | CHEF BATAILLON HENRI GERET |                 |             | 130     | 53          | MAYENNE             | 53000 LAVAL        |                | .         | 355          | Centre Hospitalier (C.H.)                      | 1102                 | Centres Hospitaliers                                        | 2.6530024E13 | 8610Z    | 3        | ARS établissements Publics de santé dotation globale      | 1        | Etablissement public de santé | 1979-01-01     | 1979-01-01        | 2016-07-25 |         | 419048.5   | 6781209.3  | 3,ATLASANTE,100,IGN,BD_ADRESSE,V2.2,LAMBERT_93 | 2022-09-05     | .      |                      |                   | 53000       |                 |                         | False             | 48.070830362 | -0.773614615 | LAVAL           | 45 Rue Chef Bataillon Henri Geret | 53000 LAVAL             | CH DE LAVAL                      | CENTRE HOSPITALIER DE LAVAL                     | 13                  | Etablissement Public Communal d'Hospitalisation | Public                   | CH                       | False            | True                   |
| 11744 | 2017-12-31  | 380781369     | 010783009     | LE MAS DES CHAMPS                |                                        |                           |                         | 1        | Rue       | DU VILLAGE                 |                 |             | .       | 38          | ISERE               | 38370 ST PRIM      | 04 74 56 57 57 | 474564954 | 109          | Etablissement de santé privé autorisé en SSR   | 1107                 | Soins Suite & Réadap                                        | 7.7554456E13 | 8610Z    | 4        | ARS établissements PSPH dotation globale                  | 6        | ESPIC                         | 1932-04-30     | 1932-04-30        | 2014-09-17 |         | 840287.9   | 6484246.2  | 1,ATLASANTE,100,IGN,BD_ADRESSE,V2.2,LAMBERT_93 | 2017-11-22     | 84     | AUVERGNE-RHONE-ALPES | 38448             | 38370       | ST PRIM         | Activités hospitalières | False             | 45.443303697 | 4.7946709871 | ST PRIM         | 1 Rue Du Village                  | 38370 ST PRIM           | ORSAC                            | ORGANISATION POUR LA SANTE ET L'ACCUEIL (ORSAC) | 61                  | Ass.L.1901 R.U.P.                               | Privé à but non lucratif | Privé à but non lucratif | False            | False                  |
| 14308 | 2017-12-31  | 600002067     | 590799995     | UAD SANTÉLYS SENLIS              |                                        |                           |                         | 14       | Avenue    | PAUL ROUGÉ                 |                 |             | .       | 60          | OISE                | 60300 SENLIS       | 03 44 60 72 32 | 344607235 | 146          | Structure d'Alternative à la dialyse en centre | 1203                 | Dialyse Ambulatoire                                         | .            | 8610Z    | 7        | ARS établissements de santé non financés dotation globale | 7        | NON ESPIC                     | 1996-06-17     | 1996-06-17        | 2014-10-09 |         | 669006.2   | 6899673.5  | 1,ATLASANTE,100,IGN,BD_ADRESSE,V2.2,LAMBERT_93 | 2017-11-22     | 32     | HAUTS-DE-FRANCE      | 60612             | 60300       | SENLIS          | Activités hospitalières | False             | 49.196037579 | 2.5748176863 | SENLIS          | 14 Avenue Paul Rougé              | 60300 SENLIS            | SANTELYS ASSOCIATION             | SANTELYS ASSOCIATION                            | 61                  | Ass.L.1901 R.U.P.                               | Privé à but non lucratif | Privé à but non lucratif | False            | False                  |
```

Cette table contient l'ensemble des données Finess **historisées**.
Il y a une ligne par export et par Finess présent à la date de l'export.
Pour un même Finess il ya donc très souvent plusieurs lignes.

Les colonnes qui s'avèrent souvent utiles sont:

- `num_finess_et`: numéro finess géographique
- `num_finess_ej`: numéro finess juridique
- `latitude` et `longitude`: les coordonnées géographiques de l'établissement dans le référentiel WGS84
- `raison_sociale_et`: le nom de l'établissement
- `dernier_enregistrement`: indique si cette ligne est la dernière connue pour le finess en question

## Préparation des données

### Récupération des clés associées à des catégories de valeurs

Les cas d'usages pour lesquels la base est utilisé ne concernent souvant qu'un sous-ensemble des domaines présents dans la base. On peut par exemple vouloir récupérer l'historique complet des IQSS issus du questionnaire patient (ESATIS).

La mécanique pour sélectionner le sous ensemble de données d'interêt est la suivante:

- Dans le fichier `metadata.csv` utiliser la colonne `source` pour identifier le domaine d'intérêt
- Dans le fichier `metadata.csv` recupérer le nom des variables de la `source` donnée qui sont stockées dans la colonne `name`
- Parmis ces variables, utiliser les colonne `type_metier` (pour les IQSS) et/ou `description` pour sélectionner les variables d'intérêt
- Dans le fichier `valeurs.csv` sélection toutes les lignes dont la colonne `key` contient une variable du domaine d'intérêt

Ci-dessous un exemple de cette mécanique:

```sas
/*Affichage des sources de données existantes*/
proc freq data=metadata_df order=freq;
	table source/norow nocol nopercent nocum;
run;
```

```markdown
| source                           | Fréquence |
| -------------------------------- | --------- |
| IQSS dossier patient             | 403       |
| SAE                              | 115       |
| IQSS questionnaire patient       | 34        |
| Certification v2014              | 32        |
| IQSS PMSI                        | 22        |
| Certification v2021              | 21        |
| IQSS questionnaire établissement | 15        |
| Activités ET                     | 11        |
```

```sas
/*Récupération des noms des variables de certification v2014*/
proc sql noprint;
	select distinct quote(trim(name)) into:certif_14_keys separated by ","
	from metadata_df
	where source="Certification v2014";
quit;

/*Récupération des nom des variables esatis*/
proc sql noprint;
	select distinct quote(trim(name)) into:esatis_keys separated by ","
	from metadata_df
	where source="IQSS questionnaire patient";
quit;
```

```sas
/*Affichage des types métiers certification
Les variables de certification n'ont pas de type métier*/
proc print data=metadata_df (obs=5);
	where name in (&certif_14_keys);
	var name description type_metier;
run;
```

```markdown
| Obs. | name                               | description                                                                                                                                                                                                                           | type_metier |
| ---- | ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| 1    | certif_V2014_id_niveau             | Code du niveau de certification validé par le collège (1 : Certification, 2 : Certification avec recommandation(s) d’amélioration, 3 : Certification avec obligation (s) d’amélioration, 4 : Sursis à statuer, 5 : Non certification) |             |
| 2    | certif_date                        | Date de délibération du collège de la HAS qui a donné lieu à la décision                                                                                                                                                              |             |
| 5    | certif_V2014_decision_thematique_1 | Décision finale pour la thématique : Management stratégique, gouvernance                                                                                                                                                              |             |
| 6    | certif_V2014_decision_thematique_2 | Décision finale pour la thématique : Qualité de vie au travail                                                                                                                                                                        |             |
| 7    | certif_V2014_decision_thematique_3 | Décision finale pour la thématique : Management de la qualité et des risques                                                                                                                                                          |             |
```

```sas
/*Affichage des variables de type score pour esatis*/
proc print data=metadata_df (obs=5);
	where name in (&esatis_keys);
	var name description type_metier;
run;
```

```markdown
| Obs. | name                           | description                                                                                                                                                           | type_metier      |
| ---- | ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| 496  | nb_rep_score_all_rea_ajust     | Nombre de réponses concernant la satisfaction globale des patients                                                                                                    | effectif_observe |
| 497  | score_all_rea_ajust            | Note ajustée de satisfaction globale classement des patients                                                                                                          | score            |
| 498  | classement_48h                 | Classement en 6 classes {A: &gt;=77.3, B: [74-77.3[, C: [70.7-74[, D: &lt;70.7, DI: données insuffisantes(min de 30 questionnaires exploitables), NR: non répondants} | classe           |
| 499  | evolution_48h                  | Changement de classe d'une année à l'autre                                                                                                                            | evolution        |
| 500  | nb_rep_score_accueil_rea_ajust | Nombre de réponses concernant la satisfaction de l'accueil                                                                                                            | effectif_observe |
```

```sas
/*Affichage des variables de type score pour esatis*/
proc print data=metadata_df;
	where name in (&esatis_keys) and type_metier="score";
	var name description type_metier;
run;

```

```markdown
| Obs. | name                    | description                                                                     | type_metier |
| ---- | ----------------------- | ------------------------------------------------------------------------------- | ----------- |
| 497  | score_all_rea_ajust     | Note ajustée de satisfaction globale classement des patients                    | score       |
| 501  | score_accueil_rea_ajust | Note ajustée concernant la satisfaction de l'accueil                            | score       |
| 503  | score_pecinf_rea_ajust  | Note ajustée de satisfaction de la prise en charge infirmiers/aides-soignants   | score       |
| 505  | score_pecmed_rea_ajust  | Note ajustée de satisfaction de la prise en charge par les médecins/chirurgiens | score       |
| 507  | score_chambre_rea_ajust | Note ajustée de satisfaction de la chambre                                      | score       |
| 509  | score_repas_rea_ajust   | Note ajustée de satisfaction des repas                                          | score       |
| 511  | score_sortie_rea_ajust  | Note ajustée de satisfaction de la sortie                                       | score       |
| 512  | taux_reco_brut_48h      | Pourcentage de patients recommandant certainement l’établissement               | score       |
| 515  | score_all_ajust_ca      | Score de satisfaction globale ajusté                                            | score       |
| 518  | score_avh_ajust         | Note ajustée de la satisfaction avant l’hospitalisation                         | score       |
| 520  | score_acc_ajust         | Note ajustée concernant la satisfaction accueil le jour de l’hospitalisation    | score       |
| 522  | score_pec_ajust         | Note ajustée de la satisfaction de la prise en charge                           | score       |
| 524  | score_cer_ajust         | Note ajustée de la satisfaction chambre et repas / collation                    | score       |
| 526  | score_ovs_ajust         | Note ajustée de la satisfaction sortie et retour à domicile                     | score       |
| 528  | taux_reco_brut_ca       | Pourcentage de patients recommandant certainement l’établissement               | score       |
```

### Isolation des domaines de données

Afin de pouvoir préparer et analyser un domaine de données, il est nécessaire de l'isoler en vue de traitements décrits plus bas.
Pour des raisons de volumétrie, le présent tutoriel se restreint aux données esatis. Mais il est naturellement possible d'effectuer ces analyses sur un ensemble de domaines de données, voire sur tous les domaines (les mêmes opérations décrites ici peuvent leur être appliquées).

```sas
/*Isolation du domaine de données esatis*/
data esatis_df;
	set valeurs_df;
	if key in (&esatis_keys);
run;

/*Isolation du domaine de données certification v2014, pour illustration*/
data certif_df;
	set valeurs_df;
	if key in (&certif_14_keys);
run;
```

### Pivot de la table esatis

Actuellement, le format clé-valeur de la base induit que chaque valeur de chaque variable pour chacun des établissements est associée à une ligne.
Nous souhaitons pourvoir avoir un regroupement des valeurs pour chaque établissement.

Pour cela, nous devons effectuer les opérations suivantes (cf. cellule de code ci-dessous) :

- Effectuer un pivot à la table pour avoir une colonne par variable (par type), chaque ligne représentant un établissement pour une année donnée (puisque certaines valeurs sont collectées annuellement)
- Chaque classe de valeur n'ayant qu'un type de valeur acceptable (hormis le type `missing_value`), toutes les colonnes des autres types seront remplis de valeus manquantes => il s'agit donc également de supprimer ces colonnes inutiles
- Les colonnes des valeurs manquantes pour une clé sont fusionnées avec la colonne de valeurs de la clé correspondante pour ne pas perdre l'information correspondante

```sas
/*Les colonnes de valeurs sont pivotées selon la colonne key, pour chaque triplet (annee, finess, finess_type)
et pour chaque variable de valeur (pour gérer les conflits de format avec SAS)
Les colonnes ne comportant que des valeurs manquantes sont supprimées*/
%macro tt (var=);

proc sort data=esatis_df out=&var;
	where &var is not missing;
	by annee finess finess_type;
run;

proc transpose data=&var out=&var (drop=_name_ );
	by annee finess finess_type;
	id key;
	var &var;
run;

%mend;
%tt (var=value_boolean);
%tt (var=value_string);
%tt (var=value_integer);
%tt (var=value_float);
%tt (var=value_date);
/*optionnelle si les valeurs manquantes sont sans intérêt pour l'étude*/
%tt (var=missing_value);

/*on fusionne les 6 tables obtenues*/
data esatis_df;
	merge value_boolean value_string value_integer value_float value_date missing_value ;
	by annee finess finess_type;
	if finess ne '';
run;
```

Un échantillon de la table résultante peut être observée ci-dessous, en notant qu'il manque des données pour de nombreux établissements sur certaines années car certains indicateurs n'étaient pas calculés à ce moment là.

```sas
proc print data=esatis_df (obs=5);
	where ranuni(100)<0.001;
run;
```

```markdown
| Obs. | annee | finess    | finess_type | classement_48h        | evolution_48h    | classement_ca | evolution_ca | nb_rep_score_all_rea_ajust | score_all_rea_ajust | nb_rep_score_accueil_rea_ajust | score_accueil_rea_ajust | nb_rep_score_pecinf_rea_ajust | score_pecinf_rea_ajust | nb_rep_score_pecmed_rea_ajust | score_pecmed_rea_ajust | nb_rep_score_chambre_rea_ajust | score_chambre_rea_ajust | nb_rep_score_repas_rea_ajust | score_repas_rea_ajust | nb_rep_score_sortie_rea_ajust | score_sortie_rea_ajust | nb_rep_score_all_ajust_ca | score_all_ajust_ca | score_avh_ajust | nb_rep_score_avh_ajust | score_acc_ajust | nb_rep_score_acc_ajust | score_pec_ajust | nb_rep_score_pec_ajust | score_cer_ajust | nb_rep_score_cer_ajust | score_ovs_ajust | nb_rep_score_ovs_ajust | taux_reco_brut_48h | nb_reco_brut_48h | taux_reco_brut_ca | nb_reco_brut_ca |
| ---- | ----- | --------- | ----------- | --------------------- | ---------------- | ------------- | ------------ | -------------------------- | ------------------- | ------------------------------ | ----------------------- | ----------------------------- | ---------------------- | ----------------------------- | ---------------------- | ------------------------------ | ----------------------- | ---------------------------- | --------------------- | ----------------------------- | ---------------------- | ------------------------- | ------------------ | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | ------------------ | ---------------- | ----------------- | --------------- |
| 301  | 2016  | 330780040 | geo         | A                     |                  |               |              | 91                         | 77.88               | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 661  | 2016  | 620105940 | geo         | Données insuffisantes |                  |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 1339 | 2017  | 220000541 | geo         | A                     | 2-Stable         |               |              | 41                         | 78.73               | 41                             | 79.73                   | 41                            | 84.23                  | 41                            | 82.2                   | 41                             | 77.4                    | 41                           | 63.58                 | 41                            | 75.28                  | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 2184 | 2017  | 910000306 | geo         | Données insuffisantes | 4-Non calculable |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 2198 | 2017  | 910300359 | geo         | Données insuffisantes | 4-Non calculable |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
```

## Exploitation des données

Dans cette section, une analyse simple sur le domaine esatis est effectuée à des fins d'illustration quant à la valorisation de ces données.

Le cas d'usage de cette analyse est l'observation de l'évolution du positionnement national d'un établissement identifié (il s'agit ici de l'établissement associé au finess n°`060000478`) vis-à-vis des établissements de même catégorie (il s'agit ici de la catégorie _Centres Hospitaliers_, associée au code `1102`) sur l'indicateur `score_all_ajust_ca` (correspondant au "score de satisfaction globale ajusté" en chirurgie ambulatoire).

Pour ce faire, il faut :

- associer aux données esatis les données finess nécessaires à la jointure et à la catégorisation des établissements
- calculer le classement de l'établissement (son rang absolu et le pourcentage associé) pour chaque année
- représenter la distribution des établissements pour chaque année en positionnant l'établissement étudié

Ces étapes sont réalisées dans les deux sections ci-dessous.

### Finalisation de la préparation des données

```sas
/*On ne prend que les informations des finess les plus récentes pour éviter les doublons à la jointure*/
data finess_df;
	set finess_df;
	if dernier_enregistrement="True";
run;

/*Jointure entre les données esatis et les données Finess restreintes aux agrégats de catégories d'établissement*/
proc sql;
	create table df as select b.num_finess_et, b.categorie_agregat_et,b.raison_sociale_et,b.departement,a.*
	from esatis_df a
	inner join finess_df b
	on (a.finess=b.num_finess_et);
quit;

/*Affichage d'un extrait de la table obtenue*/
proc print data=df (obs=5);
	where ranuni(100)<0.001;
run;
```

```markdown
| Obs. | num_finess_et | categorie_agregat_et | raison_sociale_et          | departement | annee | finess    | finess_type | classement_48h        | evolution_48h    | classement_ca | evolution_ca | nb_rep_score_all_rea_ajust | score_all_rea_ajust | nb_rep_score_accueil_rea_ajust | score_accueil_rea_ajust | nb_rep_score_pecinf_rea_ajust | score_pecinf_rea_ajust | nb_rep_score_pecmed_rea_ajust | score_pecmed_rea_ajust | nb_rep_score_chambre_rea_ajust | score_chambre_rea_ajust | nb_rep_score_repas_rea_ajust | score_repas_rea_ajust | nb_rep_score_sortie_rea_ajust | score_sortie_rea_ajust | nb_rep_score_all_ajust_ca | score_all_ajust_ca | score_avh_ajust | nb_rep_score_avh_ajust | score_acc_ajust | nb_rep_score_acc_ajust | score_pec_ajust | nb_rep_score_pec_ajust | score_cer_ajust | nb_rep_score_cer_ajust | score_ovs_ajust | nb_rep_score_ovs_ajust | taux_reco_brut_48h | nb_reco_brut_48h | taux_reco_brut_ca | nb_reco_brut_ca |
| ---- | ------------- | -------------------- | -------------------------- | ----------- | ----- | --------- | ----------- | --------------------- | ---------------- | ------------- | ------------ | -------------------------- | ------------------- | ------------------------------ | ----------------------- | ----------------------------- | ---------------------- | ----------------------------- | ---------------------- | ------------------------------ | ----------------------- | ---------------------------- | --------------------- | ----------------------------- | ---------------------- | ------------------------- | ------------------ | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | --------------- | ---------------------- | ------------------ | ---------------- | ----------------- | --------------- |
| 301  | 330780040     | 1110                 | NOUVELLE CLINIQUE BEL AIR  | 33          | 2016  | 330780040 | geo         | A                     |                  |               |              | 91                         | 77.88               | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 661  | 620105940     | 1110                 | POLYCLINIQUE DU TERNOIS    | 62          | 2016  | 620105940 | geo         | Données insuffisantes |                  |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 1339 | 220000541     | 1102                 | CENTRE HOSPITALIER PAIMPOL | 22          | 2017  | 220000541 | geo         | A                     | 2-Stable         |               |              | 41                         | 78.73               | 41                             | 79.73                   | 41                            | 84.23                  | 41                            | 82.2                   | 41                             | 77.4                    | 41                           | 63.58                 | 41                            | 75.28                  | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 2184 | 910000306     | 1102                 | CH D ORSAY                 | 91          | 2017  | 910000306 | geo         | Données insuffisantes | 4-Non calculable |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
| 2198 | 910300359     | 1110                 | HOPITAL PRIVE D ATHIS MONS | 91          | 2017  | 910300359 | geo         | Données insuffisantes | 4-Non calculable |               |              | .                          | .                   | .                              | .                       | .                             | .                      | .                             | .                      | .                              | .                       | .                            | .                     | .                             | .                      | .                         | .                  | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .               | .                      | .                  | .                | .                 | .               |
```

```sas
data df;
	set df;
/*Pour cette étude, seuls les centres hospitaliers sont conservés
L'indicateur n'est collecté qu'a partir de 2019
Les établissements sans valeurs pour l'indicateur sont retirés*/
	if categorie_agregat_et=1102 and annee>=2019 and score_all_ajust_ca ne . then output;
/*Seules les colonnes suivantes sont nécessaires pour la suite : annee, taux_reco_brut_ca, finess*/
	keep annee finess raison_sociale_et departement score_all_ajust_ca;
run;
```

Un extrait de la table correspondante peut être observée :

```sas
proc print data=df (obs=5);
	where ranuni(100)<0.01;
run;
```

```markdown
| Obs. | raison_sociale_et            | departement | annee | finess    | score_all_ajust_ca |
| ---- | ---------------------------- | ----------- | ----- | --------- | ------------------ |
| 2    | CH BUGEY SUD                 | 01          | 2019  | 010000032 | 79.26              |
| 139  | CH BIGORRE SITE GESPE TARBES | 65          | 2019  | 650000417 | 79.04              |
| 190  | CHD SITE LA ROCHE SUR YON    | 85          | 2019  | 850000142 | 81.85              |
| 262  | CH DE VALENCE                | 26          | 2020  | 260000013 | 77.26              |
| 301  | CH EMILE ROUX LE PUY         | 43          | 2020  | 430000117 | 78.43              |
```

### Visualisation des résultats

```sas
/*choix du finess*/
%let finess=060000478;

/*Calcul du rang et de la valeur du score de l'établissement sur l'année considérée*/
proc rank data=df out=df_rang descending ties=high;
  var score_all_ajust_ca ;
  ranks rang ;
  by annee;
run;

%macro graphe;

proc sql noprint;
	select max(annee),min(annee) into:anneemax,:anneemin  from df;
quit;

%do i=&anneemin %to &anneemax;
proc sql noprint;
	select max(rang)into:maxrang
	from df_rang
	where annee=&i;

	select score_all_ajust_ca,rang,round(rang/&maxrang*100,0.1) into:valeur,:rang, :pctrang
	from df_rang
    where finess="&finess" and annee=&i;
quit;

/*Graphique*/
title color=black "&i" ;
proc sgplot data=df noborder nowall;
   where annee=&i;
   histogram score_all_ajust_ca / nooutline fillattrs=(color="#4D7EBF");
   refline &valeur / axis=x lineattrs=(thickness=2 color=red) labelpos=min label=("&valeur") labelattrs=(color=red weight=bold);
   xaxis label="Score de satisfaction globale ajusté" type=linear values=(0 to 100 by 20) ;
   yaxis label="Fréquences" VALUES=(0 TO 35 BY 5);
   inset ("Classement :"="%sysfunc(compress(&rang))/%sysfunc(compress(&maxrang))" "Position (%):"="%sysfunc(compress(&pctrang %))") / textattrs=(Color=red) position=topleft noborder labelalign=left backcolor=lightgrey;
   inset ("finess"="&finess")/ textattrs=(Color=red weight=bold) position=TOPright noborder labelalign=right;
run;
%end;
%mend;

%graphe ;
```

#### 2019

![et_2019](img/et_2019.png)

#### 2020

![et_2020](img/et_2020.png)

#### 2021

![et_2021](img/et_2021.png)

⚠️ Attention : L’analyse et l’interprétation des résultats des IQSS nécessite de bien prendre connaissance de leur définition.

Nous pouvons constater que l'établissement considéré dans cette étude a:

- Amélioré son score (de `77.73` à `80.78`)
- Amélioré sa place dans la distribution de `54.6`ème percentile à `24.3`ème percentile
