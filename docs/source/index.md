```{include} ../../README.md
:relative-docs: docs/source/
:relative-images:
```

```{toctree}
   :hidden:
   :maxdepth: 1
   :glob:

   Données <donnees/index>
   Création de la base <creation/index>
   Développement <developpement/index>
   Historique <CHANGELOG>
```
