import logging
from typing import Final

import pandas as pd
import pytest
from frictionless import Schema

from bqss import data_acquisition_utils
from bqss.bqss.metadata_reader import get_iqss_metadata
from bqss.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)
from bqss.data_acquisition_utils import clean_directory
from bqss.iqss.constants import (
    DATA_IQSS_PATH,
    FINAL_IQSS_PATH,
    RAW_IQSS_PATH,
    RESOURCES_IQSS_PATH,
    SCHEMAS_IQSS_PATH,
)
from bqss.iqss.data_acquisition import download_data
from bqss.iqss.processing import merged_iqss_data, process_data
from bqss.iqss.processing_clean import clean_data
from bqss.iqss.processing_finess import set_finess_type
from bqss.iqss.processing_reformate import reformate_historized_iqss
from bqss.iqss.validate_metadata import validate_metadata_file
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger: Final[logging.Logger] = logging.getLogger(__name__)
HISTORIZED_IQSS_COLUMNS: Final[list] = [
    "finess",
    "finess_type",
    "raison_sociale",
    "annee",
    "key",
    "value_float",
    "value_string",
]


@pytest.fixture(name="data_download", scope="session")
def fixture_data_download(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du téléchargement des données"
        )
        return
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_IQSS_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)


@pytest.mark.usefixtures("data_download")
def test_download_data():
    iqss_raw_data = {
        "2014": [
            "rcp_mco_data.csv",
            "dan_mco_data.csv",
            "dpa_ssr_data.csv",
            "dpa_mco_data.csv",
            "dpa_had_data.csv",
            "dpa_psy_data.csv",
        ],
        "2015": [
            "hpp_mco_data.csv",
            "idm_mco_data.csv",
            "dia_mco_data.csv",
            "avc_mco_data.csv",
        ],
        "2016": [
            "dan_mco_data.csv",
            "dpa_had_data.csv",
            "dpa_mco_data.csv",
            "dpa_psy_data.csv",
            "dpa_ssr_data.csv",
            "rcp_mco_data.csv",
            "ias_lin_data.csv",
        ],
        "2017": [
            "avc_mco_data.csv",
            "dia_mco_data.csv",
            "hpp_mco_data.csv",
            "ias_None_data.csv",
        ],
        "2018": [
            "rcp_mco_data.csv",
            "eteortho_mco_ipaqss_data.csv",
            "dpa_ssr_data.csv",
            "dpa_mco_data.csv",
            "dpa_had_data.csv",
            "dan_mco_data.csv",
            "ias_None_data.csv",
        ],
        "2019": [
            "iqss_None_data.csv",
        ],
        "2020": [
            "iqss_None_data.xlsx",
        ],
        "2021": [
            "iqss_None_data.xlsx",
        ],
        "2022": [
            "iqss_None_data.xlsx",
        ],
    }
    for source_name, theme_activity in iqss_raw_data.items():
        iqss_data_path = RAW_IQSS_PATH / source_name

        assert len(list(iqss_data_path.iterdir())) > 0

        for file in theme_activity:
            iqss_file_path = iqss_data_path / file
            assert iqss_file_path.exists()
            assert iqss_file_path.stat().st_size > 0


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du traitement des données"
        )
        return
    process_data()


@pytest.mark.skip(
    reason="Certains indicateurs du thème DPA des recueils 2019 et 2020 ne sont pas remontés qu'au niveau géographique, à investiguer"
)
def test_build_historized_iqss_data():
    clean_directory(FINAL_IQSS_PATH)

    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv", sep=",")
    clean_data(metadata_df)
    merged_iqss_data()
    set_finess_type()
    historized_iqss_key_value = reformate_historized_iqss()

    assert historized_iqss_key_value.shape[0] > 10000
    assert historized_iqss_key_value.shape[1] == 7
    assert (
        len(
            set(historized_iqss_key_value["key"])
            - set(metadata_df["code"].str.lower())
        )
        == 0
    )
    assert set(HISTORIZED_IQSS_COLUMNS) == set(
        historized_iqss_key_value.columns
    )

    # Validate type finess
    # Pour les indicateurs ete | iso avant 2019, le finess est mixte (mélange de
    # geo et jur). A partir de 2019, le finess est géographique uniquement
    iso_ete_after_2019 = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("isoortho|eteortho")
        & (historized_iqss_key_value["annee"] >= 2019)
    ]
    assert (
        iso_ete_after_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs ete/iso ne sont pas remontés qu'au niveau géographique"

    # Pour les indicateurs du thèmes DPA des recueil 2019 et 2020 le finess doit
    # être géographique
    dpa_2018_2019 = historized_iqss_key_value[
        historized_iqss_key_value["annee"].isin([2018, 2019])
        & historized_iqss_key_value["key"].str.contains("dpa")
    ]
    assert (
        dpa_2018_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs du thème DPA des receuils 2019 et 2020 ne sont pas remontés qu'au niveau géographique"

    # Le type de finess des indicateurs psy et icsha doit être mixte (geo + jur)
    psy_icsha_finess_types = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("psy|icsha")
    ]["finess_type"].unique()
    assert all(
        finess_type in psy_icsha_finess_types
        for finess_type in [FINESS_TYPE_GEO, FINESS_TYPE_JUR]
    ), "Le type de finess des indicateurs psy/icsha devrait être mixte (géo et jur)"


def test_validate_iqss_metadata(from_cli, caplog):
    schema = Schema(SCHEMAS_IQSS_PATH / "iqss_key_value.json")

    assert is_metadata_valid(schema)

    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip de la validation des metatadata"
        )
        return

    validate_metadata_file()

    for record in caplog.records:
        assert record.levelno < logging.ERROR


@pytest.mark.usefixtures("data_processing")
def test_validate_full_iqss_data():
    schema = tableschema_to_pandera(
        SCHEMAS_IQSS_PATH / "iqss_key_value.json",
        nullable_bool=True,
        nullable_int=True,
        coerce=False,
    )

    kv_df = pd.read_parquet(FINAL_IQSS_PATH / "iqss_key_value.parquet")
    schema.validate(kv_df)


@pytest.mark.usefixtures("data_processing")
def test_regression_full_iqss_data():
    kv_df = pd.read_parquet(FINAL_IQSS_PATH / "iqss_key_value.parquet")
    assert (
        kv_df[
            kv_df["key"] == "mco_eteortho_eteortho_2017-ete_ortho_cible_etbt"
        ]
        .notna()
        .any()
        .any()
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_iqss_resultat_data():
    kv_df = pd.read_parquet(FINAL_IQSS_PATH / "iqss_key_value.parquet")
    meta_df = get_iqss_metadata()

    resv_df = kv_df[
        kv_df["key"].isin(
            meta_df[meta_df["type_metier"] == "resultat"]["name"]
        )
    ]

    resb_df = resv_df.groupby(["key"]).agg(
        {"value_float": ["min", "max"], "value_integer": ["min", "max"]}
    )

    # cet indicateur peut dépasser les 100 par construction...
    resb_df = resb_df.loc[
        resb_df.index != "tch_ias_icshav4_2021-icsha_v4_resultat"
    ]

    for value_col in ["value_integer", "value_float"]:
        # Toutes les valeurs doivent être comprises entre 0 et 100
        assert resb_df[resb_df[value_col]["max"] < 1.1].empty
        assert resb_df[resb_df[value_col]["max"] > 100].empty
