from bqss import data_acquisition_utils
from bqss.iqss.constants import RAW_IQSS_PATH, RESOURCES_IQSS_PATH
from bqss.iqss.data_acquisition import download_data

iqss_raw_data = {
    "2014": [
        "rcp_mco_data.csv",
        "dan_mco_data.csv",
        "dpa_ssr_data.csv",
        "dpa_mco_data.csv",
        "dpa_had_data.csv",
        "dpa_psy_data.csv",
    ],
    "2015": [
        "hpp_mco_data.csv",
        "idm_mco_data.csv",
        "dia_mco_data.csv",
        "avc_mco_data.csv",
    ],
    "2016": [
        "dan_mco_data.csv",
        "dpa_had_data.csv",
        "dpa_mco_data.csv",
        "dpa_psy_data.csv",
        "dpa_ssr_data.csv",
        "rcp_mco_data.csv",
        "ias_lin_data.csv",
    ],
    "2017": [
        "avc_mco_data.csv",
        "dia_mco_data.csv",
        "hpp_mco_data.csv",
        "ias_None_data.csv",
    ],
    "2018": [
        "rcp_mco_data.csv",
        "eteortho_mco_ipaqss_data.csv",
        "dpa_ssr_data.csv",
        "dpa_mco_data.csv",
        "dpa_had_data.csv",
        "dan_mco_data.csv",
        "ias_None_data.csv",
    ],
    "2019": [
        "iqss_None_data.csv",
    ],
    "2020": [
        "iqss_None_data.xlsx",
    ],
}


def test_download_data():
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_IQSS_PATH
    )

    download_data(data_sources)

    for source_name, theme_activity in iqss_raw_data.items():

        iqss_data_path = RAW_IQSS_PATH / source_name

        assert len(list(iqss_data_path.iterdir())) > 0

        for file in theme_activity:
            iqss_file_path = iqss_data_path / file
            assert iqss_file_path.exists()
            assert iqss_file_path.stat().st_size > 0
