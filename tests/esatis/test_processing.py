import logging
import os
from typing import Final

import pandas as pd
import pytest
from frictionless import Schema

from bqss import data_acquisition_utils
from bqss.esatis.constants import (
    DATA_ESATIS_PATH,
    FINAL_ESATIS_PATH,
    RAW_ESATIS_PATH,
    RESOURCES_ESATIS_PATH,
    SCHEMAS_ESATIS_PATH,
)
from bqss.esatis.data_acquisition import download_data
from bqss.esatis.main import main
from bqss.validation_utils import is_data_valid_parquet, is_metadata_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)
HISTORIZED_ESATIS_COLUMNS: Final[list] = [
    "finess",
    "finess_type",
    "annee",
    "key",
    "value_integer",
    "value_float",
    "value_string",
    "missing_value",
]


@pytest.fixture(name="data_download", scope="session")
def fixture_data_download(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du téléchargement des données"
        )
        return
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_ESATIS_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_ESATIS_PATH
        )
        download_data(data_sources)


@pytest.mark.usefixtures("data_download")
def test_download_data():
    esatis_raw_data = {
        "2016": ["mco_48h.csv"],
        "2017": ["mco_48h.csv"],
        "2018": ["mco_48h.csv", "mco_ca.csv"],
        "2019": ["mco_48h.csv", "mco_ca.csv"],
        "2020": ["mco_48h.xlsx", "mco_ca.xlsx"],
        "2021": ["mco_48h.xlsx", "mco_ca.xlsx"],
        "2022": ["mco_48h.xlsx", "mco_ca.xlsx", "ssr_ssr.xlsx"],
    }

    for source_name, theme_activity in esatis_raw_data.items():
        assert len(os.listdir(RAW_ESATIS_PATH)) > 0

        esatis_data_path = RAW_ESATIS_PATH / source_name

        for file in theme_activity:
            esatis_file_path = esatis_data_path / file
            assert esatis_file_path.exists()
            assert esatis_file_path.stat().st_size > 0


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du processing des données"
        )
        return
    main(download=False)


@pytest.mark.usefixtures("data_processing")
def test_build_historized_esatis_key_value_data():
    esatis_key_value = pd.read_parquet(
        FINAL_ESATIS_PATH / "esatis_key_value.parquet"
    )

    assert esatis_key_value.shape[0] > 10000
    assert esatis_key_value.shape[1] == 8
    assert set(HISTORIZED_ESATIS_COLUMNS) == set(esatis_key_value.columns)

    # Make sure 2019 parsing and "`.` as missing value" parsing works
    assert (
        esatis_key_value.loc[
            (esatis_key_value["finess"] == "030000061")
            & (esatis_key_value["annee"] == 2019)
            & (esatis_key_value["key"] == "score_acc_ajust"),
            "value_float",
        ].iloc[0]
        == 82.81
    )

    # Ensure missing values are distributed across all components
    assert (
        esatis_key_value.loc[
            (esatis_key_value["finess"] == "980500011")
            & (esatis_key_value["annee"] == 2022)
            & (esatis_key_value["key"] == "score_sortie_rea_ajust"),
            "missing_value",
        ].iloc[0]
        == "Non validé"
    )


def test_validate_esatis_metadata():
    schema = Schema(SCHEMAS_ESATIS_PATH / "esatis.json")

    assert is_metadata_valid(schema)


def test_validate_esatis_key_value_metadata():
    schema = Schema(SCHEMAS_ESATIS_PATH / "esatis_key_value.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_full_esatis_data():
    assert is_data_valid_parquet(
        FINAL_ESATIS_PATH / "esatis.parquet",
        SCHEMAS_ESATIS_PATH / "esatis.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_full_esatis_key_value_data():
    assert is_data_valid_parquet(
        FINAL_ESATIS_PATH / "esatis_key_value.parquet",
        SCHEMAS_ESATIS_PATH / "esatis_key_value.json",
    )
