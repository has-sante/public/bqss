from itertools import islice

import pytest
from frictionless import Schema

from bqss.esatis.constants import FINAL_ESATIS_PATH, SCHEMAS_ESATIS_PATH
from bqss.esatis.processing import process_data
from bqss.validation_utils import is_data_valid, is_metadata_valid

# import needed for its data_download fixture
# noinspection PyUnresolvedReferences
from tests.esatis.test_processing import (  # pylint: disable=unused-import
    fixture_data_download,
)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()

    # create a subset of the data to validate
    with open(
        FINAL_ESATIS_PATH / "esatis_key_value_output_extract.csv",
        "w",
        encoding="UTF-8",
    ) as esatis_output_extract, open(
        FINAL_ESATIS_PATH / "esatis_key_value.csv", encoding="UTF-8"
    ) as esatis_output:
        esatis_output_extract.writelines(
            line for line in islice(esatis_output, 1000)
        )

    with open(
        FINAL_ESATIS_PATH / "esatis_output_extract.csv", "w", encoding="UTF-8"
    ) as esatis_output_extract, open(
        FINAL_ESATIS_PATH / "esatis.csv", encoding="UTF-8"
    ) as esatis_output:
        esatis_output_extract.writelines(
            line for line in islice(esatis_output, 1000)
        )


def test_validate_esatis_key_value_metadata():
    schema = Schema(SCHEMAS_ESATIS_PATH / "esatis_key_value.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_esatis_key_value_data():
    assert is_data_valid(
        FINAL_ESATIS_PATH / "esatis_key_value_output_extract.csv",
        SCHEMAS_ESATIS_PATH / "esatis_key_value.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_full_esatis_key_value_data():
    assert is_data_valid(
        FINAL_ESATIS_PATH / "esatis_key_value.csv",
        SCHEMAS_ESATIS_PATH / "esatis_key_value.json",
    )


def test_validate_esatis_metadata():
    schema = Schema(SCHEMAS_ESATIS_PATH / "esatis.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_esatis_data():
    assert is_data_valid(
        FINAL_ESATIS_PATH / "esatis_output_extract.csv",
        SCHEMAS_ESATIS_PATH / "esatis.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_full_esatis_data():
    assert is_data_valid(
        FINAL_ESATIS_PATH / "esatis.csv",
        SCHEMAS_ESATIS_PATH / "esatis.json",
    )
