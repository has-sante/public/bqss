import pytest
from frictionless import Schema

from bqss.certification_14_20.constants import (
    FINAL_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.certification_14_20.processing import process_data
from bqss.validation_utils import is_data_valid, is_metadata_valid


@pytest.fixture(name="data_processing", scope="session")
def fixture_data_processing(from_cli):
    if not from_cli:
        process_data()


def test_validate_certifications_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "certifications.json")

    assert is_metadata_valid(schema)


def test_validate_thematiques_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "thematiques.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_certifications_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "certifications.csv",
        SCHEMAS_DOMAIN_PATH / "certifications.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_thematiques_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "thematiques.csv",
        SCHEMAS_DOMAIN_PATH / "thematiques.json",
    )
