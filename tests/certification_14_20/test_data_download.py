import logging
import os

from bqss import data_acquisition_utils
from bqss.certification_14_20.constants import (
    RAW_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
)
from bqss.certification_14_20.data_download import download_data

logger = logging.getLogger(__name__)


def test_download_data(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du téléchargement des données"
        )
        return
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )

    download_data(data_sources)

    assert len(os.listdir(RAW_DOMAIN_PATH)) > 0
    for filename in [
        "certifications.csv",
        "thematiques.csv",
        "thematiques_labels.csv",
    ]:
        assert os.path.exists(RAW_DOMAIN_PATH / filename)
        assert os.stat(RAW_DOMAIN_PATH / filename).st_size > 0
