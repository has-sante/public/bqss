import logging
import os

import pandas as pd

from bqss.certification_21_25.constants import (
    FINAL_DOMAIN_PATH,
    RAW_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.certification_21_25.main import main
from bqss.validation_utils import is_data_valid

logger = logging.getLogger(__name__)


def test_process_certifications(from_cli):
    if not from_cli:
        main(download=not from_cli)
    else:
        logger.info(
            "Appel depuis la CLI, skip du test du processing des données"
        )

    for filename in ["certifications", "chapitres", "objectifs"]:
        valid = is_data_valid(
            FINAL_DOMAIN_PATH / f"{filename}.csv",
            SCHEMAS_DOMAIN_PATH / f"{filename}.json",
        )
        if not valid:
            raise ValueError(f"{filename} non valide")


def test_certifications_results():
    assert len(os.listdir(RAW_DOMAIN_PATH)) > 0
    for filename in [
        "demarches.csv",
        "etablissements_geo.csv",
        "etablissements_jur.csv",
        "resultats_chapitres.csv",
        "resultats_objectifs.csv",
    ]:
        assert os.path.exists(RAW_DOMAIN_PATH / filename)
        assert os.stat(RAW_DOMAIN_PATH / filename).st_size > 0

    kv_df = pd.read_csv(FINAL_DOMAIN_PATH / "certifications_key_value.csv")

    assert len(kv_df) > 5000
    assert kv_df.shape[1] == 8
    assert kv_df.columns.tolist() == [
        "annee",
        "finess",
        "finess_type",
        "key",
        "value_boolean",
        "value_integer",
        "value_float",
        "value_date",
    ]

    should_have_res = kv_df[
        (kv_df["key"] == "certification_ref_2021_decision")
        & (kv_df["value_integer"].isin([1, 2]))
    ]["finess"]

    has_chapters = kv_df[
        kv_df["key"].str.startswith("certification_ref_2021_score_chapitre")
    ]["finess"]

    has_objectifs = kv_df[
        kv_df["key"].str.startswith("certification_ref_2021_score_objectif")
    ]["finess"]

    assert should_have_res.isin(has_chapters).all()
    assert should_have_res.isin(has_objectifs).all()
