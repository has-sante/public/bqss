import logging
import os
from typing import Final

import pandas as pd
import pytest
from frictionless import Schema

from bqss import data_acquisition_utils
from bqss.finess import processing, processing_finess, processing_finess_ej
from bqss.finess.constants import (
    FINAL_DOMAIN_PATH,
    HISTORIZED_AUTORISATIONS_AS_COLUMNS,
    PREPROCESSED_FINESS_EJ_PATH,
    PREPROCESSED_FINESS_ET_PATH,
    RAW_AUTORISATIONS_AS_PATH,
    RAW_DOMAIN_PATH,
    RAW_FINESS_EJ_PATH,
    RAW_FINESS_ET_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.finess.data_acquisition import download_data
from bqss.finess.processing_autorisations_as import FINAL_AS_DTYPES_DICT
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger = logging.getLogger(__name__)

LATEST_FINESS_COLUMNS: Final[list] = [
    "categagretab",
    "categetab",
    "codeape",
    "codemft",
    "codesph",
    "commune",
    "compldistrib",
    "complrs",
    "compvoie",
    "coordxet",
    "coordyet",
    "dateautor",
    "date_export",
    "dategeocod",
    "datemaj",
    "dateouv",
    "departement",
    "libcategagretab",
    "libcategetab",
    "libdepartement",
    "libmft",
    "libsph",
    "lieuditbp",
    "ligneacheminement",
    "nofinessej",
    "nofinesset",
    "numuai",
    "numvoie",
    "rs",
    "rslongue",
    "siret",
    "sourcecoordet",
    "telecopie",
    "telephone",
    "typvoie",
    "voie",
]

HISTORIZED_FINESS_COLUMNS: Final[list] = [
    "date_export",
    "num_finess_et",
    "num_finess_ej",
    "raison_sociale",
    "raison_sociale_longue",
    "complement_raison_sociale",
    "complement_distribution",
    "num_voie",
    "type_voie",
    "libelle_voie",
    "complement_voie",
    "lieu_dit_bp",
    "commune",
    "departement",
    "libelle_departement",
    "ligne_acheminement",
    "telephone",
    "telecopie",
    "categorie_et",
    "libelle_categorie_et",
    "categorie_agregat_et",
    "libelle_categorie_agregat_et",
    "siret",
    "code_ape",
    "code_mft",
    "libelle_mft",
    "code_sph",
    "libelle_sph",
    "date_ouverture",
    "date_autorisation",
    "date_maj",
    "num_uai",
    "coord_x_et",
    "coord_y_et",
    "source_coord_et",
    "date_geocodage",
    "region",
    "libelle_region",
    "code_officiel_geo",
    "code_postal",
    "libelle_routage",
    "libelle_code_ape",
    "ferme_cette_annee",
    "latitude",
    "longitude",
    "libelle_commune",
    "adresse_postale_ligne_1",
    "adresse_postale_ligne_2",
]

LATEST_AUTORISATIONS_AS_COLUMNS: Final[list] = [
    "activite",
    "dateautor",
    "date_export",
    "datefin",
    "datemeo",
    "forme",
    "libactivite",
    "libforme",
    "libmodalite",
    "libsectpsy",
    "modalite",
    "noautor",
    "noautorarhgos",
    "nofinessej",
    "nofinesset",
    "noimplarhgos",
    "noligautor",
    "noligautoranc",
    "rsej",
    "rset",
    "sectpsy",
]


ACTIVITES_ET_COLUMNS: Final[list] = [
    "annee",
    "finess",
    "key",
    "finess_type",
    "value_boolean",
    "value_string",
    "value_integer",
    "value_float",
    "missing_value",
    "value_date",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download(from_cli):
    if from_cli:
        logger.info("Appel depuis la CLI, skip du téléchargement des données")
        return
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not RAW_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    processing.clean_directory_tree()


@pytest.fixture(name="finess_et", scope="session")
def fixture_finess_et(from_cli):
    if from_cli:
        return pd.read_parquet(
            PREPROCESSED_FINESS_ET_PATH / "historized_finess_et.parquet"
        )
    return processing_finess.build_historized_finess_data()


@pytest.fixture(name="finess_ej", scope="session")
def fixture_finess_ej(from_cli):
    if from_cli:
        return pd.read_parquet(
            PREPROCESSED_FINESS_EJ_PATH / "historized_finess_ej.parquet"
        )
    return processing_finess_ej.build_historized_finess_ej_data()


@pytest.fixture(name="finess_et_ej", scope="session")
def fixture_finess_et_ej(finess_et, finess_ej):
    finess_et_ej = processing.add_fields_from_ej(finess_et, finess_ej)
    finess_et_ej.to_parquet(FINAL_DOMAIN_PATH / "finess.parquet")

    return finess_et_ej


@pytest.fixture(name="raw_activites_et", scope="session")
def fixture_finess_raw_activite(from_cli):
    if from_cli:
        raw_activite_et = pd.read_csv(
            FINAL_DOMAIN_PATH / "autorisations-as.csv"
        )[HISTORIZED_AUTORISATIONS_AS_COLUMNS]
    else:
        raw_activite_et = processing.process_autorisations_as_data()

    return raw_activite_et


@pytest.fixture(name="final_activites_et", scope="session")
def fixture_finess_activite_et(raw_activites_et, from_cli):
    if from_cli:
        final_activites_et = pd.read_csv(
            FINAL_DOMAIN_PATH / "activites_et_key_value.csv"
        )
    else:
        final_activites_et = processing.process_activites_et_data(
            raw_activites_et
        )

    return final_activites_et


@pytest.mark.usefixtures("data_download")
def test_download_data():
    # finess ET
    assert len(os.listdir(RAW_FINESS_ET_PATH)) > 0
    assert os.path.exists(RAW_FINESS_ET_PATH / "latest.csv")
    assert os.path.exists(RAW_FINESS_ET_PATH / "historic.zip")
    assert os.stat(RAW_FINESS_ET_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_FINESS_ET_PATH / "historic.zip").st_size > 0
    # finess EJ
    assert len(os.listdir(RAW_FINESS_EJ_PATH)) > 0
    assert os.path.exists(RAW_FINESS_EJ_PATH / "latest.csv")
    assert os.path.exists(RAW_FINESS_EJ_PATH / "historic.zip")
    assert os.stat(RAW_FINESS_EJ_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_FINESS_EJ_PATH / "historic.zip").st_size > 0
    # autorisation activités de soins
    assert len(os.listdir(RAW_AUTORISATIONS_AS_PATH)) > 0
    assert os.path.exists(RAW_AUTORISATIONS_AS_PATH / "latest.csv")
    assert os.path.exists(RAW_AUTORISATIONS_AS_PATH / "historic.zip")
    assert os.stat(RAW_AUTORISATIONS_AS_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_AUTORISATIONS_AS_PATH / "historic.zip").st_size > 0


def test_build_historized_finess_data(finess_et):
    assert finess_et.shape[0] > 190_000
    assert finess_et.shape[1] == 48
    assert set(HISTORIZED_FINESS_COLUMNS) == set(finess_et.columns)


def test_build_historized_finess_ej_data(finess_ej):
    assert finess_ej.shape[0] > 1e6
    assert finess_ej.shape[1] == 31


def test_build_autorisations_as_and_activites_et_data(
    raw_activites_et, final_activites_et
):
    assert raw_activites_et.shape[0] > 3e5
    assert raw_activites_et.shape[1] == 11
    assert (
        HISTORIZED_AUTORISATIONS_AS_COLUMNS
        == raw_activites_et.columns.tolist()
    )

    assert final_activites_et.shape[0] > 5e4
    assert final_activites_et.shape[1] == 10
    assert ACTIVITES_ET_COLUMNS == final_activites_et.columns.tolist()


def test_validate_finess_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "finess.json")

    assert is_metadata_valid(schema)


def test_validate_autorisations_as_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "autorisations_as.json")

    assert is_metadata_valid(schema)


def test_validate_activites_et_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "activites_et.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("raw_activites_et")
def test_validate_autorisations_as_data():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "autorisations_as.json", nullable_int=True
    )
    aut_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "autorisations-as.csv", dtype=FINAL_AS_DTYPES_DICT
    )
    # les données d'avant 2015 ont des clefs primaires nulles
    schema.validate(aut_df[aut_df["date_export"] >= "2015"])


@pytest.mark.usefixtures("final_activites_et")
def test_validate_activites_et_data():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "activites_et_key_value.json", nullable_int=True
    )

    act_df = pd.read_csv(FINAL_DOMAIN_PATH / "activites_et_key_value.csv")
    schema.validate(act_df)


@pytest.mark.usefixtures("finess_et_ej")
def test_validate_finess_data():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "finess.json",
        coerce=True,
        nullable_int=True,
    )
    finess_df = pd.read_parquet(FINAL_DOMAIN_PATH / "finess.parquet")
    schema.validate(finess_df)


@pytest.mark.usefixtures("finess_et_ej")
def test_validate_latitude_longitude():
    finess_df = pd.read_parquet(FINAL_DOMAIN_PATH / "finess.parquet")

    invalid_finess = finess_df[
        (finess_df["latitude"] < -90)
        | (finess_df["latitude"] > 90)
        | (finess_df["longitude"] < -180)
        | (finess_df["longitude"] > 180)
    ]

    assert (
        invalid_finess.empty
    ), f"There are {invalid_finess['nofinesset'].nunique()} FINESS with invalid latitude/longitude"

    # Check that most finess have non empty lat-lon

    nb_na_by_export_date_df = (
        finess_df[finess_df[["latitude", "longitude"]].isna().any(axis=1)]
        .groupby("date_export")
        .size()
    )
    nb_by_export_date_df = finess_df.groupby("date_export").size()
    missing_share_df = (
        (nb_na_by_export_date_df / nb_by_export_date_df)
        .dropna()
        .rename("share")
        .reset_index()
    )
    sparse_geo_df = (
        missing_share_df[missing_share_df["date_export"] > "2012"]["share"]
        > 0.01
    )
    # data before 2012 is messed up
    assert (
        not sparse_geo_df.any()
    ), f"Trop forte absence de coordonnées géographiques pour les exports suivantes {sparse_geo_df}"


@pytest.mark.usefixtures("final_activites_et")
def test_validate_annee_derniere_activite():
    act_df = pd.read_csv(FINAL_DOMAIN_PATH / "activites_et_key_value.csv")
    latest_year = act_df["annee"].max()
    assert sorted(
        act_df[
            (act_df["finess"] == "640000436")
            & (act_df["annee"] == latest_year)
        ]["key"].tolist()
    ) == ["psychiatrie"]
