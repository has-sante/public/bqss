"""Pytest configuration module"""
import pytest


def pytest_addoption(parser):
    """
    On ajoute une option pour désactiver le téléchargement des données de façon globale.
    Cela permet de valider les données lors des appels de la cli en utilisant pytest.
    Dans ce cas d'usage, les données ont déjà été téléchargées
    """
    parser.addoption(
        "--from-cli",
        action="store_true",
        default=False,
        help="Flag signaler qu'on appelle les tests depuis la cli",
    )


@pytest.fixture(scope="session")
def from_cli(request):
    return request.config.getoption("--from-cli")


def pytest_collection_modifyitems(items):
    """
    Les tests bqss doivent être joués après tous les autres.
    """
    first_items = []
    last_items = []
    # Iteratively move tests of each module to the end of the test queue
    for item in items:
        if item.module.__name__.startswith("tests.bqss."):
            last_items.append(item)
        else:
            first_items.append(item)

    items[:] = first_items + last_items
