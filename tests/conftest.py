"""Pytest configuration module"""


def pytest_collection_modifyitems(items):
    """
    Les tests bqss doivent être joués après tous les autres.
    """
    first_items = []
    last_items = []
    # Iteratively move tests of each module to the end of the test queue
    for item in items:
        if item.module.__name__.startswith("tests.bqss."):
            last_items.append(item)
        else:
            first_items.append(item)

    items[:] = first_items + last_items
