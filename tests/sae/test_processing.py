import glob
import logging
import os

import pandas as pd
import pytest
from frictionless import Schema

from bqss import data_acquisition_utils
from bqss.metadata_utils import table_schema_to_csv
from bqss.sae.constants import (
    DATA_DOMAIN_PATH,
    FINAL_DOMAIN_PATH,
    RAW_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.sae.data_download import download_data
from bqss.sae.processing import process_data
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger = logging.getLogger(__name__)


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du téléchargement des données"
        )
        return
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)


def test_download_data():
    assert len(os.listdir(RAW_DOMAIN_PATH)) >= 3
    # the following asserts are based on at least 3 years of data including 2019
    filenames = glob.glob(
        str(RAW_DOMAIN_PATH / f"**{os.path.sep}*.csv"), recursive=True
    )
    assert len(filenames) > 150
    mco_2019_file = glob.glob(
        str(RAW_DOMAIN_PATH / f"**{os.path.sep}MCO_2019r.csv"), recursive=True
    )
    assert len(mco_2019_file) == 1
    mco_2019_file = mco_2019_file[0]
    assert os.stat(mco_2019_file).st_size > 0


@pytest.fixture(name="sae_df", scope="session")
def fixture_sae_df(from_cli):
    if not from_cli:
        process_data()
    return pd.read_csv(FINAL_DOMAIN_PATH / "sae.csv")


def test_process_id_data(sae_df):
    sae_df = sae_df[[c for c in sae_df.columns if c.startswith("id_")]]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 4
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["id_rs"] == "HOPITAL FOCH"


def test_process_filtre_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("filtre_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 41
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["filtre_heb_med"]


def test_process_mco_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_")
            or c.startswith("mco_")
            or c.startswith("perinat_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 33
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["mco_lit_med"] == 267


def test_process_had_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("had_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 6
    es_test = sae_df[
        (sae_df["id_fi"] == "750806226") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["had_platot"] == 803


def test_process_ssr_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("ssr_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 8
    es_test = sae_df[
        (sae_df["id_fi"] == "750000499") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["ssr_lit"] == 10


def test_process_psy_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("psy_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 14
    es_test = sae_df[
        (sae_df["id_fi"] == "010000495") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["psy_jou_htp_gen"] == 96916


def test_process_dialyse_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("dialyse_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 40
    es_test = sae_df[
        (sae_df["id_fi"] == "920000650") & (sae_df["id_an"] == 2019)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["dialyse_capa_112ba"] == 12


def test_process_cancero_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("cancero_")
        ]
    ]

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 7
    es_test = sae_df[
        (sae_df["id_fi"] == "010000024") & (sae_df["id_an"] == 2018)
    ]
    assert len(es_test) == 1
    data = es_test.iloc[0, :]
    assert data["cancero_a10"] == 6888
    assert data["cancero_a15"] == 18510
    assert data["cancero_a16"] == 0


def test_process_usld_data(sae_df):
    sae_df = sae_df[
        [
            c
            for c in sae_df.columns
            if c.startswith("id_") or c.startswith("usld_")
        ]
    ]
    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 5
    es_test = sae_df[
        (sae_df["id_fi"] == "010000081") & (sae_df["id_an"] == 2018)
    ]
    assert len(es_test) == 1
    assert es_test.iloc[0]["usld_jou"] == 10316


def test_validate_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "sae.json")

    assert is_metadata_valid(schema)


def test_table_schema_to_csv():
    if not FINAL_DOMAIN_PATH.exists():
        FINAL_DOMAIN_PATH.mkdir(parents=True)
    table_schema_to_csv(
        SCHEMAS_DOMAIN_PATH / "sae.json",
        FINAL_DOMAIN_PATH / "sae_metadata.csv",
    )

    assert os.path.exists(FINAL_DOMAIN_PATH / "sae_metadata.csv")


def test_validate_data(sae_df):
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "sae.json", nullable_int=True
    )

    schema.validate(sae_df)
