import json
import logging
import os
from typing import Final

import pandas as pd
import pytest
from frictionless import Schema
from pytest_mock import MockerFixture
from requests.models import Response

from bqss import data_acquisition_utils
from bqss.bqss.constants import (
    FINAL_DOMAIN_PATH,
    KEY_VALUE_DTYPES_DICT,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.bqss.models.documents import FinessDocumentModel
from bqss.bqss.models.documents_stats import StatsModel
from bqss.bqss.open_data import update_metadata, upload_open_data
from bqss.bqss.processing import process_document_data, process_key_value_data
from bqss.constants import FINESS_TYPE_GEO, FINESS_TYPE_JUR, UTF_8
from bqss.data_acquisition_utils import clean_directory
from bqss.finess import constants as finess_constants
from bqss.finess.processing_finess import FINAL_FINESS_DTYPES_DICT
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger: Final[logging.Logger] = logging.getLogger(__name__)


@pytest.mark.run("second-to-last")
def test_process_data():
    clean_directory(FINAL_DOMAIN_PATH)

    process_key_value_data()

    assert os.path.exists(FINAL_DOMAIN_PATH / "autorisations_as.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "finess.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "metadata.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "metadata.xlsx")
    assert os.path.exists(FINAL_DOMAIN_PATH / "valeurs.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "nomenclatures.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "nomenclatures.xlsx")


@pytest.mark.run("last")
def test_validate_finess_join():
    finess_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
    )
    all_finess = pd.concat(
        [finess_df["num_finess_et"], finess_df["num_finess_ej"]]
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    missing_finess_df = valeurs_df[~valeurs_df["finess"].isin(all_finess)]

    assert (
        missing_finess_df.empty
    ), f"""Ces finess ne sont pas référencés : {missing_finess_df["finess"].unique().to_numpy()}"""


@pytest.mark.run("last")
def test_validate_finess_type():
    finess_df = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    valeurs_finess_jur = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_JUR
    ]
    valeurs_wrong_finess = valeurs_finess_jur[
        ~valeurs_finess_jur["finess"].isin(finess_df["num_finess_ej"])
    ]
    assert valeurs_wrong_finess.empty, (
        f"Des finess parmi les clés suivantes ne sont pas juridiques "
        f"(ou non-référencés) : {valeurs_wrong_finess['key'].unique().tolist()}"
    )

    valeurs_finess_geo = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_GEO
    ]
    valeurs_wrong_finess = valeurs_finess_geo[
        ~valeurs_finess_geo["finess"].isin(finess_df["num_finess_et"])
    ]
    assert valeurs_wrong_finess.empty, (
        f"Des finess parmi des clé suivantes ne sont pas géographiques "
        f"(ou non-référencés) : {valeurs_wrong_finess['key'].unique().tolist()}"
    )


@pytest.mark.run("last")
def test_update_metadata(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "bqss.data_gouv_utils.requests.put",
        return_value=ok_response,
    )

    update_metadata(data_sources)


@pytest.mark.run(after="test_process_data")
def test_process_document_data():
    db_file_path = FINAL_DOMAIN_PATH / "base_document_etablissements.jsonl"
    stats_file_path = FINAL_DOMAIN_PATH / "base_document_statistiques.json"
    db_file_path.unlink(missing_ok=True)
    stats_file_path.unlink(missing_ok=True)

    process_document_data()
    assert db_file_path.exists()
    assert stats_file_path.exists()


@pytest.mark.run("last")
def test_validate_table_schemas():
    metadata_schema = Schema(SCHEMAS_DOMAIN_PATH / "metadata.json")
    valeurs_schema = Schema(SCHEMAS_DOMAIN_PATH / "valeurs.json")
    nomenclatures_schema = Schema(SCHEMAS_DOMAIN_PATH / "nomenclatures.json")

    assert is_metadata_valid(metadata_schema)
    assert is_metadata_valid(valeurs_schema)
    assert is_metadata_valid(nomenclatures_schema)


@pytest.mark.run("last")
def test_validate_valeurs_data():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "valeurs.json",
        nullable_bool=True,
        nullable_int=True,
        coerce=False,
    )

    val_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv", dtype=KEY_VALUE_DTYPES_DICT
    )
    schema.validate(val_df)


@pytest.mark.run("last")
def test_validate_metadata_content():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "metadata.json", coerce=True, nullable_int=True
    )

    meta_df = pd.read_csv(FINAL_DOMAIN_PATH / "metadata.csv")
    schema.validate(meta_df)


@pytest.mark.run("last")
def test_validate_nomenclatures_content():
    schema = tableschema_to_pandera(SCHEMAS_DOMAIN_PATH / "nomenclatures.json")

    nomenclatures_df = pd.read_csv(FINAL_DOMAIN_PATH / "nomenclatures.csv")
    schema.validate(nomenclatures_df)


@pytest.mark.run(after="test_process_document_data")
def test_validate_base_document():
    with (
        FINAL_DOMAIN_PATH / "base_document_etablissements.jsonl"
    ).open() as filep:
        for jsonl in filep:
            finess_d = json.loads(jsonl)
            FinessDocumentModel.parse_obj(finess_d)
            # Quick to test to ensure activites are here
            if finess_d["finess_geo"] == "020000162":
                assert sorted(
                    finess_d["activites"], key=lambda d: d["id"]
                ) == sorted(
                    [
                        {"id": "reanimation", "libelle": "Réanimation"},
                        {"id": "nephrologie", "libelle": "Néphrologie"},
                        {
                            "id": "ssr",
                            "libelle": "Soins de Suite et de Réadaptation",
                        },
                        {"id": "medecine", "libelle": "Médecine"},
                        {"id": "cancerologie", "libelle": "Cancérologie"},
                        {"id": "obstetrique", "libelle": "Obstétrique"},
                        {"id": "chirurgie", "libelle": "Chirurgie"},
                    ],
                    key=lambda d: d["id"],
                )


@pytest.mark.run(after="test_process_document_data")
def test_validate_base_document_stats():
    with (
        FINAL_DOMAIN_PATH / "base_document_statistiques.json"
    ).open() as filep:
        stats_d = json.load(filep)
        StatsModel.parse_obj(stats_d)


@pytest.mark.run(after="test_process_document_data")
def test_update_data(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "bqss.data_gouv_utils.requests.post",
        return_value=ok_response,
    )

    upload_open_data(data_sources)
