import json
import logging
from typing import Final
from unittest.mock import MagicMock

import pandas as pd
import pytest
from frictionless import Schema
from pytest_mock import MockerFixture
from requests.models import Response

from bqss import data_acquisition_utils
from bqss.bqss.constants import (
    FINAL_DOMAIN_PATH,
    KEY_VALUE_DTYPES_DICT,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from bqss.bqss.main import main
from bqss.bqss.models.documents import FinessDocumentModel
from bqss.bqss.models.documents_stats import StatsModel
from bqss.bqss.models.finess import FinessSchema
from bqss.bqss.open_data import update_metadata, update_open_data
from bqss.constants import FINESS_TYPE_GEO, FINESS_TYPE_JUR, UTF_8
from bqss.finess import constants as finess_constants
from bqss.finess.constants import FINAL_COLUMNS_RENAMING_DICT
from bqss.finess.processing_finess import (
    FINESS_DTYPES_DICT,
    HISTORIC_COLUMNS_RENAMING_DICT,
)
from bqss.validation_utils import is_metadata_valid, tableschema_to_pandera

logger: Final[logging.Logger] = logging.getLogger(__name__)


FINAL_FINESS_DTYPES_DICT = {
    **{
        FINAL_COLUMNS_RENAMING_DICT.get(
            HISTORIC_COLUMNS_RENAMING_DICT.get(k, k), k
        ): v
        for k, v in FINESS_DTYPES_DICT.items()
        if k != "date_export"
    },
    **{
        "statut_juridique_ej": pd.Int64Dtype(),
        "libelle_commune": "string",
        "adresse_postale_voie": "string",
        "adresse_postale_commune": "string",
    },
}


@pytest.fixture(name="data_processing", scope="session")
def fixture_data_processing(from_cli):
    if from_cli:
        logger.info(
            "Appel depuis la CLI, skip du test du processing des données"
        )
        return
    main()


@pytest.mark.usefixtures("data_processing")
def test_process_data():
    assert (FINAL_DOMAIN_PATH / "base-document-etablissements.jsonl").exists()
    assert (FINAL_DOMAIN_PATH / "base-document-statistiques.json").exists()
    assert (FINAL_DOMAIN_PATH / "autorisations-as.csv").exists()
    assert (FINAL_DOMAIN_PATH / "finess.csv").exists()
    assert (FINAL_DOMAIN_PATH / "metadata.csv").exists()
    assert (FINAL_DOMAIN_PATH / "metadata.xlsx").exists()
    assert (FINAL_DOMAIN_PATH / "valeurs.csv").exists()
    assert (FINAL_DOMAIN_PATH / "nomenclatures.csv").exists()
    assert (FINAL_DOMAIN_PATH / "nomenclatures.xlsx").exists()


@pytest.mark.usefixtures("data_processing")
def test_validate_finess_join():
    finess_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
        parse_dates=["date_export"],
    )
    all_finess = pd.concat(
        [finess_df["num_finess_et"], finess_df["num_finess_ej"]]
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    missing_finess_df = valeurs_df[~valeurs_df["finess"].isin(all_finess)]

    assert (
        missing_finess_df.empty
    ), f"""Ces finess ne sont pas référencés : {missing_finess_df["finess"].unique().to_numpy()}"""


@pytest.mark.usefixtures("data_processing")
def test_validate_finess_type():
    finess_df = pd.read_parquet(
        finess_constants.FINAL_DOMAIN_PATH / "finess.parquet",
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    valeurs_finess_jur = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_JUR
    ]
    valeurs_wrong_finess = valeurs_finess_jur[
        ~valeurs_finess_jur["finess"].isin(finess_df["num_finess_ej"])
    ]
    assert valeurs_wrong_finess.empty, (
        f"Des finess parmi les clés suivantes ne sont pas juridiques "
        f"(ou non-référencés) : {valeurs_wrong_finess['key'].unique().tolist()}"
    )

    valeurs_finess_geo = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_GEO
    ]
    valeurs_wrong_finess = valeurs_finess_geo[
        ~valeurs_finess_geo["finess"].isin(finess_df["num_finess_et"])
    ]
    assert valeurs_wrong_finess.empty, (
        f"Des finess parmi des clé suivantes ne sont pas géographiques "
        f"(ou non-référencés) : {valeurs_wrong_finess['key'].unique().tolist()}"
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_finess_data():
    finess_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        dtype=FINAL_FINESS_DTYPES_DICT,
        parse_dates=["date_export"],
    )
    FinessSchema.validate(finess_df)


@pytest.mark.usefixtures("data_processing")
def test_psy_redescente():
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    valeurs = (
        valeurs_df[
            (valeurs_df["finess"] == "020000543")
            & (valeurs_df["key"].str.match(r"psy_smhosp_.*_2021.*_resultat"))
        ][["key", "annee", "value_float"]]
        .sort_values("key")
        .to_dict(orient="records")
    )
    # Correspond au FINESS Juridique 020000295
    assert valeurs == [
        {
            "key": "psy_smhosp_add_2021-add_resultat",
            "annee": 2021,
            "value_float": 50.5,
        },
        {
            "key": "psy_smhosp_add_2021-add_resultat",
            "annee": 2023,
            "value_float": 55.2,
        },
        {
            "key": "psy_smhosp_cv_2021-cv_resultat",
            "annee": 2021,
            "value_float": 67.6,
        },
        {
            "key": "psy_smhosp_cv_2021-cv_resultat",
            "annee": 2023,
            "value_float": 59.5,
        },
        {
            "key": "psy_smhosp_pcd_2021-pcd_resultat",
            "annee": 2021,
            "value_float": 84.0,
        },
        {
            "key": "psy_smhosp_pcd_2021-pcd_resultat",
            "annee": 2022,
            "value_float": 96.0,
        },
        {
            "key": "psy_smhosp_qls_2021-qls_resultat",
            "annee": 2021,
            "value_float": 15.7,
        },
        {
            "key": "psy_smhosp_qls_2021-qls_resultat",
            "annee": 2022,
            "value_float": 9.7,
        },
    ]


@pytest.mark.usefixtures("data_processing")
def test_validate_table_schemas():
    metadata_schema = Schema(SCHEMAS_DOMAIN_PATH / "metadata.json")
    valeurs_schema = Schema(SCHEMAS_DOMAIN_PATH / "valeurs.json")
    nomenclatures_schema = Schema(SCHEMAS_DOMAIN_PATH / "nomenclatures.json")

    assert is_metadata_valid(metadata_schema)
    assert is_metadata_valid(valeurs_schema)
    assert is_metadata_valid(nomenclatures_schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_valeurs_data():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "valeurs.json",
        nullable_bool=True,
        nullable_int=True,
        coerce=False,
    )

    val_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv", dtype=KEY_VALUE_DTYPES_DICT
    )
    schema.validate(val_df)


@pytest.mark.usefixtures("data_processing")
def test_validate_metadata_content():
    schema = tableschema_to_pandera(
        SCHEMAS_DOMAIN_PATH / "metadata.json", coerce=True, nullable_int=True
    )

    meta_df = pd.read_csv(FINAL_DOMAIN_PATH / "metadata.csv")
    schema.validate(meta_df)


@pytest.mark.usefixtures("data_processing")
def test_validate_nomenclatures_content():
    schema = tableschema_to_pandera(SCHEMAS_DOMAIN_PATH / "nomenclatures.json")

    nomenclatures_df = pd.read_csv(FINAL_DOMAIN_PATH / "nomenclatures.csv")
    schema.validate(nomenclatures_df)


@pytest.mark.usefixtures("data_processing")
def test_validate_base_document():
    nb_etablissements_in_base = 0
    with (
        FINAL_DOMAIN_PATH / "base-document-etablissements.jsonl"
    ).open() as filep:
        for jsonl in filep:
            finess_d = json.loads(jsonl)
            FinessDocumentModel.parse_obj(finess_d)
            nb_etablissements_in_base += 1
            # Quick to test to ensure activites are here
            if finess_d["finess_geo"] == "020000162":
                assert sorted(
                    finess_d["activites"], key=lambda d: d["id"]
                ) == sorted(
                    [
                        {"id": "nephrologie", "libelle": "Néphrologie"},
                        {
                            "id": "ssr",
                            "libelle": "Soins de Suite et de Réadaptation",
                        },
                        {"id": "medecine", "libelle": "Médecine"},
                        {"id": "cancerologie", "libelle": "Cancérologie"},
                        {"id": "obstetrique", "libelle": "Obstétrique"},
                        {"id": "chirurgie", "libelle": "Chirurgie"},
                    ],
                    key=lambda d: d["id"],
                )

    # Nb etablissements actifs le 2023-04-07
    # changements d'autorisations dans le finess
    expected_num = 4261
    assert nb_etablissements_in_base >= expected_num
    assert nb_etablissements_in_base < expected_num + expected_num * 0.1


@pytest.mark.usefixtures("data_processing")
def test_validate_base_document_stats():
    with (
        FINAL_DOMAIN_PATH / "base-document-statistiques.json"
    ).open() as filep:
        stats_d = json.load(filep)
        StatsModel.parse_obj(stats_d)


@pytest.mark.run("last")
def test_update_metadata(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "bqss.data_gouv_utils.requests.put",
        return_value=ok_response,
    )

    update_metadata(data_sources, api_key="dummy")  # pragma: allowlist secret


@pytest.mark.run(after="test_process_document_data")
def test_update_data(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "bqss.data_gouv_utils.requests.put",
        return_value=ok_response,
    )
    mocker.patch(
        "bqss.data_gouv_utils.get_file_hash",
        return_value="customsha256",
    )

    update_open_data(
        data_sources,
        remote_fs=MagicMock(),
        environment="prod",
        api_key="dummy",  # pragma: allowlist secret
    )
