#! /usr/bin/env python
import logging

import click
import pytest
from fsspec.callbacks import TqdmCallback
from requests import ConnectionError  # pylint: disable=redefined-builtin
from requests import HTTPError

from bqss import data_acquisition_utils, data_gouv_utils, storage_utils
from bqss.bqss.compare import compare_valeurs
from bqss.bqss.constants import FINAL_DOMAIN_PATH, RESOURCES_DOMAIN_PATH
from bqss.bqss.main import main as bqss_main
from bqss.bqss.open_data import update_metadata, update_open_data
from bqss.certification_14_20 import constants as certification_14_20_constants
from bqss.certification_14_20.main import main as certification_14_20_main
from bqss.certification_21_25 import constants as certification_21_25_constants
from bqss.certification_21_25.main import main as certification_21_25_main
from bqss.constants import LOG_LEVEL, STORAGE_BUCKET
from bqss.esatis import constants as esatis_constants
from bqss.esatis.main import main as esatis_main
from bqss.finess import constants as finess_constants
from bqss.finess.main import main as finess_main
from bqss.iqss import constants as iqss_constants, validate_metadata
from bqss.iqss.main import main as iqss_main
from bqss.sae import constants as sae_constants
from bqss.sae.main import main as sae_main

logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def finess(download, validate, force) -> None:
    """Lance le pipeline du domaine de données FINESS"""
    if (
        not force
        and download
        and finess_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {finess_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False
    finess_main(download=download)
    if validate:
        res = pytest.main(["--from-cli", "tests/finess"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def certification_14_20(download, validate, force) -> None:
    """
    Lance le pipeline du domaine de données Certification du référentiel 2014-2020
    """
    if (
        not force
        and download
        and certification_14_20_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {certification_14_20_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False

    certification_14_20_main(download=download)

    if validate:
        res = pytest.main(["--from-cli", "tests/certification_14_20"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def certification_21_25(download, validate, force) -> None:
    """
    Lance le pipeline du domaine de données Certification du référentiel 2021-2025
    """
    if (
        not force
        and download
        and certification_21_25_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {certification_21_25_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False

    certification_21_25_main(download=download)

    if validate:
        res = pytest.main(["--from-cli", "tests/certification_21_25"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def sae(download, validate, force) -> None:
    """Lance le pipeline du domaine de données SAE"""
    if (
        not force
        and download
        and sae_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {sae_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False

    sae_main(download=download)

    if validate:
        res = pytest.main(["--from-cli", "tests/sae"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def iqss(download, validate, force) -> None:
    """Lance le pipeline du domaine de données IQSS"""
    if (
        not force
        and download
        and iqss_constants.DATA_IQSS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {iqss_constants.DATA_IQSS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False

    iqss_main(download=download)

    if validate:
        res = pytest.main(["--from-cli", "tests/iqss"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
def infer_iqss_metadata():
    """
    Génère une nouvelle version du fichier de metadata des IQSS qualhas pour
    intégrer les nouveaux indicateurs.

    Cette commande essaie de deviner les valeurs des différentes colonnes pour chaque
    composante d'indicateurs.

    Un fichier `resources/iqss/new_metadata.csv` est crée qui contient le résultat de l'analyse.
    """
    validate_metadata.infer_iqss_metadata()


@cli.command()
@click.option(
    "--download/--no-download",
    default=True,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Écrase les données brutes si elles existent",
)
def esatis(download, validate, force) -> None:
    """Lance le pipeline du domaine de données e-Satis"""
    if (
        not force
        and download
        and esatis_constants.DATA_ESATIS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {esatis_constants.DATA_ESATIS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        download = False

    esatis_main(download=download)

    if validate:
        res = pytest.main(["--from-cli", "tests/esatis"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--force",
    default=False,
    is_flag=True,
    help="Force le run de tous les pipelines nécessaires à la constitution de la base",
)
@click.option(
    "--validate",
    default=False,
    is_flag=True,
    help="Active la validation des données pour chaque domaine de données intermédiaire",
)
@click.pass_context
def bqss(
    ctx,
    force=False,
    validate=False,
) -> None:
    """Lance la constitution de la base BQSS"""

    if force or not finess_constants.FINAL_DOMAIN_PATH.exists():
        ctx.invoke(finess, download=True, validate=validate, force=force)
    if force or not certification_14_20_constants.FINAL_DOMAIN_PATH.exists():
        ctx.invoke(
            certification_14_20, download=True, validate=validate, force=force
        )
    if force or not certification_21_25_constants.FINAL_DOMAIN_PATH.exists():
        ctx.invoke(
            certification_21_25, download=True, validate=validate, force=force
        )
    if force or not sae_constants.FINAL_DOMAIN_PATH.exists():
        ctx.invoke(sae, download=True, validate=validate, force=force)
    if force or not iqss_constants.FINAL_IQSS_PATH.exists():
        ctx.invoke(iqss, download=True, validate=validate, force=force)
    if force or not esatis_constants.FINAL_ESATIS_PATH.exists():
        ctx.invoke(esatis, download=True, validate=validate, force=force)

    bqss_main()

    if validate:
        res = pytest.main(["--from-cli", "tests/bqss"])
        if res != pytest.ExitCode.OK:
            raise click.ClickException("Échec de la validation")


@cli.command()
@click.option(
    "--from-env",
    type=click.Choice(["local", "preprod", "prod"], case_sensitive=False),
    required=True,
    help="Sélectionne la source des données",
)
@click.option(
    "--to-env",
    type=click.Choice(["preprod", "prod"], case_sensitive=False),
    required=True,
    help="Sélectionne la zone de dépôt dans le stockage objet",
)
def release_data(from_env, to_env):
    """
    Dépose les données sur le stockage objet et sur data-gouv.

    Requiert qu'un vault token avec le role `bqss` soit déclarer dans l'envvar `VAULT_TOKEN`

    Le flow est le suivant:

    - `cli release-data --from-env local --to-env preprod`
    - recette en preprod
    - `cli release-data --from-env preprod --to-env prod`
    """
    if from_env == "local" and to_env == "prod":
        raise click.ClickException(
            "Il faut d'abord pousser les données en preprod avant de les mettre en prod"
        )
    if from_env == to_env:
        raise click.ClickException(
            f"from-env ({from_env}) et to-env ({to_env}) doivent être différents"
        )

    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    remote_fs = storage_utils.get_remote_fs()
    if from_env == "local":
        for resource in data_sources["resources"]:
            remote_fs.put_file(
                str(FINAL_DOMAIN_PATH / resource["filename"]),
                f"{STORAGE_BUCKET}/{to_env}/{resource['filename']}",
                callback=TqdmCallback(
                    tqdm_kwargs={
                        "desc": f"Uploading: {STORAGE_BUCKET}/{to_env}/{resource['filename']}"
                    }
                ),
            )
    else:
        for resource in data_sources["resources"]:
            click.echo(
                f"Copying: {STORAGE_BUCKET}/{from_env}/{resource['filename']}"
                f" -> {STORAGE_BUCKET}/{to_env}/{resource['filename']}"
            )
            remote_fs.copy(
                f"{STORAGE_BUCKET}/{from_env}/{resource['filename']}",
                f"{STORAGE_BUCKET}/{to_env}/{resource['filename']}",
            )

    # On n'update data gouv que sur la prod
    if to_env != "prod":
        return

    api_key = data_gouv_utils.get_data_gouv_api_key()

    try:
        update_metadata(data_sources, api_key=api_key)
    except (ConnectionError, HTTPError) as error:
        raise RuntimeError(
            "Impossible de mettre à jour les métadonnées data.gouv.fr, pas d'upload en open data"
        ) from error

    try:
        update_open_data(
            data_sources,
            remote_fs,
            environment=to_env,
            api_key=api_key,
        )
    except (ConnectionError, HTTPError) as error:
        raise RuntimeError("L'upload sur data gouv a échoué") from error


@cli.command()
@click.option(
    "--source-env",
    type=click.Choice(["local", "preprod", "prod"], case_sensitive=False),
    required=True,
    help="Sélectionne la source des données à comparer",
)
@click.option(
    "--ref-env",
    type=click.Choice(["preprod", "prod"], case_sensitive=False),
    required=True,
    help="Sélectionne la source des données qui servira de référence",
)
@click.option(
    "--export-comp-file",
    is_flag=True,
    required=False,
    default=False,
    help="Exporte, en local, un fichier contenant les changements dans les données",
)
def compare_data(source_env, ref_env, export_comp_file):
    """
    Compare les données entre deux environnements.
    Permet d'afficher certains changements importants et de faire des vérifications
    manuelles avant de faire une mise à jour importante des données.

    Le scope de cette commande est principalement les FINESS actifs sur qualiscope.
    """
    compare_valeurs(source_env, ref_env, export_comp_file)


if __name__ == "__main__":
    cli()
