FROM python:3.11-slim
LABEL maintainer="Timothée Chehab <t.chehab@has-sante.fr>"
ENV LANG C.UTF-8

# Install common functionality for downstream layers/user env
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    curl \
    wget \
    gpg \
    git \
    make

# setup vault package
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    && echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com bullseye main" | tee /etc/apt/sources.list.d/hashicorp.list

RUN apt-get update && \
    apt-get install -y vault && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG POETRY_VERSION="1.7.1"
# Setup Poetry for Python package and dependency management
ENV POETRY_HOME=/opt/poetry \
    POETRY_VIRTUALENVS_CREATE=true
ENV PATH="${POETRY_HOME}/bin:${PATH}"
# Install `poetry` via `pip` and system `python`
RUN pip install poetry==${POETRY_VERSION} && \
    poetry --version && \
    poetry config --list

# Install project dependencies
COPY pyproject.toml poetry.lock /app/
WORKDIR /app/
RUN poetry install --no-interaction --no-root --with documentation && rm -rf ~/.cache/pip && rm -rf ~/.cache/pypoetry/artifacts && rm -rf ~/.cache/pypoetry/cache

# Note: delete the below if you do NOT want application files distributed with
# your container image
COPY . /app/
RUN poetry install --no-interaction

CMD ["/app/bin/entrypoint"]
